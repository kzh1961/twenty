<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<title>아이디 찾기</title>
<link href="/resources/web/css/bootstrap.css" rel="stylesheet" type="text/css"/>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<!-- <script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 -->



</head>
<body>

<div class="container" style="margin-bottom: 30px;">
	
<div class="panel panel-default">
			<div class="panel-body">
				<h3><center>아이디 찾기</center></h3>
				<form:form commandName="member" class="form-horizontal" role="form" action="idFindOk" method="post" name="memberFindForm"
			onsubmit="return chk()">

				<div class="form-group" id="divFindId" style="margin: 0 100px 10px 300px;" >
				<label for="inputName" class="col-lg-2 control-label">이름</label>
                <div class="col-lg-8">
                    <form:input type="text" class="form-control2" path="name" data-rule-required="true" placeholder="이름" maxlength="30"/>
				</div>
				</div>
				<div class="form-group" id="divFindemail" style="margin: 0 100px 10px 300px;">
				<label for="inputemail" class="col-lg-2 control-label">이메일</label>
                <div class="col-lg-8">
                    <form:input type="text" class="form-control2" path="email" data-rule-required="true" placeholder="이메일" maxlength="30"/>
				</div>
				</div>
				
	
				
				<div class="form-group" style="margin: 0 100px 10px 300px;">
				<div class="col-lg-offset-2 col-lg-10">
                    <button type="submit" class="btn btn-info">찾기</button>
                   <a href="${contextpath}/twenty/loginForm"  class="btn btn-info" >돌아가기</a>
                    </div>
            </div>
            
           
</form:form>
		</div>
</div>
</div>


</body>
</html>

