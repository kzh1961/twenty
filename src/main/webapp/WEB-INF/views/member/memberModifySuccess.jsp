<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<link href="/resources/web/css/bootstrap.css" rel="stylesheet" type="text/css"/>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

</head>

<body>

<center>
<div id="container">
	<div class="contents1">
<div class="panel panel-default">
			<div class="panel-body">
			
			<br><br>
			<strong>${member.id}</strong> 고객님 <strong>정보수정</strong>이 <strong>완료</strong>되었습니다.</p>
			<p>로그인 후 이용해 주세요</p>
			</div>
			</div>
	
	<br>

	
		<div class="form-group">
                
		<a href="${contextpath}/twenty/loginForm" class="btn btn-default">로그인</a>
					
	</div>
</div>
</div>
</center>
</body>
</html>

