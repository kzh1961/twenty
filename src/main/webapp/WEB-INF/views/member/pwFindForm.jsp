<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<title>아이디 찾기</title>
<link href="/resources/web/css/bootstrap.css" rel="stylesheet" type="text/css"/>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>




</head>
<body>
<div class="container">
	<div class="contents1">
<div class="panel panel-default">
			<div class="panel-body">
				<h3><center>비밀번호 찾기</center></h3>
				<form:form commandName="member" class="form-horizontal" role="form" action="pwFindOk" method="post" name="memberFindForm"
			onsubmit="return chk()">

				<div class="form-group" id="divFindPw" style="margin: 0 35px 10px 300px;">
				<label for="inputName" class="col-lg-2 control-label">아이디</label>
                <div class="col-lg-6">
                    <form:input type="text" class="form-control" path="id" data-rule-required="true" placeholder="아이디" maxlength="30"/>
				</div>
				</div>
				<div class="form-group" id="divjumin" style="margin: 0 50px 10px 300px;">
						<label for="inputJuminNumber" class="col-lg-2 control-label">주민번호</label>
						<div class="row">
							<div class="col-lg-3">
								<form:input type="text" class="form-control" path="jumin1"
									data-rule-required="true" placeholder="주민번호 앞6자리" maxlength="6" />
								
							</div>
							<div class="col-lg-3">
								<form:input type="password" class="form-control" path="jumin2"
									data-rule-required="true" placeholder="주민번호 뒷7자리" maxlength="7" />
							</div>
						</div>
					</div>
				
	
				
				<div class="form-group" style="margin: 0 100px 10px 310px;">
                <div class="col-lg-offset-2 col-lg-10">
                    <button type="submit" class="btn btn-info">찾기</button>
                   <a href="${contextpath}/twenty/loginForm" class="btn btn-info">돌아가기</a>
                    
                </div>
            </div>
           
</form:form>
		</div>
</div>
</div>
</div>
</body>
</html>