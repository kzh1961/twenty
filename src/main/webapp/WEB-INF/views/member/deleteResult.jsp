<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<title></title>
<link href="/resources/web/css/bootstrap.css" rel="stylesheet" type="text/css"/>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

</head>
<body>
<c:if test="${deleteCheck == 1 }">
<br />
<center>
<div id="container">
	<div class="contents1">
<div class="panel panel-default">
			<div class="panel-body">
				<h3>회원탈퇴</h3>
	
	
		
			<br><br>
			<p>고객님 <strong>회원탈퇴</strong>가 <strong>완료</strong>되었습니다.</p>
			
			
		</div>
		
				<div class="form-group">
                <div>
		<a href="${contextpath}/twenty/mainForm" class="btn btn-default">메인으로</a>
		</div>
		</div>
		
</div>
</div>
</div>
</center>
	
		 



</c:if>

<c:if test="${deleteCheck == -1 }">
<center>
<br/>
<div id="container">
	<div class="contents1">
<div class="panel panel-default">
			<div class="panel-body">
		
			<br><br>
			<p><strong>${session_name}</strong> 고객님 <strong>비밀번호</strong>가 <strong>틀렸습니다</strong></p>
			
		</div>
	</div>

	<div class="form-group">
                <div>
                
                   <input type="button" class="btn btn-default" onclick="javascript:history.back()" value="돌아가기">
                    
                </div>
            </div>
</div>
</div>

</center>

</c:if>

</body>
</html>






<script type="text/javascript">

	function back() {
		history.back();
	}
	
</script>
