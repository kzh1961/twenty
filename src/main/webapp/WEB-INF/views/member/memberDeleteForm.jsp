<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<title>회원 탈퇴</title>
<link href="/resources/web/css/bootstrap.css" rel="stylesheet" type="text/css"/>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<script type="text/javascript">
	function memberDeleteChk(){
		if(document.memberDeleteform.passwd.value == ""){
			alert("비밀번호를 입력하세요.");
			document.memberDeleteform.passwd.focus();
			return false;
		}
		return true;;
	}
</script>
<body>
<div id="container">
	<div class="contents1">
<div class="panel panel-default">
			<div class="panel-body">
		<h3><center>회원 탈퇴</center></h3>
<br/><br/>
<form:form commandName="member" class="form-horizontal" role="form" action="memberDelete" method="post" name="memberDeleteform" onsubmit="return memberDeleteChk()">
					
					
					
		
				<div class="form-group" id="divMemberDelete" style="margin: 0 50px 10px 300px;">
				<label for="inputPassword" class="col-lg-2 control-label">비밀번호</label>
                <div class="col-lg-7">
                    <form:input type="password" class="form-control2" path="password" data-rule-required="true" placeholder="패스워드" maxlength="20"/>
				</div><br/><br/><br/>
				<div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                    <button type="submit" class="btn btn-info">회원탈퇴</button>
                   <input type="button" class="btn btn-info" onclick="javascript:history.back()" value="돌아가기">
                    
                </div>
            </div>
				</div>
				</form:form>								
				</div>			
				</div>
		
	</div>
</div>
</body>
</html>

