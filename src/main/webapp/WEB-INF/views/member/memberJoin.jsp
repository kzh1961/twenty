<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>회원가입</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="/resources/web/css/bootstrap.css" rel="stylesheet"
	type="text/css" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script type="text/javascript">
	var EC_SDE_SHOP_NUM = 1;
	var SHOP = {
		getLanguage : function() {
			return "ko_KR";
		},
		getCurrency : function() {
			return "KRW";
		},
		getFlagCode : function() {
			return "ko";
		},
		isMultiShop : function() {
			return false;
		},
		isDefaultShop : function() {
			return true;
		},
		getProductVer : function() {
			return 2;
		},
		isSDE : function() {
			return true;
		}
	};
</script>
<script type="application/x-javascript">
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 
	 function openZipcode(){
			var url="zipcodeCheckForm";
			open(url, "confirm","toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=no, width=410, height=400");
		}





















</script>
<script>
	function sample6_execDaumPostcode() {
		new daum.Postcode(
				{
					oncomplete : function(data) {
						// 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

						// 각 주소의 노출 규칙에 따라 주소를 조합한다.
						// 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
						var fullAddr = ''; // 최종 주소 변수
						var extraAddr = ''; // 조합형 주소 변수

						// 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
						if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
							fullAddr = data.roadAddress;

						} else { // 사용자가 지번 주소를 선택했을 경우(J)
							fullAddr = data.jibunAddress;
						}

						// 사용자가 선택한 주소가 도로명 타입일때 조합한다.
						if (data.userSelectedType === 'R') {
							//법정동명이 있을 경우 추가한다.
							if (data.bname !== '') {
								extraAddr += data.bname;
							}
							// 건물명이 있을 경우 추가한다.
							if (data.buildingName !== '') {
								extraAddr += (extraAddr !== '' ? ', '
										+ data.buildingName : data.buildingName);
							}
							// 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
							fullAddr += (extraAddr !== '' ? ' (' + extraAddr
									+ ')' : '');
						}

						// 우편번호와 주소 정보를 해당 필드에 넣는다.
						document.getElementById('sample6_postcode').value = data.zonecode; //5자리 새우편번호 사용
						document.getElementById('sample6_address1').value = fullAddr;

						// 커서를 상세주소 필드로 이동한다.
						document.getElementById('sample6_address2').focus();
					}
				}).open();
	}
</script>
</head>
<body>
	<div class="container" style="margin-bottom: 30px;">
		<div class="panel panel-default">
			<div class="panel-body">
				<center>
					<h2>회원가입</h2>
				</center>
			</div>
			<form:form commandName="member" class="form-horizontal" role="form"
				method="post" action="memberJoin">
				<div class="xans-element- xans-member xans-member-join">
					<div class="form-group" id="divId">
						<label for="inputId" class="col-lg-2 control-label">아이디</label>
						<div class="col-lg-9">
							<form:input type="text"
								class="form-control onlyAlphabetAndNumber" path="id"
								data-rule-required="true" placeholder="아이디" maxlength="30" />
							<font color="red"><form:errors path="id" /></font> <font
								color="red"><form:errors element="id" /></font>
						</div>
					</div>
					<div class="form-group" id="divPassword">
						<label for="inputPassword" class="col-lg-2 control-label">패스워드</label>
						<div class="col-lg-9">
							<form:input type="password" class="form-control" path="password"
								name="excludeHangul" data-rule-required="true"
								placeholder="패스워드" maxlength="30" />
							<font color="red"><form:errors path="password" /></font>
						</div>
					</div>
					<div class="form-group" id="divPasswordCheck">
						<label for="inputPasswordCheck" class="col-lg-2 control-label">패스워드
							확인</label>
						<div class="col-lg-9">
							<form:input type="password" class="form-control" path="password2"
								data-rule-required="true" placeholder="패스워드 확인" maxlength="30" />
							<font color="red"><form:errors path="password2" /></font>
						</div>
					</div>
					<div class="form-group" id="divName">
						<label for="inputName" class="col-lg-2 control-label">이름</label>
						<div class="col-lg-9">
							<form:input type="text" class="form-control onlyHangul"
								path="name" data-rule-required="true"
								placeholder="한글만 입력 가능합니다." maxlength="15" />
							<font color="red"><form:errors path="name" /></font>
						</div>
					</div>

					<div class="form-group" id="divjumin">
						<label for="inputJuminNumber" class="col-lg-2 control-label">주민번호</label>
						<div class="row">
							<div class="col-lg-2">
								<form:input type="text" class="form-control" path="jumin1"
									data-rule-required="true" placeholder="주민번호 앞6자리" maxlength="6" />
								<font color="red"><form:errors path="jumin1" /></font>
							</div>
							<div class="col-lg-2">
								<form:input type="password" class="form-control" path="jumin2"
									data-rule-required="true" placeholder="주민번호 뒷7자리" maxlength="7" />
								<font color="red"><form:errors path="jumin2" /></font>
							</div>
						</div>
					</div>

					<div class="form-group" id="divEmail">
						<label for="inputEmail" class="col-lg-2 control-label">이메일</label>
						<div class="col-lg-9">
							<form:input type="email" class="form-control" path="email"
								data-rule-required="true" placeholder="이메일" maxlength="40" />
							<font color="red"><form:errors path="email" /></font>
						</div>
					</div>
					<div class="form-group" id="divPhoneNumber">
						<label for="inputPhoneNumber" class="col-lg-2 control-label">휴대폰
							번호</label>
						<div class="col-lg-9">
							<form:input type="tel" class="form-control onlyNumber"
								path="phone" data-rule-required="true"
								placeholder="-를 제외하고 숫자만 입력하세요." maxlength="11" />
							<font color="red"><form:errors path="phone" /></font>
						</div>
					</div>

					<div class="form-group">
						<label for="inputZipcode" class="col-lg-2 control-label">주소</label>
						<div class="row">
							<div class="col-lg-2">
								<form:input placeholder="우편번호" type="text" class="form-control"
									id="sample6_postcode" path="zipcode" onclick="this.value=''" />
							</div>
							<div class="col-lg-1">
								<form:input type="button" onclick="sample6_execDaumPostcode()"
									value="우편번호 찾기" path="" class="btn btn-default" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="inputZipcode" class="col-lg-2 control-label"></label>
						<div class="col-lg-9">
							<form:input placeholder="주소" type="text" class="form-control"
								id="sample6_address1" path="address1" onclick="this.value=''" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-offset-2 col-lg-9">
							<form:input placeholder="상세주소" type="text" class="form-control"
								id="sample6_address2" path="address2" onclick="this.value=''" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-offset-2 col-lg-9">
							<button type="submit" class="btn btn-info">가입완료</button>
						</div>
					</div>
				</div>
			</form:form>
		</div>
	</div>
</body>
</html>
