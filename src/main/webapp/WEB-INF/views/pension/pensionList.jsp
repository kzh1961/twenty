<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<title>다모여펜션 - 실시간 펜션예약</title>
<link href="/web/css/bootstrap.css" rel="stylesheet" type="text/css"
	media="all" />
<!-- jQuery(necessary for Bootstrap's JavaScript plugins) -->
<script src="/web/js/jquery-1.11.0.min.js"></script>
<!-- Custom-Theme-files
theme-style -->
<link href="/web/css/style.css" rel="stylesheet" type="text/css"
	media="all" />
<!-- //theme-style -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript">
	
	
	
	
	
	
	
	
	
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 









</script>
<!-- start-menu -->
<script src="/web/js/simpleCart.min.js">
	
</script>
<link href="/web/css/memenu.css" rel="stylesheet" type="text/css"
	media="all" />
<script type="text/javascript" src="/web/js/memenu.js"></script>
<script>
	$(document).ready(function() {
		$(".memenu").memenu();
	});
</script>
<!-- dropdown -->
<script src="/web/js/jquery.easydropdown.js"></script>
</head>
<body>

	<!--start-breadcrumbs-->
	<div class="breadcrumbs">
		<div class="container">
			<div class="breadcrumbs-main">
				<ol class="breadcrumb">
					<li><a href="/twenty/main">Home</a></li>
					<li class="active">${search }</li>
				</ol>
			</div>
		</div>
	</div>
	<!--end-breadcrumbs-->
	<!--prdt-starts-->
	<div class="product">
		<div class="container">
			<div class="product-top">
				<div class="product-one">
					<c:forEach var="pensionList" items="${pensionList}">
						<div class="col-md-4 product-left" style="margin-bottom: 20px;">
							<div class="product-main simpleCart_shelfItem">
								<a href="/twenty/pensionView?pen_name=${pensionList.pen_name }"><img
									src="/twenty/resources/pension_images/${pensionList.pen_image1 }"
									width="350" height="200" />
									<h4>${pensionList.pen_name }</h4></a>
								<h5>${pensionList.pen_code }</h5>
								<br>
								<h5>${pensionList.content }</h5>
								<!-- 상품평 별점 -->
								<c:set var="avgnum" value="${pensionList.avgstar }" />
								<div class="so">
									<c:if test='${fn:substring(avgnum, 0, 1) == 0}'>
      &nbsp;<img src="/twenty/resources/images/goods/star_off2.gif"
											border="0">
										<img src="/twenty/resources/images/goods/star_off2.gif"
											border="0">
										<img src="/twenty/resources/images/goods/star_off2.gif"
											border="0">
										<img src="/twenty/resources/images/goods/star_off2.gif"
											border="0">
										<img src="/twenty/resources/images/goods/star_off2.gif"
											border="0">
									</c:if>
									<c:if test='${fn:substring(avgnum, 0, 1) == 1}'>
      &nbsp;<img src="/twenty/resources/images/goods/star_on2.gif"
											border="0" />
										<img src="/twenty/resources/images/goods/star_off2.gif"
											border="0" />
										<img src="/twenty/resources/images/goods/star_off2.gif"
											border="0" />
										<img src="/twenty/resources/images/goods/star_off2.gif"
											border="0" />
										<img src="/twenty/resources/images/goods/star_off2.gif"
											border="0" />&nbsp;<strong>불만족</strong>
									</c:if>
									<c:if test='${fn:substring(avgnum, 0, 1) == 2}'>
     &nbsp; <img src="/twenty/resources/images/goods/star_on2.gif"
											border="0">
										<img src="/twenty/resources/images/goods/star_on2.gif"
											border="0">
										<img src="/twenty/resources/images/goods/star_off2.gif"
											border="0">
										<img src="/twenty/resources/images/goods/star_off2.gif"
											border="0">
										<img src="/twenty/resources/images/goods/star_off2.gif"
											border="0">&nbsp;<strong>미흡</strong>
									</c:if>
									<c:if test='${fn:substring(avgnum, 0, 1) == 3}'>
     &nbsp; <img src="/twenty/resources/images/goods/star_on2.gif"
											border="0">
										<img src="/twenty/resources/images/goods/star_on2.gif"
											border="0">
										<img src="/twenty/resources/images/goods/star_on2.gif"
											border="0">
										<img src="/twenty/resources/images/goods/star_off2.gif"
											border="0">
										<img src="/twenty/resources/images/goods/star_off2.gif"
											border="0">&nbsp;<strong>보통</strong>
									</c:if>
									<c:if test='${fn:substring(avgnum, 0, 1) == 4}'>
     &nbsp; <img src="/twenty/resources/images/goods/star_on2.gif"
											border="0">
										<img src="/twenty/resources/images/goods/star_on2.gif"
											border="0">
										<img src="/twenty/resources/images/goods/star_on2.gif"
											border="0">
										<img src="/twenty/resources/images/goods/star_on2.gif"
											border="0">
										<img src="/twenty/resources/images/goods/star_off2.gif"
											border="0">&nbsp;<strong>만족</strong>
									</c:if>
									<c:if test='${fn:substring(avgnum, 0, 1) == 5}'>
       &nbsp; <img src="/twenty/resources/images/goods/star_on2.gif"
											border="0">
										<img src="/twenty/resources/images/goods/star_on2.gif"
											border="0">
										<img src="/twenty/resources/images/goods/star_on2.gif"
											border="0">
										<img src="/twenty/resources/images/goods/star_on2.gif"
											border="0">
										<img src="/twenty/resources/images/goods/star_on2.gif"
											border="0">&nbsp;<strong>아주만족</strong>
									</c:if>

									<!-- 상품평 작성자/등록일 -->
								</div>
								<h3>${pensionList.avgstar }</h3>

							</div>
						</div>
					</c:forEach>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<!--product-end-->
</body>
</html>