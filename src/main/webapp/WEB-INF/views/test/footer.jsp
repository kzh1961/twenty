<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!-- 하단정보 시작 -->
<footer id="footer" class="footer_new">
        <div class="footer-layer1">
            <a href="/np/etc/introduce">회사소개</a>
            <a href="http://www.coupang.com/np/jobs" target="_blank">인재채용</a>
            <a href="https://wing.coupang.com/vendor/joining/welcome?inflow=WEB_HEADER_B">입점 / 제휴문의</a>
            <a href="http://cs.coupang.com/customerCenter/notice/list">공지사항</a>
            <a href="http://cs.coupang.com/customerCenter/voiceOfCustomer">고객의 소리</a>
            <a href="/np/policies/terms">이용약관</a>
            <a href="/np/policies/privacy"><b>개인정보 처리방침</b></a>
            <a href="/np/safety">신뢰관리센터</a>
        </div>
        <div class="footer-layer2">
            <h1><a href="/" title="COUPANG">COUPANG</a></h1>
            <div class="footer-content">
                <address>
                    (주)포워드벤처스 | 대표이사 : 김범석<br>
                    서울시 강남구 테헤란로 501 <br>
                    사업자 등록번호 : 120-88-00767 <br>
                    통신판매업신고 : 2013-서울강남-02121<br>
                    <a href="http://www.ftc.go.kr/info/bizinfo/communicationViewPopup.jsp?wrkr_no=1208800767" target="_blank" class="licensee" title="사업자정보 확인">사업자정보 확인 &gt;</a>
                </address>
                <div class="contact-info">
                    <a href="/csFaq.pang" class="call-center" title="365 고객센터">
                        <strong>365고객센터</strong> | 전자금융거래분쟁처리담당<br>
                        <em>1577-7011</em>
                        서울시 종로구 창경궁로 120 종로플레이스 12층<br>
                        <span class="contact-fax">Fax : 02-3441-7011 | email : mediation@coupang.com</span>
                    </a>
                </div>
                <p class="safe-service">
                    <strong>우리은행 채무지급보증 안내</strong><br>
            <span>
                당사는 고객님이 현금 결제한 금액에 대해<br>우리은행과 채무지급보증 계약을 체결하여<br>안전거래를 보장하고 있습니다.<br>
            </span>
                    <a href="javascript:;" id="serviceCheck" class="service-check" title="서비스 가입사실 확인">서비스 가입사실 확인 &gt;</a>
                </p>
            </div>
        </div>
        <div class="footer-layer3 slide-unit">
    <span class="slide-navi">
        <a href="javascript:;" class="move prev">prev</a>
        <a href="javascript:;" class="move next">next</a>
    </span>
            <div class="slide-area">
                <ul class="award-list">
                    <li style="display: list-item;"><a href="https://my.coupang.com/customerCenter/notice/2993" class="award-link1" title="2014국가고객만족지수(NCSI) 소셜커머스 1위">2014국가고객만족지수(NCSI) 소셜커머스 1위</a></li>
                    <li style="display: list-item;"><a href="https://my.coupang.com/customerCenter/notice/2810" class="award-link2" title="한국산업의고객만족도 소셜커머스부분 1위">한국산업의고객만족도 소셜커머스부분 1위</a></li>
                    <li style="display: list-item;"><a href="https://my.coupang.com/customerCenter/notice/2345" class="award-link3" title="정보보호 관리체계 ISMS 인증획득">정보보호 관리체계 ISMS 인증획득</a></li>
                    <li style="display: list-item;"><a href="https://my.coupang.com/customerCenter/notice/2344" class="award-link4" title="개인정보보호 관리체계 PIMS 인증획득">개인정보보호 관리체계 PIMS 인증획득</a></li>
                    <li style="display: list-item;"><a href="https://my.coupang.com/customerCenter/notice/2473" class="award-link5" title="정보보안 국제표준 ISO27001 인증획득">정보보안 국제표준 ISO27001 인증획득</a></li>
                    <li style="display: none;"><a href="http://www.bsigroup.com/ko-KR/bs-10008-electronic-information-management/" class="award-link7" title="전자정보경영시스템 BS100008인증 획득">전자정보경영시스템 BS100008인증 획득</a></li>
                    <li style="display: none;"><a href="https://my.coupang.com/customerCenter/notice/2206" class="award-link8" title="한국 소비자 만족지수 1위">한국 소비자 만족지수 1위</a></li>
                    <li style="display: none;"><a href="https://my.coupang.com/customerCenter/notice/2577" class="award-link9" title="한국능률협회컨설팅 주관 한국산업의 고객만족 1위">한국능률협회컨설팅 주관 한국산업의 고객만족 1위</a></li>
                    <li style="display: none;"><a href="https://my.coupang.com/customerCenter/notice/2192" class="award-link10" title="국가 브랜드 대상 수상">국가 브랜드 대상 수상</a></li>
                    <li style="display: none;"><a href="https://my.coupang.com/customerCenter/notice/2447" class="award-link11" title="제2회 모바일 브랜드 대상">제2회 모바일 브랜드 대상</a></li>
                    <li style="display: none;"><a href="http://www.kolsa.or.kr/" class="award-link12" target="_blank" title="한국온라인쇼핑협회 정회원사">한국온라인쇼핑협회 정회원사</a></li>
                    <li style="display: none;"><a href="http://www.eprivacy.or.kr/" class="award-link13" title="개인정보우수사이트 ePRIVACY인증획득">개인정보우수사이트 ePRIVACY인증획득</a></li>
                </ul>
            </div>
        </div>
        <div class="footer-layer4">
            <div class="coupang-copyright">
                <p class="info">개별 판매자가 등록한 마켓플레이스(오픈마켓) 상품에 대한 광고, 상품주문, 배송 및 환불의 의무와 책임은 각 판매자가 부담하고,<br> 이에 대하여 쿠팡은 통신판매중개자로서 통신판매의 당사자가 아니므로 일체 책임을 지지 않습니다.</p>
                <p class="copyright">Copyright Forward Ventures Co.,Ltd. All rights reserved.</p>
                <ul class="sns-link">
                    <li><a href="http://cafe.naver.com/coupanglife" target="_blank" class="cafe" title="쿠팡 카페">쿠팡 카페</a></li>
                    <li><a href="https://www.facebook.com/Coupang.korea" target="_blank" class="facebook" title="쿠팡 페이스북">쿠팡 페이스북</a></li>
                    <li><a href="https://story.kakao.com/ch/coupang" target="_blank" class="kakaostory" title="쿠팡 카카오스토리">쿠팡 카카오스토리</a></li>
                    <li><a href="http://twitter.com/coupang" target="_blank" class="twitter" title="쿠팡 트위터">쿠팡 트위터</a></li>
                    <li><a href="http://blog.naver.com/coupang_kr" target="_blank" class="blog" title="쿠팡 블로그">쿠팡 블로그</a></li>
                    <li><a href="https://www.instagram.com/coupang_official/" target="_blank" class="instagram" title="쿠팡 인스타그램">쿠팡 인스타그램</a></li>
                </ul>
            </div>
        </div>
    </footer>
<!-- <div id="footer">
	<div class="foot_area">
		<div class="ft_box">
			<div class="xans-layout-footer">
				<div class="utilMenu">            
					<div class="utilMenu_unit">  
						<h4>카테고리</h4>
						<ul>
							<li><a href="/pet/goods/goodsCategoryList.dog?goods_category=0"><span>사료</span></a></li>
							<li><a href="/pet/goods/goodsCategoryList.dog?goods_category=1"><span>간식</span></a></li>
							<li><a href="/pet/goods/goodsCategoryList.dog?goods_category=2"><span>패션용품</span></a></li>
							<li><a href="/pet/goods/goodsCategoryList.dog?goods_category=3"><span>장난감</span></a></li>
							<li><a href="/pet/goods/goodsCategoryList.dog?goods_category=4"><span>생활/잡화</span></a></li>
							<li><a href="/product/list.html?cate_no=65"><span>위생/배변</span></a></li>
							<li><a href="/product/list.html?cate_no=66"><span>미용/목욕</span></a></li>
							<li><a href="/product/list.html?cate_no=67"><span>집/인테리어</span></a></li>
							<li><a href="/product/list.html?cate_no=68"><span>이동장/유모차</span></a></li>
							<li><a href="/product/list.html?cate_no=69"><span>야외/훈련</span></a></li>
							<li><a href="/product/list.html?cate_no=70"><span>건강관리</span></a></li>
						</ul>
					</div>
					<div class="utilMenu_unit">
						<h4>커뮤니티</h4>
						<ul>
							<li><a href="/pet/notice/noticeList.dog"><span>공지사항</span></a></li>
							<li><a href="/pet/pet_img/pet_imgList.dog"><span>마이펫갤러리</span></a></li>
							<li><a href="/pet/pet/petList.dog"><span>분양게시판</span></a></li>
							<li><a href="/pet/QnA/QnAList.dog"><span>Q&amp;A</span></a></li>
							<li><a href="/pet/review/reviewList.dog"><span>구매후기</span></a></li>
							<li><a href="/product/list.html?cate_no=400"><span>기부나무</span></a></li>
							<li><a href="/board/magazin/main01.html"><span>매거진</span></a></li>
						</ul>
					</div>
					<div class="utilMenu_unit">
						<h4>쇼핑몰정보</h4>
						<ul>
							<li><a href="#">이용약관</a></li>
							<li><a href="#">개인정보취급방침</a></li>
						</ul>
					</div>
					
					
					<div class="utilMenu_unit">
						<h4>고객센터</h4>
						<ul>
							<li class="phone">070-4675-2233</li>
							<li><span><strong>평일: 오전09:00~오후07:00</strong></span><br/><span><strong>점심시간: 12:00~13:00</strong></span><br/><span><strong>토,일,공휴일 휴무</strong></span></li>
						</ul>
						<h4 style="padding:12px 0 0;">무통장입금</h4>
						<p class="bank_info"><strong>외환은행 630-009567-131<br/>예금주 : (주)D편한세상</strong></p>
					</div>
				</div>
			
				<div class="qrcode">
					<p><img src="/pet/resources/images/SkinImg/dlogo.png"/></p>
					<div class="utilMenu_unit2">
						<h4 class="escrow_title">개편한세상 구매안전 서비스</h4>
						<p class="escrow_info">오키독키에서는 구매시 회원님의 권리를<br/> 보호하는 서비스 구매안전에 가입되어 있습니다.<br/>
					</div>
				</div>
			</div>
		</div>
	</div>
</div> --><!--// 라인업 끝 -->    

 