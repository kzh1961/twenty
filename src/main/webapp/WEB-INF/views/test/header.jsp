<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<title>Twenty</title>
<link href="/twenty/resources/web/css/bootstrap.css" rel="stylesheet"
	type="text/css" media="all" />
<!--jQuery(necessary for Bootstrap's JavaScript plugins)-->
<script src="/twenty/resources/web/js/jquery-1.11.0.min.js"></script>
<!--Custom-Theme-files-->
<!--theme-style-->
<link href="/twenty/resources/web/css/style.css" rel="stylesheet"
	type="text/css" media="all" />
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords"
	content="Luxury Watches Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript">
	
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 

</script>
<!--start-menu-->
<script src="/twenty/resources/web/js/simpleCart.min.js">
	
</script>
<link href="/twenty/resources/web/css/memenu.css" rel="stylesheet"
	type="text/css" media="all" />
<script type="text/javascript" src="/twenty/resources/web/js/memenu.js"></script>
<script>
	$(document).ready(function() {
		$(".memenu").memenu();
	});
</script>
<!--dropdown-->
<script src="/twenty/resources/web/js/jquery.easydropdown.js"></script>
</head>
<body>
	<!--top-header-->
	<div class="top-header">
		<div class="container">
			<div class="top-header-main">
				<div class="col-md-6 top-header-left">
					<div class="drop">
						<div class="box">
							<a href="${contextPath}/twenty/member/memberForm">회원가입</a>
						</div>
						<div class="box">
							<a href="/twenty/member/loginForm">로그인</a>
						</div>
						<div class="box">
							<select tabindex="4" class="dropdown drop" onchange="location.href=this.value">
								<option value="" class="label">고객센터 :</option>
								<option value="/twenty/notice/noticeList">공지사항</option>
								<option value="/twenty/qna/qnaList">Q&amp;A</option>
							</select>
						</div>
						<div class="box1">
							<select tabindex="4" class="dropdown">
								<option value="" class="label">마이페이지 :</option>
								<option value="1">주문목록</option>
								<option value="2">예약목록</option>
								<option value="3">취소/반품</option>
							</select>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="col-md-6 top-header-left">
					<div class="cart box_1">
						<a href="checkout.html">
							<div class="total">
								<span class="simpleCart_total"></span>
							</div> <img src="/twenty/resources/web/images/cart-1.png" alt="" />
						</a>
						<p>
							<a href="javascript:;" class="simpleCart_empty">장바구니</a>
						</p>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!--top-header-->
	<!--start-logo-->
	<div class="logo">
		<a href="index.html"><h1>Twenty</h1></a>
	</div>
	<!--start-logo-->
	<!--bottom-header-->
	<div class="header-bottom">
		<div class="container">
			<div class="header">
				<div class="col-md-9 header-left">
					<div class="top-nav">
						<ul class="memenu skyblue">
							<li class="active"><a href="index.html">Home</a></li>
							<li class="grid"><a href="#">패션의류/잡화</a>
								<div class="mepanel">
									<div class="row">
										<div class="col1 me-one">
											<h4>패션의류</h4>
											<ul>
												<li><a href="products.html">여성패션</a></li>
												<li><a href="products.html">남성패션</a></li>
												<li><a href="products.html">여아패션</a></li>
												<li><a href="products.html">남아패션</a></li>
											</ul>
										</div>
									</div>
								</div></li>
							<li class="grid"><a href="#">뷰티</a>
								<div class="mepanel">
									<div class="row">
										<div class="col1 me-one">
											<h4>Shop</h4>
											<ul>
												<li><a href="products.html">New Arrivals</a></li>
												<li><a href="products.html">Blazers</a></li>
												<li><a href="products.html">Swem Wear</a></li>
												<li><a href="products.html">Accessories</a></li>
												<li><a href="products.html">Handbags</a></li>
												<li><a href="products.html">T-Shirts</a></li>
												<li><a href="products.html">Watches</a></li>
												<li><a href="products.html">My Shopping Bag</a></li>
											</ul>
										</div>

									</div>
								</div></li>
							<li class="grid"><a href="#">유아동</a>
								<div class="mepanel">
									<div class="row">
										<div class="col1 me-one">
											<h4>Shop</h4>
											<ul>
												<li><a href="products.html">New Arrivals</a></li>
												<li><a href="products.html">Blazers</a></li>
												<li><a href="products.html">Swem Wear</a></li>
												<li><a href="products.html">Accessories</a></li>
												<li><a href="products.html">Handbags</a></li>
												<li><a href="products.html">T-Shirts</a></li>
												<li><a href="products.html">Watches</a></li>
												<li><a href="products.html">My Shopping Bag</a></li>
											</ul>
										</div>
									</div>
								</div></li>
							<li class="grid"><a href="#">식품</a>
								<div class="mepanel">
									<div class="row">
										<div class="col1 me-one">
											<h4>Shop</h4>
											<ul>
												<li><a href="products.html">New Arrivals</a></li>
												<li><a href="products.html">Blazers</a></li>
												<li><a href="products.html">Swem Wear</a></li>
												<li><a href="products.html">Accessories</a></li>
												<li><a href="products.html">Handbags</a></li>
												<li><a href="products.html">T-Shirts</a></li>
												<li><a href="products.html">Watches</a></li>
												<li><a href="products.html">My Shopping Bag</a></li>
											</ul>
										</div>
									</div>
								</div></li>
							<li class="grid"><a href="#">가구/홈</a>
								<div class="mepanel">
									<div class="row">
										<div class="col1 me-one">
											<h4>Shop</h4>
											<ul>
												<li><a href="products.html">New Arrivals</a></li>
												<li><a href="products.html">Blazers</a></li>
												<li><a href="products.html">Swem Wear</a></li>
												<li><a href="products.html">Accessories</a></li>
												<li><a href="products.html">Handbags</a></li>
												<li><a href="products.html">T-Shirts</a></li>
												<li><a href="products.html">Watches</a></li>
												<li><a href="products.html">My Shopping Bag</a></li>
											</ul>
										</div>
									</div>
								</div></li>
							<li class="grid"><a href="#">가전/디지털</a>
								<div class="mepanel">
									<div class="row">
										<div class="col1 me-one">
											<h4>Shop</h4>
											<ul>
												<li><a href="products.html">New Arrivals</a></li>
												<li><a href="products.html">Blazers</a></li>
												<li><a href="products.html">Swem Wear</a></li>
												<li><a href="products.html">Accessories</a></li>
												<li><a href="products.html">Handbags</a></li>
												<li><a href="products.html">T-Shirts</a></li>
												<li><a href="products.html">Watches</a></li>
												<li><a href="products.html">My Shopping Bag</a></li>
											</ul>
										</div>
									</div>
								</div></li>
							<li class="grid"><a href="#">숙박</a>
								<div class="mepanel">
									<div class="row">
										<div class="col1 me-one">
											<h4>펜션</h4>
											<ul>
												<li><a href="products.html">서울</a></li>
												<li><a href="products.html">경기</a></li>
												<li><a href="products.html">인천</a></li>
												<li><a href="products.html">충청도</a></li>
												<li><a href="products.html">강원도</a></li>
												<li><a href="products.html">전라도</a></li>
												<li><a href="products.html">경상도</a></li>
												<li><a href="products.html">제주도</a></li>
											</ul>
										</div>
										<div class="col1 me-one">
											<h4>호텔</h4>
											<ul>
												<li><a href="products.html">서울</a></li>
												<li><a href="products.html">경기</a></li>
												<li><a href="products.html">인천</a></li>
												<li><a href="products.html">충청도</a></li>
												<li><a href="products.html">강원도</a></li>
												<li><a href="products.html">전라도</a></li>
												<li><a href="products.html">경상도</a></li>
												<li><a href="products.html">제주도</a></li>
											</ul>
										</div>
									</div>
								</div></li>
						</ul>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="col-md-3 header-right">
					<div class="search-bar">
						<input type="text" value="Search" onfocus="this.value = '';"
							onblur="if (this.value == '') {this.value = 'Search';}">
						<input type="submit" value="">
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!--bottom-header-->