<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>회원가입</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="text/javascript">
function validateForm() {
    var x = document.forms["joinform"]["goods_name"].value;
    if (x == null || x == "") {
        alert("상품명은 입력해야합니다");
        return false;
    }
}
</script>
</head>
<body>
	<div class="container">
		<div class="panel panel-default">
			<div class="panel-body">
				<center><h2>회원가입</h2></center>
			</div>
			<form:form commandName="goods" class="form-horizontal" role="form" method="post" action="adminGoodsWrite" >
			<div class="xans-element- xans-member xans-member-join">
			
			<div class="form-group" id="divId">
                <label for="inputId" class="col-lg-2 control-label">아이디</label>
                <div class="col-lg-10">
                    <form:input type="text" class="form-control onlyAlphabetAndNumber" path="goods_category" data-rule-required="true" placeholder="아이디" maxlength="30"/>
                    <font color="red"><form:errors path="goods_category" /></font>
					<font color="red"><form:errors element="goods_category" /></font>
                </div>
            </div>

            <div class="form-group" id="divName">
                <label for="inputName" class="col-lg-2 control-label">이름</label>
                <div class="col-lg-10">
                    <form:input type="text" class="form-control onlyHangul" path="goods_name" data-rule-required="true" placeholder="한글만 입력 가능합니다." maxlength="15"/>
                    <font color="red"><form:errors path="goods_name" /></font>
                </div>
            </div>
            
            <div class="form-group" id="divPhoneNumber">
                <label for="inputPhoneNumber" class="col-lg-2 control-label">휴대폰 번호</label>
                <div class="col-lg-10">
                    <form:input type="tel" class="form-control onlyNumber" path="goods_amount" data-rule-required="true" placeholder="-를 제외하고 숫자만 입력하세요." maxlength="11"/>
                    <font color="red"><form:errors path="goods_amount" /></font>
                </div>
                </div>
               
      
                <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                    <button type="submit" class="btn btn-default">가입완료</button>
                </div>
            </div>
            
                </div>
         </form:form>
            
            
</div>
	</div>
</body>


            <form action="adminGoodsWrite" enctype="multipart/form-data" method="post">
카테고리 <input type="text" name="goods_category" /><br/>
상품 이름<input type="text" name="goods_name" /><br/>
상품 수량<input type="text" name="goods_amount" /><br/>
상품 가격<input type="text" name="goods_price" /><br/>
상품 부제목<input type="text" name="goods_subtitle" /><br/>
이미지1<input type="file" name="file1" /><br/>
이미지2<input type="file" name="file2" /><br/>
이미지3<input type="file" name="file3" /><br/>
<input type="submit" value="전송" /></form>


<!-- 메뉴 시작 -->

<%-- <div>    
	<h1 class="page-header">상품등록</h1>
</div>

<div>
	<div>
		<div>상품등록 페이지입니다. 빠짐없이 입력하셔야합니다</div>
			<div>
				<form commandName="goods" action="adminGoodsInsert" enctype="multipart/form-data" method="post"name="joinform" onsubmit="return validateForm()">	
						<div class="form-group">
                            <label>카테고리</label>	                            
                               <select path="goods_category" class="form-control" style="width:initial;" >
								<option value="0" label="사료" />
								<option value="1" label="간식" />
								<option value="2" label="패션의류" />
								<option value="3" label="목줄/야외" />
								<option value="4" label="생활/잡화" />
							</select>
                        </div>
                        <div class="form-group">
                            <label>상품명</label>
                            <form:input type="text" name="goods_name" class="form-control" path="goods_name"  placeholder="상품명을 입력하세요" style="width:500px;"/>
                        </div>
                        <div class="form-group">
                            <label>상품이미지</label>
                            <input type="file" name="file[0]" size="30" value=''/>
                            <p class="help-block">메인상품 이미지 입니다 800x800 사이즈 권장합니다</p>
                        </div>
                        <div class="form-group">
                            <label>상품수량</label>
                            <form:input type="text" class="form-control" path="goods_amount"  placeholder="상품수량을 입력해주세요" value="1" style="width:107px;"/>
                        </div>
                        <div class="form-group">
                            <label>판매가격</label>
                            <form:input type="text" class="form-control" path="goods_price" style="width:initial;"/>
                            <p class="help-block">판매가격 입력하세요. 0원으로 그대로 갈 경우 큰일납니다</p>
                        </div>
                        <div class="form-group">
                            <label>상품 내용 이미지</label><!-- goods_contentimage -->
                            <input type="file" name="file[1]" size="30" value=''/>
                            <p class="help-block">상품설명 이미지 입니다 1000x(2500~3800)사이즈 권장</p>
                        </div>
                        <div>
                            <label>배송 내용 이미지</label><!-- goods_delevimage -->
                            <input type="file" name="file[2]" size="30" value=''/>
                            <p class="help-block">상품설명 이미지 입니다 1000x1000사이즈 권장</p>
                        </div>
                        <div class="form-group">
                            <label>상품구분</label>	                            
                               <form:select path="goods_best" class="form-control" style="width:initial;" >
								<form:option value="0" label="기본" />
								<form:option value="1" label="추천" />
								<form:option value="2" label="베스트" />
								<form:option value="3" label="신상" />
							</form:select>
							<p class="help-block">선택옵션에 따라 추천, 베스트, 신상으로 구분되며 메인페이지 출력옵션에 사용됩니다</p>
                        </div>
						<button type="submit" class="btn btn-success">상품등록</button>
						<button type="reset" class="btn btn-default">작성취소</button>					
				</form>
			</div>
	</div>
</div>


 --%>