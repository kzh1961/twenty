<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<head>
<script type="text/javascript">
	var no;
	function onDelete(no) {
		if (confirm("삭제하겠습니까?") == true) {
			location.href = 'adminNoticeDelete?notice_num=' + no;
		} else {
			return;
		}
	}
	function onModify(no) {
		location.href = 'adminNoticeModifyForm?notice_num=' + no;
	};
</script>
</head>
<body>
	<!-- 핵심 로직 -->
	<h1>공지사항 리스트</h1>
	<div class="container-fluid">
		<hr>
		<!-- 핵심 로직 -->
		<div class="widget-box">
			<div class="widget-title">
				<span class="icon"><i class="icon-th"></i></span>
				<h5>Notice List</h5>
			</div>


			<div class="dataTables_filter" id="example_filter">
				<label>Search: <input type="text" aria-controls="example"></label>
			</div>


			<div class="widget-content nopadding">
				<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper"
					role="grid">
					<div class="">
						<div id="DataTables_Table_0_length" class="dataTables_length"></div>
					</div>
					<table class="table table-bordered data-table dataTable"
						id="DataTables_Table_0">
						<thead>
							<tr role="row">
								<th style="width: 4%; text-align: center;">번호</th>
								<th style="width: 35%; text-align: center;">제목</th>
								<th style="width: 40%; text-align: center;">내용</th>
								<th style="width: 6%; text-align: center;">등록일자</th>
								<th style="width: 6%; text-align: center;">조회수</th>
								<th style="width: 9%; text-align: center;">관리</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="list" items="${noticeList}">
								<tr class="gradeA even" role="row">
									<td style="text-align: center; vertical-align: middle;">${list.notice_num}</td>
									<td style="text-align: center; vertical-align: middle;">${list.subject}</td>
									<td style="text-align: center; vertical-align: middle;">${list.content}</td>
									<td style="text-align: center; vertical-align: middle;"><fmt:formatDate
											value="${list.regdate}" pattern="YY.MM.dd" /></td>
									<td style="text-align: center; vertical-align: middle;">${list.readcount}</td>
									<td style="text-align: center; vertical-align: middle;"><input
										type="image"
										src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/92/Cog_font_awesome.svg/32px-Cog_font_awesome.svg.png"
										onclick="onModify(${list.notice_num})">&nbsp;&nbsp; <input
										type="image"
										src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Trash_font_awesome.svg/32px-Trash_font_awesome.svg.png"
										onclick="onDelete(${list.notice_num})"></td>
								</tr>
							</c:forEach>
							<!--  등록된 공지사항이 없을때 -->
							<c:if test="${fn:length(noticeList) le 0}">
								<tr>
									<td colspan="9" style="text-align: center;">등록된 공지사항이 없습니다</td>
								</tr>
							</c:if>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</body>