<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<head>
<script type="text/javascript">
	function delchk(pen_name,room_num) {
		var con = confirm("삭제하시겠습니까?");
		if(con==true){
			location.href="adminRoomDelete?pen_name="+pen_name+"&room_num="+room_num;
		}
		return false;
	}
</script>

<!-- 핵심 로직 -->
<h1>펜션 룸 리스트</h1>
<div class="container-fluid">
	<hr>
<!-- 핵심 로직 -->


	<div class="widget-box">
		<div class="widget-title">
			<span class="icon"><i class="icon-th"></i></span>
			<h5>Room List</h5>
		</div>


		<div class="dataTables_filter" id="example_filter">
			<label>Search: <input type="text" aria-controls="example"></label>
		</div>


		<div class="widget-content nopadding">
			<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper"
				role="grid">
				<div class="">
					<div id="DataTables_Table_0_length" class="dataTables_length"></div>
				</div>
				<table class="table table-bordered data-table dataTable"
					id="DataTables_Table_0">
					<thead>
									<tr>
								<th style="width: 7%; text-align: center;">룸 번호</th>
								<th style="width: 10%; text-align: center;">펜션 이름</th>
								<th style="width: 8%; text-align: center;">평일 가격</th>
								<th style="width: 8%; text-align: center;">주말 가격</th>
								<th style="width: 10%; text-align: center;">최대인원</th>
								<th style="width: 10%; text-align: center;">룸 사진1</th>
								<th style="width: 10%; text-align: center;">룸 사진2</th>
								<th style="width: 25%; text-align: center;">내용</th>
								<th style="width: 10%; text-align: center;">관리</th>
							</tr>
								</thead>
					<tbody>
								<c:forEach var="adminRoomList"  items="${adminRoomList}" varStatus="stat">															
									<tr class="gradeA even" role="row">
										<td style="text-align: center; vertical-align: middle;">${adminRoomList.room_num}</td>
									<td style="text-align: center; vertical-align: middle;">${adminRoomList.pen_name}</td>
									<td style="text-align: center; vertical-align: middle;">${adminRoomList.price1}
											</td>
									<td style="text-align: center; vertical-align: middle;">${adminRoomList.price2}
											</td>
									<td style="text-align: center; vertical-align: middle;">${adminRoomList.max_people}</td>		
											<td style="text-align: center; vertical-align: middle;"><img
										src="/twenty/resources/room_images/${adminRoomList.room_image1}"
										style="width:60px;height:60px;" alt=""
										onerror="this.src='/twenty/resources/images/noimg_130.gif'" /></td>
										<td style="text-align: center; vertical-align: middle;"><img
										src="/twenty/resources/room_images/${adminRoomList.room_image2}"
										style="width:60px;height:60px;" alt=""
										onerror="this.src='/twenty/resources/images/noimg_130.gif'" /></td>
										<td style="text-align: center; vertical-align: middle;">${adminRoomList.content}</td>								
										<td style="text-align: center; vertical-align: middle;"><input type="image" onclick="javascript:location.href='adminRoomModifyForm?pen_name=${adminRoomList.pen_name}&room_num=${adminRoomList.room_num }'"
											src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/92/Cog_font_awesome.svg/32px-Cog_font_awesome.svg.png"></a>&nbsp;&nbsp;
									<input type="image"  src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Trash_font_awesome.svg/32px-Trash_font_awesome.svg.png"
										onclick="return delchk('${adminRoomList.pen_name}','${adminRoomList.room_num }')"></td>
									
									
									</tr>
								</c:forEach>
								<!--  등록된 Room이 없을때 -->
									<c:if test="${fn:length(adminRoomList) le 0}">
										<tr><td colspan="9" style="text-align:center;">등록된 Room 없습니다</td></tr>
									</c:if> 
								</tbody>
				</table>
				<!-- <div><input class="btn-primary" type="submit" value="글쓰기" onclick="javascript:location.href='adminNoticeWriteForm'"  /></div> -->
				
				
			</div>
		</div>
	</div>
	
	
	
	
	
	
	
	
	
	
	
	
	
	<%-- <%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<head>
<script type="text/javascript">
	function delchk() {
		return confirm("삭제하시겠습니까?");
	}
</script>

<!-- 핵심 로직 -->
<h1>상품관리</h1>
<div class="container-fluid">
	<hr>
<!-- 핵심 로직 -->


	<div class="widget-box">
		<div class="widget-title">
			<span class="icon"><i class="icon-th"></i></span>
			<h5>Goods List</h5> <input class="btn-primary" type="submit" value="글쓰기" onclick="javascript:location.href='adminNoticeWriteForm'"  />
		</div>


		<div class="dataTables_filter" id="example_filter">
			<label>Search: <input type="text" aria-controls="example"></label>
		</div>


		<div class="widget-content nopadding">
			<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper"
				role="grid">
				<div class="">
					<div id="DataTables_Table_0_length" class="dataTables_length"></div>
				</div>
				<table class="table table-bordered data-table dataTable"
					id="DataTables_Table_0">
					<thead>
							<tr>
								<th style="width: 5%; text-align: center;">룸 번호</th>
								<th style="width: 10%; text-align: center;">펜션 이름</th>
								<th style="width: 10%; text-align: center;">평일 가격</th>
								<th style="width: 10%; text-align: center;">주말 가격</th>
								<th style="width: 10%; text-align: center;">최대인원</th>
								<th style="width: 15%; text-align: center;">룸 사진1</th>
								<th style="width: 15%; text-align: center;">룸 사진2</th>
								<th style="width: 15%; text-align: center;">내용</th>
								<th style="width: 10%; text-align: center;">관리</th>
							</tr>
						
							
							
							
						</thead>
					<tbody>
							<c:forEach var="adminRoomList" items="${adminRoomList}">
								<c:url var="viewURL" value="pensionView">
									<c:param name="pen_name" value="${pensionList.pen_name}" />


								</c:url>
								<tr>
									<td style="text-align: center; vertical-align: middle;">${adminRoomList.room_num}</td>
									<td style="text-align: center; vertical-align: middle;">${adminRoomList.pen_name}</td>
									<td style="text-align: center; vertical-align: middle;"><fmt:formatNumber value="${adminRoomList.price1}"
											type="number" /></td>
									<td style="text-align: center; vertical-align: middle;"><fmt:formatNumber value="${adminRoomList.price2}"
											type="number" /></td>
									<td style="text-align: center; vertical-align: middle;">${adminRoomList.max_people}</td>
									
									<td style="text-align: center; vertical-align: middle;"><img
										src="/twenty/resources/room_images/${adminRoomList.room_image1}"
										width="60" height="60" alt=""
										onerror="this.src='/twenty/resources/images/noimg_130.gif'" /></td>
									<td style="text-align: center; vertical-align: middle;"><img
										src="/twenty/resources/room_images/${adminRoomList.room_image2}"
										width="60" height="60" alt=""
										onerror="this.src='/twenty/resources/images/noimg_130.gif'" /></td>
										<td style="text-align: center; vertical-align: middle;">${adminRoomList.content}</td>
										
										<td style="text-align: center; vertical-align: middle;"><a
										href="/twenty/admin/adminGoodsModifyForm?goods_num=${adminRoomList.goods_num }"><input
											type="image"
											src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/92/Cog_font_awesome.svg/32px-Cog_font_awesome.svg.png"></a>&nbsp;&nbsp;
										<c:url var="viewURL2" value="adminGoodsDelete">
											<c:param name="goods_num" value="${adminRoomList.goods_num }" />
										</c:url> <a href="${viewURL2}"><input type="image"
											src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Trash_font_awesome.svg/32px-Trash_font_awesome.svg.png"
											onclick="return delchk()"></a></td>
										
										
										
								
								</tr>
							</c:forEach>
							<!--  등록된 상품이 없을때 -->
							<c:if test="${fn:length(adminRoomList) le 0}">
								<tr>
									<td>등록된 상품이 없습니다</td>
								</tr>
							</c:if>
						</tbody>
				</table>
				<!-- <div><input class="btn-primary" type="submit" value="글쓰기" onclick="javascript:location.href='adminNoticeWriteForm'"  /></div> -->
				
				
			</div>
		</div>
	</div>
</div></head> --%>

















<%-- <%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<head>
<script type="text/javascript">
	function delchk() {
		return confirm("삭제하시겠습니까?");
	}
</script>

<!-- 핵심 로직 -->
<h1>공지사항</h1>
<div class="container-fluid">
	<hr>
<!-- 핵심 로직 -->


	<div class="widget-box">
		<div class="widget-title">
			<span class="icon"><i class="icon-th"></i></span>
			<h5>Room List</h5> <input class="btn-primary" type="submit" value="글쓰기" onclick="javascript:location.href='adminRoomWriteForm'"  />
		</div>


		<div class="dataTables_filter" id="example_filter">
			<label>Search: <input type="text" aria-controls="example"></label>
		</div>


		<div class="widget-content nopadding">
			<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper"
				role="grid">
				<div class="">
					<div id="DataTables_Table_0_length" class="dataTables_length"></div>
				</div>
				<table class="table table-bordered data-table dataTable"
					id="DataTables_Table_0">
					<thead>
									<tr role="row">
									
				<th style="width: 12%; text-align:center;">펜션 이름</th>
										<th style="width: 40%; text-align:center;">평일 가격</th>
										<th style="width: 12%; text-align:center;">주말 가격</th>										
										<th style="width: 12%; text-align:center;">최대 인원</th>
										<th style="width: 12%; text-align:center;">내 용</th>
										<th style="width: 12%; text-align:center;">관리</th>
									</tr>
								</thead>
					<tbody>
								<c:forEach var="adminRoomList"  items="${adminRoomList}" varStatus="stat">															
									<tr class="gradeA even" role="row">
										<td style="text-align:center;vertical-align:middle;">${adminRoomList.pen_name}</td>
										<td style="text-align:center;vertical-align:middle;">${adminRoomList.price1}</td>
										<td style="text-align:center;vertical-align:middle;">${adminRoomList.price2}</td>
										<td style="text-align:center;vertical-align:middle;">${adminRoomList.max_people}</td>
										<td style="text-align:center;vertical-align:middle;">${adminRoomList.content}</td>									
										<td style="text-align: center; vertical-align: middle;"><c:url
											var="viewURL" value="adminNoticeModify">
											<c:param name="id" value="${noticeList.notice_num}" />
										</c:url> <a href="${viewURL}"><input type="image"
											src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/92/Cog_font_awesome.svg/32px-Cog_font_awesome.svg.png"></a>&nbsp;&nbsp;
										<c:url var="viewURL2" value="adminNoticeDelete">
											<c:param name="id" value="${noticeList.notice_num}" />
										</c:url> <a href="${viewURL2}"><input type="image"
											src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Trash_font_awesome.svg/32px-Trash_font_awesome.svg.png"
											onclick="return delchk()"></a></td>
									
									
									</tr>
								</c:forEach>
								<!--  등록된 Room이 없을때 -->
									<c:if test="${fn:length(adminRoomList) le 0}">
										<tr><td colspan="9" style="text-align:center;">등록된 Room 없습니다</td></tr>
									</c:if> 
								</tbody>
				</table>
				<!-- <div><input class="btn-primary" type="submit" value="글쓰기" onclick="javascript:location.href='adminNoticeWriteForm'"  /></div> -->
				
				
			</div>
		</div>
	</div> --%>
	
	
	
	
	
	
	<%-- 
	
	<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<title>Insert title here</title>
</head>
<body>
<table style="border:1px solid #ccc">
    <colgroup>
        <col width="10%"/>
        <col width="*"/>
        <col width="15%"/>
        <col width="20%"/>
    </colgroup>
    <thead>
        <tr>
            <th scope="col">펜션이름</th>
            <th scope="col">평일가격</th>
            <th scope="col">주말가격</th>
            <th scope="col">최대인원</th>
            <th scope="col">내용</th>
        </tr>
    </thead>
    <tbody>
        <c:choose>
            <c:when test="${fn:length(adminRoomList) > 0}">
                <c:forEach items="${adminRoomList }" var="adminRoomList">
                    <tr>
                        <td>${adminRoomList.pen_name }</td>
                        <td>${adminRoomList.price1 }</td>
                        <td>${adminRoomList.price2 }</td>
                        <td>${adminRoomList.max_people }</td>
                        <td>${adminRoomList.content }</td>
                    </tr>
                </c:forEach>
            </c:when>
            <c:otherwise>
                <tr>
                    <td colspan="4">조회된 결과가 없습니다.</td>
                </tr>
            </c:otherwise>
        </c:choose>
         
    </tbody>
</table>

</body>
</html> --%>