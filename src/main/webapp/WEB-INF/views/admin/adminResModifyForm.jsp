
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<title>펜션 예약 수정</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="/resources/web/css/bootstrap.css" rel="stylesheet"
	type="text/css" />
<script type="text/javascript">
	function validateForm() {
		var a = document.forms["adminResModifyForm"]["name"].value;
		var b = document.forms["adminResModifyForm"]["people"].value;
		var c = document.forms["adminResModifyForm"]["st_day"].value;
		var d = document.forms["adminResModifyForm"]["end_day"].value;
		var g = document.forms["adminResModifyForm"]["pen_name"].value;
		var e = document.forms["adminResModifyForm"]["phone"].value;
		var f = document.forms["adminResModifyForm"]["price"].value;

		if (a == null || a == "") {
			alert("이름을 입력해야합니다");
			return false;
		} else if (b == null || b == "") {
			alert("이용자 수를 입력해야합니다");
			return false;
		} else if (c == null || c == "") {
			alert("체크인 날짜를 입력해야합니다");
			return false;
		} else if (d == null || d == "") {
			alert("체크아웃 날짜를 입력해야합니다");
			return false;
		} else if (ㅎ == null || e == "") {
			alert("펜션 이름을 입력해야합니다");
			return false;
		} else if (ㄷ == null || f == "") {
			alert("연락처를 입력해야합니다");
			return false;
		} else if (ㄹ == null || f == "") {
			alert("결제 금액을 입력해야합니다");
			return false;
		}
	}
</script>
</head>
<body>
	<!-- 핵심 로직 -->
	<h1>펜션 예약 수정</h1>
	<div class="container-fluid">
		<hr>
		<!-- 핵심 로직 -->

		<form action="adminResModify" enctype="multipart/form-data"
			method="post" name="adminResModifyForm"
			onsubmit="return validateForm()">
			<h5>예약 번호</h5>
			<input type="text" name="res_num" style="width: 500px;"
				value="${reservation.res_num }" readonly="readonly"/>
			<h5>아이디</h5>
			<input type="text" name="id" style="width: 500px;"
				value="${reservation.id }" readonly="readonly"/>
			<h5>이름</h5>
			<input type="text" name="name" style="width: 500px;"
				value="${reservation.name }" />		
			<h5>이용자 수</h5>
			<input type="text" name="people" style="width: 500px;"
				value="${reservation.people }" />		
			<h5>체크인 날짜</h5>
			<input type="text" name="st_day" style="width: 500px;"
				value="${reservation.st_day }" />	
			<h5>체크아웃 날짜</h5>
			<input type="text" name="end_day" style="width: 500px;"
				value="${reservation.end_day }" />				
			<h5>펜션 이름</h5>
			<input type="text" name="pen_name" style="width: 500px;"
				placeholder="펜션 이름을 입력해주세요." value="${reservation.pen_name }" />	
			<h5>방 번호</h5>
			<input type="text" name="room_num" style="width: 500px;"
			placeholder="방 번호를 입력해주세요." value="${reservation.room_num }" />

			<h5>연락처</h5>
			<input type="text" name="phone" style="width: 500px;"
				placeholder="연락처를 입력해주세요." value="${reservation.phone }" />
			<h5>메모</h5>
			<input type="text" name="content" style="width: 500px;"
				 value="${reservation.content}" />
			<h5>결제 금액</h5>
			<input type="text" name="price" style="width: 500px;"
				value="${reservation.price }" />
<br />
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp; <input
				type="reset" value="되돌리기" /> <input type="submit" value="예약 수정" />
		</form>
	</div>
</body>
</html>