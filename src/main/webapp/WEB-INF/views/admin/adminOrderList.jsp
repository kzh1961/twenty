<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<head>
<script type="text/javascript">
	function delchk() {
		return confirm("삭제하시겠습니까?");
	}
</script>
</head>
<body>

<!-- 핵심 로직 -->
<h1>주문리스트</h1>
<div class="container-fluid">
	<hr>
<!-- 핵심 로직 -->


	<div class="widget-box">
		<div class="widget-title">
			<span class="icon"><i class="icon-th"></i></span>
			<h5>Order List</h5> 
		</div>


		<div class="dataTables_filter" id="example_filter">
			<label>Search: <input type="text" aria-controls="example"></label>
		</div>


		<div class="widget-content nopadding">
			<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper"
				role="grid">
				<div class="">
					<div id="DataTables_Table_0_length" class="dataTables_length"></div>
				</div>
				<table class="table table-bordered data-table dataTable"
					id="DataTables_Table_0">
					<thead>
							<tr role="row">
							

								<th style="width: 2%; text-align: center;">주문번호</th>
								<th style="width: 3%; text-align: center;">송장번호</th>
								<th style="width: 2%; text-align: center;">상품번호</th>
								<th style="width: 6%; text-align: center;">상품이름</th>
								<th style="width: 3%; text-align: center;">상품가격</th>
								<th style="width: 2%; text-align: center;">상품개수</th>
								<th style="width: 3%; text-align: center;">회원ID</th>
								<th style="width: 3%; text-align: center;">받는 이름</th>
								<th style="width: 7%; text-align: center;">받는 주소</th>
								<th style="width: 4%; text-align: center;">받는 폰번</th>
								<th style="width: 3%; text-align: center;">받는 우편</th>
								<th style="width: 6%; text-align: center;">메모</th>
								<th style="width: 3%; text-align: center;">총 금액</th>
								<th style="width: 3%; text-align: center;">주문날짜</th>
								<th style="width: 3%; text-align: center;">주문상태</th>
								<th style="width: 5%; text-align: center;">이미지</th>
								<th style="width: 7%; text-align: center;">옵션</th>
							</tr>
						</thead>
					<tbody>
							<c:forEach var="adminOrderList" items="${adminOrderList}"
								varStatus="stat">

								<tr class="gradeA even" role="row">
	
									<td style="text-align: center; vertical-align: middle;">${adminOrderList.order_num}</td>
									<td style="text-align: center; vertical-align: middle;">${adminOrderList.order_trans_num}</td>
									<td style="text-align: center; vertical-align: middle;">${adminOrderList.order_goods_num}</td>
									<td style="text-align: center; vertical-align: middle;">${adminOrderList.order_goods_name}</td>
									<td style="text-align: center; vertical-align: middle;">${adminOrderList.order_goods_price}</td>
									<td style="text-align: center; vertical-align: middle;">${adminOrderList.order_goods_amount}</td>
									<td style="text-align: center; vertical-align: middle;">${adminOrderList.order_member_id}</td>
									<td style="text-align: center; vertical-align: middle;">${adminOrderList.order_receive_name}</td>
									<td style="text-align: center; vertical-align: middle;">${adminOrderList.order_receive_addr}</td>
									<td style="text-align: center; vertical-align: middle;">${adminOrderList.order_receive_mobile}</td>
									<td style="text-align: center; vertical-align: middle;">${adminOrderList.order_receive_zipcode}</td>
									<td style="text-align: center; vertical-align: middle;">${adminOrderList.order_memo}</td>
									<td style="text-align: center; vertical-align: middle;">${adminOrderList.order_sum_money}</td>
									
									<td style="text-align: center; vertical-align: middle;"><fmt:formatDate
											value="${adminOrderList.order_date}" pattern="YY.MM.dd HH:mm" /></td>
									<td style="text-align: center; vertical-align: middle;">${adminOrderList.order_status}</td>
									<td style="text-align: center; vertical-align: middle;"><img
										src="/twenty/resources/goods_images/${adminOrderList.order_goods_image}"
										style="width:60px;height:60px" alt=""
										onerror="this.src='/twenty/resources/images/noimg_130.gif'" /></td>
									
									<td style="text-align: center; vertical-align: middle;"><a
									href="/twenty/adminOrderModifyForm?order_num=${adminOrderList.order_num }"><input
										type="image"
										src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/92/Cog_font_awesome.svg/32px-Cog_font_awesome.svg.png"></a>&nbsp;&nbsp;
									<c:url var="viewURL2" value="adminOrderDelete">
										<c:param name="order_num" value="${adminOrderList.order_num }" />
									</c:url> <a href="${viewURL2}"><input type="image"
										src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Trash_font_awesome.svg/32px-Trash_font_awesome.svg.png"
										onclick="return delchk()"></a></td>
							
								</tr>
							</c:forEach>
							<!--  등록된 회원이 없을때 -->
							<c:if test="${fn:length(adminOrderList) le 0}">
								<tr>
									<td colspan="9" style="text-align: center;">등록된 주문이 없습니다</td>
								</tr>
							</c:if>
						</tbody>
				</table>
				<!-- <div><input class="btn-primary" type="submit" value="글쓰기" onclick="javascript:location.href='adminNoticeWriteForm'"  /></div> -->
				
				
			</div>
		</div>
	</div>
	
	</div>
	</body>
	
	
