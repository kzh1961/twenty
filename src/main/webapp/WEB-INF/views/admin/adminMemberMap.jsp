<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>마커 클러스터러 사용하기</title>
    
</head>
<body>
<!-- 핵심 로직 -->
<h1>회원분포도</h1>
<div class="container-fluid">
	<hr>
<!-- 핵심 로직 -->
<div id="map" style="width:100%; height:650px;"></div>
<p id="result"></p>
<script type="text/javascript" src="//apis.daum.net/maps/maps3.js?apikey=311369b647ccd48aae7bb9dac08a5ce2&libraries=clusterer"></script>
<script type="text/javascript" src="//apis.daum.net/maps/maps3.js?apikey=311369b647ccd48aae7bb9dac08a5ce2"></script>
<script>
var map = new daum.maps.Map(document.getElementById('map'), { // 지도를 표시할 div
    center : new daum.maps.LatLng(36.2683, 127.6358), // 지도의 중심좌표 
    level : 14 // 지도의 확대 레벨 
});
//지도가 이동, 확대, 축소로 인해 중심좌표가 변경되면 마지막 파라미터로 넘어온 함수를 호출하도록 이벤트를 등록합니다
daum.maps.event.addListener(map, 'center_changed', function() {        
    
    // 지도의  레벨을 얻어옵니다
    var level = map.getLevel();
    
    // 지도의 중심좌표를 얻어옵니다 
    var latlng = map.getCenter(); 
    
    var message = '<p>지도 레벨은 ' + level + ' 이고</p>';
    message += '<p>중심 좌표는 위도 ' + latlng.getLat() + ', 경도 ' + latlng.getLng() + '입니다</p>'; 
    
    var resultDiv = document.getElementById('result');  
    resultDiv.innerHTML = message;
    
});

//일반 지도와 스카이뷰로 지도 타입을 전환할 수 있는 지도타입 컨트롤을 생성합니다
var mapTypeControl = new daum.maps.MapTypeControl();

// 지도에 컨트롤을 추가해야 지도위에 표시됩니다
// daum.maps.ControlPosition은 컨트롤이 표시될 위치를 정의하는데 TOPRIGHT는 오른쪽 위를 의미합니다
map.addControl(mapTypeControl, daum.maps.ControlPosition.TOPRIGHT);

// 지도 확대 축소를 제어할 수 있는  줌 컨트롤을 생성합니다
var zoomControl = new daum.maps.ZoomControl();
map.addControl(zoomControl, daum.maps.ControlPosition.RIGHT);



/* // 마커 클러스터러를 생성합니다 
// 마커 클러스터러를 생성할 때 disableClickZoom 값을 true로 지정하지 않은 경우
// 클러스터 마커를 클릭했을 때 클러스터 객체가 포함하는 마커들이 모두 잘 보이도록 지도의 레벨과 영역을 변경합니다 
// 이 예제에서는 disableClickZoom 값을 true로 설정하여 기본 클릭 동작을 막고
// 클러스터 마커를 클릭했을 때 클릭된 클러스터 마커의 위치를 기준으로 지도를 1레벨씩 확대합니다 
var clusterer = new daum.maps.MarkerClusterer({
    map: map, // 마커들을 클러스터로 관리하고 표시할 지도 객체 
    averageCenter: true, // 클러스터에 포함된 마커들의 평균 위치를 클러스터 마커 위치로 설정 
    minLevel: 10, // 클러스터 할 최소 지도 레벨 
    disableClickZoom: true // 클러스터 마커를 클릭했을 때 지도가 확대되지 않도록 설정한다 
}); */

//마커 클러스터러를 생성합니다 
var clusterer = new daum.maps.MarkerClusterer({
	map: map, // 마커들을 클러스터로 관리하고 표시할 지도 객체 
    averageCenter: true, // 클러스터에 포함된 마커들의 평균 위치를 클러스터 마커 위치로 설정 
    minLevel: 10, // 클러스터 할 최소 지도 레벨 
    disableClickZoom: true, // 클러스터 마커를 클릭했을 때 지도가 확대되지 않도록 설정한다 
    calculator: [10, 30, 50], // 클러스터의 크기 구분 값, 각 사이값마다 설정된 text나 style이 적용된다
    texts: getTexts, // texts는 ['삐약', '꼬꼬', '꼬끼오', '치멘'] 이렇게 배열로도 설정할 수 있다 
    styles: [{ // calculator 각 사이 값 마다 적용될 스타일을 지정한다
            width : '30px', height : '30px',
            background: 'rgba(51, 204, 255, .8)',
            borderRadius: '15px',
            color: '#000',
            textAlign: 'center',
            fontWeight: 'bold',
            lineHeight: '31px'
        },
        {
            width : '40px', height : '40px',
            background: 'rgba(255, 153, 0, .8)',
            borderRadius: '20px',
            color: '#000',
            textAlign: 'center',
            fontWeight: 'bold',
            lineHeight: '41px'
        },
        {
            width : '50px', height : '50px',
            background: 'rgba(255, 51, 204, .8)',
            borderRadius: '25px',
            color: '#000',
            textAlign: 'center',
            fontWeight: 'bold',
            lineHeight: '51px'
        },
        {
            width : '60px', height : '60px',
            background: 'rgba(255, 80, 80, .8)',
            borderRadius: '30px',
            color: '#000',
            textAlign: 'center',
            fontWeight: 'bold',
            lineHeight: '61px'
        }
    ]
});

// 클러스터 내부에 삽입할 문자열 생성 함수입니다 
function getTexts( count ) {

  // 한 클러스터 객체가 포함하는 마커의 개수에 따라 다른 텍스트 값을 표시합니다 
  if(count < 10) {
    return '10명미만';        
  } else if(count < 30) {
    return '30명미만';
  } else if(count < 50) {
    return '50명미만';
  } else {
    return '50명이상';
  }
}


// 데이터를 가져오기 위해 jQuery를 사용합니다
// 데이터를 가져와 마커를 생성하고 클러스터러 객체에 넘겨줍니다
 $.get("/twenty/resources/admin/json/MemberMap.json", function(data) {
    // 데이터에서 좌표 값을 가지고 마커를 표시합니다
    // 마커 클러스터러로 관리할 마커 객체는 생성할 때 지도 객체를 설정하지 않습니다
    var markers = $(data.positions).map(function(i, position) {
        return new daum.maps.Marker({
            position : new daum.maps.LatLng(position.lat, position.lng)
        });
    });

    // 클러스터러에 마커들을 추가합니다
    clusterer.addMarkers(markers);
});

// 마커 클러스터러에 클릭이벤트를 등록합니다 
// 마커 클러스터러를 생성할 때 disableClickZoom을 true로 설정하지 않은 경우 
// 이벤트 헨들러로 cluster 객체가 넘어오지 않을 수도 있습니다 
daum.maps.event.addListener(clusterer, 'clusterclick', function(cluster) {

    // 현재 지도 레벨에서 1레벨 확대한 레벨 
    var level = map.getLevel()-1;

    // 지도를 클릭된 클러스터의 마커의 위치를 기준으로 확대합니다 
    map.setLevel(level, {anchor: cluster.getCenter()});  
});

//마커를 클릭했을 때 마커 위에 표시할 인포윈도우를 생성합니다
var iwContent = '<div style="padding:5px;">Hello World!</div>', // 인포윈도우에 표출될 내용으로 HTML 문자열이나 document element가 가능합니다
    iwRemoveable = true; // removeable 속성을 ture 로 설정하면 인포윈도우를 닫을 수 있는 x버튼이 표시됩니다

// 인포윈도우를 생성합니다
var infowindow = new daum.maps.InfoWindow({
    content : iwContent,
    removable : iwRemoveable
});

// 마커에 클릭이벤트를 등록합니다
daum.maps.event.addListener(marker, 'click', function() {
      // 마커 위에 인포윈도우를 표시합니다
      infowindow.open(map, marker);  
});


</script>
</body>
</html>

<%-- <%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<head>
<script
	src="https://maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyCRSeEVl3pSPGUVWwW4DSwZNDu0Q3AuSpc"></script>
<script src="/twenty/resources/admin//js/google_map.js"
	type="text/javascript"></script>
</head>
<body>
	<c:forEach var="memberaddress" items="${memberaddress}">
		<script>
			geocode('${memberaddress.address1}', '${memberaddress.name}',
					'${memberaddress.name}');
		</script>
	</c:forEach>
	<div id="map-canvas" style="height: 300px; width: 550px"></div>
</body> --%>




<!-- *************************************************************************************************************************************************************** -->
<%--  <%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
	<!DOCTYPE html>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>전시 상세보기</title>
<script src="https://maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyCRSeEVl3pSPGUVWwW4DSwZNDu0Q3AuSpc"></script>
<script src="/twenty/resources/admin/js/google_map.js"></script>
</head>
  <body>
 <script type="text/javascript"></script>
   <c:forEach var="memberaddress" items="${memberaddress}" varStatus="stat">
			geocode('${memberaddress.address1}','${memberaddress.address1}');
 			</c:forEach>
/* 		geocode('<s:property value="resultClass.address1" />', '<s:property value="resultClass.address2" />', '<s:property value="resultClass.subject" />'); */
</script>
<div id="map-canvas" style="width: 555px; height: 555px"></div>

  </body>
</html> --%>


<!-- *************************************************************************************************************************************************************** -->
<%-- <%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>회원 분포도</title>
<script src="https://maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyC70EaSIpSzzgdnOEB7xKuENx7LrgR7Lp8"></script>
</head>
  <body>
    <!-- GoogoleMap Asynchronously Loading the API ********************************************* -->
<script type="text/javascript">
var map_x;
var map_y;
var contents = "";
var map;

function setLocation(x, y) {
	map_x = x;
	map_y = y;
}

function initialize() {
	var cairo = {lat: map_x, lng: map_y};
	var mapOptions = {
		zoom : 16,
		scaleControl: true,
		/* center: new google.maps.LatLng(35.87110100714382, 128.60169690333006) */
		center : cairo
	};
	map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
	
	var infowindow = new google.maps.InfoWindow;
	infowindow.setContent('<b>' + contents + '</b>');
	
	var marker = new google.maps.Marker({map: map, position: cairo});
	marker.addListener('click', function() {
	  infowindow.open(map, marker);
	});
}

function geocode(address1, address2, content) {
	var address = "";
	address = address1 + " " + address2;
	contents = content;
	var geocoder = new google.maps.Geocoder();
	geocoder.geocode({
		'address' : address,
		'partialmatch' : true
	}, geocodeResult);
}

function geocodeResult(results, status) {
	//if( status == google.maps.GeocoderStatus.OK ) {
	if (status == 'OK' && results.length > 0) {
		//map.fitBounds(results[0].geometry.viewport);
		setLocation(results[0].geometry.location.lat(), results[0].geometry.location.lng());
		initialize();
	} else {
		alert("Geocode was not successful for the following reason: "
				+ status);
	}
}
/* google.maps.event.addDomListener(window, 'load', geocode());  */
geocode('서울특별시', '강남구', '데리야끼');
/* geocode('<s:property value="resultClass.address1" />', '<s:property value="resultClass.address2" />', '<s:property value="resultClass.subject" />'); */
</script>
<div id="map-canvas" style="width: 100%; height: 340px"></div>
  </body>
</html> --%>
<!-- *************************************************************************************************************************************************************** -->

<%-- <%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<!DOCTYPE html>
<html>
<head>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script type="text/javascript" src="https://maps.google.com/maps/api/js?v=3.exp&region=KR"></script>
</head>
  <body>
    <!-- GoogoleMap Asynchronously Loading the API ********************************************* -->
<script type="text/javascript">
    function initialize() {
     
        var mapOptions = {
                            zoom: 18, // 지도를 띄웠을 때의 줌 크기
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                        };
         
         
        var map = new google.maps.Map(document.getElementById("map-canvas"), // div의 id과 값이 같아야 함. "map-canvas"
                                    mapOptions);
         
        var size_x = 40; // 마커로 사용할 이미지의 가로 크기
        var size_y = 40; // 마커로 사용할 이미지의 세로 크기
     
        // 마커로 사용할 이미지 주소
        var image = new google.maps.MarkerImage( '대전광역시 중구 한밭도서관길 222',
                                                    new google.maps.Size(size_x, size_y),
                                                    '',
                                                    '',
                                                    new google.maps.Size(size_x, size_y));
         
        // Geocoding *****************************************************
        var address = '대전광역시 중구 한밭도서관길 222'; // DB에서 주소 가져와서 검색하거나 왼쪽과 같이 주소를 바로 코딩.
        var marker = null;
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode( { 'address': address}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
                marker = new google.maps.Marker({
                                map: map,
                                icon: image, // 마커로 사용할 이미지(변수)
                                title: '한밭도서관', // 마커에 마우스 포인트를 갖다댔을 때 뜨는 타이틀
                                position: results[0].geometry.location
                            });
 
                var content = "한밭도서관<br/><br/>Tel: 042-580-4114"; // 말풍선 안에 들어갈 내용
             
                // 마커를 클릭했을 때의 이벤트. 말풍선 뿅~
                var infowindow = new google.maps.InfoWindow({ content: content});
                google.maps.event.addListener(marker, "click", function() {infowindow.open(map,marker);});
            } else {
                alert("Geocode was not successful for the following reason: " + status);
            }
        });
        // Geocoding // *****************************************************
         
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>
 <div id="map-canvas" style="width: 100%; height: 340px" title="도서관 위치입니다."></div>
  </body>
</html> --%>

<!-- *************************************************************************************************************************************************************** -->
<%-- <%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script type="text/javascript" src="https://maps.google.com/maps/api/js?v=3.exp&region=KR"></script>
</head>
<body>
<div id="google_map" style="width:100px; height:100px;"></div>

<script type="text/javascript">
jQuery(document).ready(function(){
    google.maps.event.addDomListener(window, 'load', initialize);
 
function initialize(){
    if($("#google_map").length) {
 
    var mapOptions = { //구글 맵 옵션 설정
        zoom : 555, //기본 확대율
        center : new google.maps.LatLng(37.5651, 126.98955), // 지도 중앙 위치
        scrollwheel : false, //마우스 휠로 확대 축소 사용 여부
        mapTypeControl : false //맵 타입 컨트롤 사용 여부
    };
 
    var map = new google.maps.Map(document.getElementById('google_map'), mapOptions); //구글 맵을 사용할 타겟
    
    var image = 'http://cheolguso.com/img/iconimg.png'; //마커 이미지 설정
 
    var marker = new google.maps.Marker({ //마커 설정
        map : map,
        position : map.getCenter(), //마커 위치
        icon : image //마커 이미지
    });
    google.maps.event.addDomListener(window, "resize", function() { //리사이즈에 따른 마커 위치
        var center = map.getCenter();
        google.maps.event.trigger(map, "resize");
        map.setCenter(center); 
    });
 
    }//if-end
}//function-end
 
});//jQuery-end
</script>
</body>
</html> --%>