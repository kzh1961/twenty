<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<title>공지사항 수정</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="/resources/web/css/bootstrap.css" rel="stylesheet"
	type="text/css" />
<script type="text/javascript">
	function validateForm() {
		var a = document.forms["adminNoticeModifyForm"]["subject"].value;
		var b = document.forms["adminNoticeModifyForm"]["content"].value;

		if (a == null || a == "") {
			alert("제목을 입력해야합니다.");
			return false;
		} else if (b == null || b == "") {
			alert("내용을 입력해야합니다");
			return false;
		}
	}
</script>
</head>
<body>
	<!-- 핵심 로직 -->
	<h1>공지사항 수정</h1>
	<div class="container-fluid">
		<hr>
		<!-- 핵심 로직 -->

		<form action="adminNoticeModify" enctype="multipart/form-data"
			method="post" name="adminNoticeModifyForm"
			onsubmit="return validateForm()">
			
			<input type="hidden" name="notice_num"
				value="${noticeModel.notice_num}" />

			<h5>공지사항 제목</h5>
			<input type="text" name="subject" style="width: 500px;"
				value="${noticeModel.subject}" placeholder="공지사항 제목을 입력해주세요" />
				
			<h5>내 용</h5>
			<textarea name="content" rows="10" style="width: 500px;"
				 value="${noticeModel.subject}" placeholder="공지사항 제목을 입력해주세요"></textarea>
			<br/>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input
				type="reset" value="되돌리기" /> <input type="submit" value="수정" />
		</form>
	</div>
</body>
</html>









<%-- <%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<script type="text/javascript">
	function onDelete() {
		if (confirm("정말로 1:1문의를 삭제하시겠습니까?") == true) {
			alert("1:1문의가 삭제되었습니다.");
			location.href = 'qnaDelete?qna_num=${qnaModel.qna_num}';
		} else {
			return;
		}
	}
	function onModify() {
		location.href = 'qnaModifyForm?qna_num=${qnaModel.qna_num}';
	};
	function cmmDelete() {
		if (confirm("정말로 답변글을 삭제하시겠습니까?") == true) {
			alert("답변글이 삭제되었습니다.");
			location.href = 'qnaCommDelete?qna_num=${qnaModel.qna_num}&comment_num=${qnaCommModel.comment_num}';
		} else {
			return;
		}
	}
</script>
</head>
<body>
	<!--start-breadcrumbs-->
	<div class="breadcrumbs">
		<div class="container">
			<div class="breadcrumbs-main">
				<ol class="breadcrumb">
					<li><a href="./index.jsp">Home</a></li>
					<li>고객센터</li>
					<li class="active">1:1문의</li>
				</ol>
			</div>
		</div>
	</div>
	<!--end-breadcrumbs-->
	<div class="container">
		<div class="register-top heading">
			<br> <br>
			<h2>1:1 문의</h2>
		</div>
		<ul class="pager">
			<li class="next"><a href="./qnaList">목록</a></li>
		</ul>
		<!-- 문의글 -->
		<div class="panel panel-default">
			<div class="panel-heading">
				<p>${qnaModel.subject }</p>
				<p id="regdate">
					<fmt:formatDate value="${qnaModel.regdate }" pattern="yyyy-MM-dd" />
				</p>
			</div>
			<div class="panel-body" style="min-height: 200px">
				<br>${qnaModel.content }<br> <br>
			</div>
		</div>
		<!-- 답변 리스트 -->
		<c:if test="${null ne qnaCommModel.content}">
			<div class="panel panel-info">
				<div class="panel-heading">
					<p>답변입니다.</p>
					<p id="regdate">
						<fmt:formatDate value="${qnaCommModel.regdate }"
							pattern="yyyy-MM-dd" />
					</p>
				</div>
				<div class="panel-body">${qnaCommModel.content }
					<c:if test="${session_id == 'admin'}">
						<button type="button" class="btn btn-info" onclick="cmmDelete()">삭제</button>
					</c:if>
				</div>
			</div>
		</c:if>
		<c:if test="${null eq qnaCommModel.content}">
		</c:if>
		<!-- 답변 쓰기 -->
		<form:form commandName="QnA" action="./qnaCommWrite" method="post"
			class="form-horizontal">
			<form:input type="hidden" path="qna_num" value="${qnaModel.qna_num}" />
			<c:if test="${session_id == 'admin' && null eq qnaCommModel.content}">
				<div style="padding-left: 20px">
					<div class="col-sm-12 form-group">
						<div class="form-group">
							<label for="comment">답변쓰기</label>
							<form:textarea class="form-control" rows="2" path="content"
								required="required"></form:textarea>
						</div>
						<div class="form-group">
							<div class="col-sm-12" style="text-align: right;">
								<button type="submit" class="btn btn-info">등록</button>
							</div>
						</div>
					</div>
				</div>
			</c:if>
		</form:form>
		<!-- 회원:문의글 수정/삭제 -->
		<c:if test="${session_id != 'admin' }">
			<button type="button" class="btn btn-info" onclick="onModify()">수정</button>
			<button type="button" class="btn btn-info" onclick="onDelete()">삭제</button>
		</c:if>
	</div>
	<br>
	<br>
	<br>
	<br>
	<br>
</body>
</html> --%>