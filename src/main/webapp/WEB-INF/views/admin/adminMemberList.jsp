<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<head>
<script type="text/javascript">
	function delchk() {
		return confirm("삭제하시겠습니까?");
	}
	function delchk() {
		return confirm("삭제하시겠습니까?");
	}
</script>
</head>
<body>

<!-- 핵심 로직 -->
<h1>일반회원 리스트</h1>
<div class="container-fluid">
	<hr>
<!-- 핵심 로직 -->


	<div class="widget-box">
		<div class="widget-title">
			<span class="icon"><i class="icon-th"></i></span>
			<h5>Member List</h5>
		</div>


		<div class="dataTables_filter" id="example_filter">
			<label>Search: <input type="text" aria-controls="example"></label>
		</div>


		<div class="widget-content nopadding">
			<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper"
				role="grid">
				<div class="">
					<div id="DataTables_Table_0_length" class="dataTables_length"></div>
				</div>
				<table class="table table-bordered data-table dataTable"
					id="DataTables_Table_0">
					<thead>
							<tr role="row">
								<th style="width: 5%; text-align: center;">번호</th>
								<th style="width: 8%; text-align: center;">ID</th>
								<th style="width: 7%; text-align: center;">이름</th>
								<th style="width: 9%; text-align: center;">전화번호</th>
								<th style="width: 14%; text-align: center;">E-Mail</th>
								<th style="width: 29%; text-align: center;">주소</th>
								<th style="width: 5%; text-align: center;">Point</th>
								<th style="width: 10%; text-align: center;">가입일자</th>
								<th style="width: 13%; text-align: center;">관리</th>
							</tr>
						</thead>
					<tbody>
							<c:forEach var="memberlist" items="${memberlist}"
								varStatus="stat">

								<tr class="gradeA even" role="row">
									<td style="text-align: center; vertical-align: middle;">${memberlist.mem_num}</td>
									<td style="text-align: center; vertical-align: middle;">${memberlist.id}</td>
									<td style="text-align: center; vertical-align: middle;">${memberlist.name}</td>
									<td style="text-align: center; vertical-align: middle;">${memberlist.phone}</td>
									<td style="text-align: center; vertical-align: middle;">${memberlist.email}</td>
									<td style="text-align: center; vertical-align: middle;">${memberlist.address1}</td>
									<td style="text-align: center; vertical-align: middle;">${memberlist.point}</td>
									<td style="text-align: center; vertical-align: middle;"><fmt:formatDate
											value="${memberlist.join_date}" pattern="YY.MM.dd HH:mm" /></td>
									<td style="text-align: center; vertical-align: middle;"><c:url
											var="viewURL" value="adminMemberModifyForm">
											<c:param name="id" value="${memberlist.id }" />
										</c:url> <a href="${contextpath}/twenty/adminMemberModifyForm?id=${memberlist.id}">
										
										<input type="image" 
											src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/92/Cog_font_awesome.svg/32px-Cog_font_awesome.svg.png"></a>&nbsp;&nbsp;
										<c:url var="viewURL2" value="adminMemberDelete">
											<c:param name="id" value="${memberlist.id }" />
										</c:url> <a href="${viewURL2}"><input type="image"
											src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Trash_font_awesome.svg/32px-Trash_font_awesome.svg.png"
											onclick="return delchk()"></a></td>
											
								</tr>
							</c:forEach>
							<!--  등록된 회원이 없을때 -->
							<c:if test="${fn:length(memberlist) le 0}">
								<tr>
									<td colspan="9" style="text-align: center;">등록된 회원이 없습니다</td>
								</tr>
							</c:if>
						</tbody>
				</table>
				<!-- <div><input class="btn-primary" type="submit" value="글쓰기" onclick="javascript:location.href='adminNoticeWriteForm'"  /></div> -->
				
				
			</div>
		</div>
	</div>
	
	</div>
	</body>
	
	
