
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<title>회원가입</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="/resources/web/css/bootstrap.css" rel="stylesheet"
	type="text/css" />
<script type="text/javascript">
	function validateForm() {
		var a = document.forms["adminRoomWriteForm"]["room_num"].value;
		var b = document.forms["adminRoomWriteForm"]["price1"].value;
		var c = document.forms["adminRoomWriteForm"]["price2"].value;
		var d = document.forms["adminRoomWriteForm"]["content"].value;
		var g = document.forms["adminRoomWriteForm"]["max_people"].value;
		var e = document.forms["adminRoomWriteForm"]["room_img1"].value;
		var f = document.forms["adminRoomWriteForm"]["room_img2"].value;

		if (a == null || a == "") {
			alert("객실 번호를 입력해야합니다");
			return false;
		} else if (b == null || b == "") {
			alert("주중 가격을 입력해야합니다");
			return false;
		} else if (c == null || c == "") {
			alert("주말 가격을 입력해야합니다");
			return false;
		} else if (d == null || d == "") {
			alert("객실 내용을 입력해야합니다");
			return false;
		} else if (e == null || e == "") {
			alert("메인이미지를 입력해야합니다");
			return false;
		} else if (f == null || f == "") {
			alert("서브이미지를 입력해야합니다");
			return false;
		} else if (g == null || f == "") {
			alert("최대인원수를 입력해야합니다");
			return false;
		}
	}
</script>
</head>
<body>
	<!-- 핵심 로직 -->
	<h1>객실 등록</h1>
	<div class="container-fluid">
		<hr>
		<!-- 핵심 로직 -->

		<form action="adminRoomWrite" enctype="multipart/form-data"
			method="post" name="adminRoomWriteForm"
			onsubmit="return validateForm()">
			<h5>펜션 이름</h5>
			<input type="text" name="pen_name" style="width: 500px;"
				readonly="readonly" value="${pen_name }"/>
			<h5>객실 번호</h5>
			<input type="text" name="room_num" style="width: 500px;"
				placeholder="객실 번호를 입력해주세요." />
			<h5>주중 가격</h5>
			<input type="text" name="price1" style="width: 500px;"
				placeholder="주중 가격을 입력해주세요." />
			<h5>주말 가격</h5>
			<input type="text" name="price2" style="width: 500px;"
				placeholder="주말 가격을 입력해주세요." maxlength="11" />
			<h5>최대 인원수</h5>
			<input type="text" name="max_people" style="width: 500px;"
				placeholder="최대 인원수를 입력해주세요." />
			<h5>객실 내용</h5>
			<input type="text" name="content" style="width: 500px;"
				placeholder="객실 내용을 입력해주세요." /><br />
				 메인 이미지<input type="file"
				name="room_img1" /><br />
			<p class="help-block">객실 메인이미지 입니다 800x800 사이즈 권장합니다</p>
			서브 이미지<input type="file" name="room_img2" /><br />
			<p class="help-block">객실 서브이미지 입니다 1000x(2500~3800)사이즈 권장</p>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input
				type="reset" value="되돌리기" /> <input type="submit" value="객실 등록" />
		</form>
	</div>
</body>
</html>