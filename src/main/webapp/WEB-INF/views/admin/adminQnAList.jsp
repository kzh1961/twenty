<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<head>
<script type="text/javascript">
	function delchk() {
		return confirm("삭제하시겠습니까?");
	}
</script>
</head>
<body>
	<!-- 핵심 로직 -->
	<h1>QnA리스트</h1>
	<div class="container-fluid">
		<hr>
		<!-- 핵심 로직 -->

		<div class="widget-box">
			<div class="widget-title">
				<span class="icon"><i class="icon-th"></i></span>
				<h5>QnA List</h5>
				
			</div>
			
			<div class="dataTables_filter" id="example_filter">
				<label>Search: <input type="text" aria-controls="example"></label>
			</div>

			<div class="widget-content nopadding">
				<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper"
					role="grid">
					<div class="">
						<div id="DataTables_Table_0_length" class="dataTables_length"></div>
					</div>
					<table class="table table-bordered data-table dataTable"
						id="DataTables_Table_0">
						<thead>
							<tr role="row">

								<th style="width: 5%; text-align: center;">ID</th>
								<th style="width: 5%; text-align: center;">번호</th>
								<th style="width: 8%; text-align: center;">이름</th>
								<th style="width: 8%; text-align: center;">전화번호</th>
								<th style="width: 10%; text-align: center;">E-Mail</th>
								<th style="width: 8%; text-align: center;">문의 상품명</th>
								<th style="width: 10%; text-align: center;">제목</th>
								<th style="width: 16%; text-align: center;">내용</th>
								<th style="width: 8%; text-align: center;">질문날짜</th>
								<th style="width: 6%; text-align: center;">관리자 답변여부</th>
								<th style="width: 8%; text-align: center;">설정</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="adminQnAList" items="${adminQnAList}"
								varStatus="stat">

								<tr class="gradeA even" role="row">
									<td style="text-align: center; vertical-align: middle;">${adminQnAList.qna_num}</td>
									<td style="text-align: center; vertical-align: middle;">${adminQnAList.id}</td>
									<td style="text-align: center; vertical-align: middle;">${adminQnAList.name}</td>
									<td style="text-align: center; vertical-align: middle;">${adminQnAList.phone}</td>
									<td style="text-align: center; vertical-align: middle;">${adminQnAList.email}</td>
									<td style="text-align: center; vertical-align: middle;">${adminQnAList.order_goods_name}</td>
									<td style="text-align: center; vertical-align: middle;">${adminQnAList.subject}</td>
									<td style="text-align: center; vertical-align: middle;">${adminQnAList.content}</td>
									<td style="text-align: center; vertical-align: middle;"><fmt:formatDate
											value="${adminQnAList.regdate}" pattern="YY.MM.dd HH:mm" /></td>
									<td style="text-align: center; vertical-align: middle;">${adminQnAList.admin}</td>
									<td style="text-align: center; vertical-align: middle;">
										<c:url var="viewURL2" value="adminQnADelete">
											<c:param name="qna_num" value="${adminQnAList.qna_num }" />
										</c:url> <a href="${viewURL2}"><input type="image"
											src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Trash_font_awesome.svg/32px-Trash_font_awesome.svg.png"
											onclick="return delchk()"></a></td>
								</tr>
							</c:forEach>
							<!--  등록된 QnA가 없을때 -->
							<c:if test="${fn:length(adminQnAList) le 0}">
								<tr>
									<td colspan="9" style="text-align: center;">등록된 QnA가 없습니다</td>
								</tr>
							</c:if>
						</tbody>
					</table>
					<!-- <div><input class="btn-primary" type="submit" value="글쓰기" onclick="javascript:location.href='adminNoticeWriteForm'"  /></div> -->
				</div>
			</div>
		</div>
	</div>
</body>