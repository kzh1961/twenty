<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<head>
<script type="text/javascript">
	function delchk() {
		return confirm("삭제하시겠습니까?");
	}
</script>

<!-- 핵심 로직 -->
<h1>상품 코멘트</h1>
<div class="container-fluid">
	<hr>
	<!-- 핵심 로직 -->


	<div class="widget-box">
		<div class="widget-title">
			<span class="icon"><i class="icon-th"></i></span>
			<h5>Comment List</h5>
			
		</div>


		<div class="dataTables_filter" id="example_filter">
			<label>Search: <input type="text" aria-controls="example"></label>
		</div>


		<div class="widget-content nopadding">
			<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper"
				role="grid">
				<div class="">
					<div id="DataTables_Table_0_length" class="dataTables_length"></div>
				</div>
				<table class="table table-bordered data-table dataTable"
					id="DataTables_Table_0">
					<thead>
						<tr role="row">
						
							<th style="width: 10%; text-align: center;">번호</th>
							<th style="width: 10%; text-align: center;">상품번호</th>
							<th style="width: 10%; text-align: center;">회원 아이디</th>
							<th style="width: 40%; text-align: center;">코멘트 내용</th>
							<th style="width: 10%; text-align: center;">별점</th>
							<th style="width: 10%; text-align: center;">등록일</th>
							<th style="width: 10%; text-align: center;">관리</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="adminCommentList" items="${adminCommentList}" varStatus="stat">
							<tr class="gradeA even" role="row">
						
								<td style="text-align: center; vertical-align: middle;">${adminCommentList.comment_num}</td>
								<td style="text-align: center; vertical-align: middle;">${adminCommentList.goods_num}</td>
								<td style="text-align: center; vertical-align: middle;">${adminCommentList.id}</td>
								<td style="text-align: center; vertical-align: middle;">${adminCommentList.comments}</td>
								<td style="text-align: center; vertical-align: middle;">${adminCommentList.goods_point}</td>
								<td style="text-align: center; vertical-align: middle;"><fmt:formatDate
										value="${adminCommentList.comment_date}" pattern="YY.MM.dd HH:mm" /></td>
								<td style="text-align: center; vertical-align: middle;">
										<c:url var="viewURL2" value="adminCommentDelete">
											<c:param name="comment_num" value="${adminCommentList.comment_num}" />
										</c:url> <a href="${viewURL2}"><input type="image"
											src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Trash_font_awesome.svg/32px-Trash_font_awesome.svg.png"
											onclick="return delchk()"></a></td>


							</tr>
						</c:forEach>
						<!--  등록된 상품코멘트가 없을때 -->
						<c:if test="${fn:length(adminCommentList) le 0}">
							<tr>
								<td colspan="9" style="text-align: center;">등록된 코멘트가 없습니다</td>
							</tr>
						</c:if>
					</tbody>
				</table>
				<!-- <div><input class="btn-primary" type="submit" value="글쓰기" onclick="javascript:location.href='adminNoticeWriteForm'"  /></div> -->

</div>
			</div>
		</div>
	</div>