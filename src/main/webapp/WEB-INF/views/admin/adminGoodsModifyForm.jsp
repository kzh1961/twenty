<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<title>상품 수정</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="text/javascript">
	function validateForm() {
		var a = document.forms["adminGoodsModifyForm"]["goods_subtitle"].value;
		var b = document.forms["adminGoodsModifyForm"]["goods_name"].value;
		var c = document.forms["adminGoodsModifyForm"]["goods_amount"].value;
		var d = document.forms["adminGoodsModifyForm"]["goods_price"].value;
		var e = document.forms["adminGoodsModifyForm"]["file1"].value;
		var f = document.forms["adminGoodsModifyForm"]["file2"].value;
		var g = document.forms["adminGoodsModifyForm"]["file3"].value;

		if (a == null || a == "") {
			alert("상품부제를 입력해야합니다");
			return false;
		} else if (b == null || b == "") {
			alert("상품명을 입력해야합니다");
			return false;
		} else if (c == null || c == "") {
			alert("상품수량을 입력해야합니다");
			return false;
		} else if (d == null || d == "") {
			alert("상품가격를 입력해야합니다");
			return false;
		} else if (e == null || e == "") {
			alert("상단이미지를 입력해야합니다");
			return false;
		} else if (f == null || f == "") {
			alert("중단이미지를 입력해야합니다");
			return false;
		} else if (g == null || g == "") {
			alert("하단이미지를 입력해야합니다");
			return false;
		}
	}
</script>
</head>
<body>
	<!-- 핵심 로직 -->
	<h1>상품 수정</h1>
	<div class="container-fluid">
		<hr>
		<!-- 핵심 로직 -->

		<form action="adminGoodsModify" enctype="multipart/form-data"
			method="post" name="adminGoodsModifyForm"
			onsubmit="return validateForm()">
	<input type="hidden" name="goods_num"
				value="${goodsModel.goods_num}" />
			<h5>카테고리</h5>
			<SELECT style="width: 510px;" name="goods_category">
				<OPTION VALUE=1 SELECTED>여성 의류</OPTION>
				<OPTION VALUE=2>여성 시계&쥬얼리</OPTION>
				<OPTION VALUE=3>여성 가방&신발</OPTION>
				<OPTION VALUE=4>여성 패션 잡화</OPTION>
				<OPTION VALUE=5>여성 언더웨어</OPTION>
				<OPTION VALUE=6>남성 의류</OPTION>
				<OPTION VALUE=7>남성 시계&쥬얼리</OPTION>
				<OPTION VALUE=8>남성 가방&신발</OPTION>
				<OPTION VALUE=9>남성 패션 잡화</OPTION>
				<OPTION VALUE=10>남성 언더웨어</OPTION>
				
				<OPTION VALUE=11>유아 의류</OPTION>
				<OPTION VALUE=12>유아 신발</OPTION>
				<OPTION VALUE=13>유아 잡화</OPTION>
				
				<OPTION VALUE=14>스킨케어</OPTION>
				<OPTION VALUE=15>메이크업</OPTION>
				<OPTION VALUE=16>헤어</OPTION>
				<OPTION VALUE=17>바디</OPTION>
				<OPTION VALUE=18>네일</OPTION>
				<OPTION VALUE=19>기저귀/물티슈</OPTION>
				<OPTION VALUE=20>분유/유아식품</OPTION>
				<OPTION VALUE=21>목욕/스킨케어</OPTION>
				
				<OPTION VALUE=22>쌀/잡곡/채소/과일</OPTION>
				<OPTION VALUE=23>축/수산</OPTION>
				<OPTION VALUE=24>김치/반찬/젓갈</OPTION>
				<OPTION VALUE=25>장/오일/조미료</OPTION>
				<OPTION VALUE=26>건강식품</OPTION>
				<OPTION VALUE=27>즉석식품</OPTION>
				<OPTION VALUE=28>스낵</OPTION>
				
				<OPTION VALUE=29>침대/매트릭스/매트</OPTION>
				<OPTION VALUE=30>쇼파/의자/책상</OPTION>
			</SELECT>
			<p class="help-block">메인페이지 출력 및 상품 카테고리별 검색시 활용됩니다.</p>

			<h5>상품 부제</h5>
			<input type="text" name="goods_subtitle" style="width: 500px;"
				value="${goodsModel.goods_subtitle}" placeholder="상품부제를 입력해주세요" />

			<h5>상품 이름</h5>
			<input type="text" name="goods_name" style="width: 500px;"
				value="${goodsModel.goods_name}" placeholder="상품이름을 입력해주세요" />

			<h5>상품 수량</h5>
			<input type="text" name="goods_amount" style="width: 500px;"
				value="${goodsModel.goods_amount}" placeholder="상품수량을 입력해주세요" />

			<h5>상품 가격</h5>
			<input type="text" name="goods_price" style="width: 500px;"
				value="${goodsModel.goods_price}" placeholder="상품가격을 입력해주세요" /><br />

			상단 이미지<input type="file" name="file1" /><br />
			<p class="help-block">메인상품 이미지 입니다 800x800 사이즈 권장합니다</p>
			중단 이미지<input type="file" name="file2" /><br />
			<p class="help-block">상품설명 이미지 입니다 1000x(2500~3800)사이즈 권장</p>
			하단 이미지<input type="file" name="file3" /><br />
			<p class="help-block">상품설명 이미지 입니다 1000x(2500~3800)사이즈 권장</p>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input
				type="reset" value="되돌리기" /> <input type="submit" value="상품 수정" />
		<input type="hidden" name="goods_old_img1" value="${goodsModel.goods_image1 }"/>
				<input type="hidden" name="goods_old_img2" value="${goodsModel.goods_image2}"/>
				<input type="hidden" name="goods_old_img3" value="${goodsModel.goods_image3}"/>
		</form>
	</div>
</body>
</html>