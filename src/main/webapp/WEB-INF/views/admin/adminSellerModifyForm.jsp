<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<title>상품 수정</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="/resources/web/css/bootstrap.css" rel="stylesheet"
	type="text/css" />
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script type="application/x-javascript">
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 
	 function openZipcode(){
			var url="zipcodeCheckForm";
			open(url, "confirm","toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=no, width=410, height=400");
		}
</script>
<script type="text/javascript">
	function validateForm() {
		var a = document.forms["adminMemberModifyForm"]["password"].value;
		var b = document.forms["adminMemberModifyForm"]["password2"].value;
		var c = document.forms["adminMemberModifyForm"]["name"].value;
		var d = document.forms["adminMemberModifyForm"]["email"].value;
		var e = document.forms["adminMemberModifyForm"]["phone"].value;
		var f = document.forms["adminMemberModifyForm"]["zipcode"].value;
		var g = document.forms["adminMemberModifyForm"]["address1"].value;
		var h = document.forms["adminMemberModifyForm"]["address2"].value;

		if (a == null || a == "") {
			alert("패스워드를 입력해야합니다");
			return false;
		} else if (b == null || b == "") {
			alert("패스워드 확인 입력해야합니다");
			return false;
		} else if (c == null || c == "") {
			alert("이름을 입력해야합니다");
			return false;
		} else if (d == null || d == "") {
			alert("이메일을 입력해야합니다");
			return false;
		} else if (e == null || e == "") {
			alert("휴대폰 번호를 입력해야합니다");
			return false;
		} else if (f == null || f == "") {
			alert("우편번호를 입력해야합니다");
			return false;
		} else if (g == null || g == "") {
			alert("기본주소를 입력해야합니다");
			return false;
		}else if (h == null || h == "") {
			alert("상세 주소를 입력해야 합니다");
			return false;
		}
		
	}
	function sample6_execDaumPostcode() {
		new daum.Postcode(
				{
					oncomplete : function(data) {
						// 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

						// 각 주소의 노출 규칙에 따라 주소를 조합한다.
						// 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
						var fullAddr = ''; // 최종 주소 변수
						var extraAddr = ''; // 조합형 주소 변수

						// 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
						if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
							fullAddr = data.roadAddress;

						} else { // 사용자가 지번 주소를 선택했을 경우(J)
							fullAddr = data.jibunAddress;
						}

						// 사용자가 선택한 주소가 도로명 타입일때 조합한다.
						if (data.userSelectedType === 'R') {
							//법정동명이 있을 경우 추가한다.
							if (data.bname !== '') {
								extraAddr += data.bname;
							}
							// 건물명이 있을 경우 추가한다.
							if (data.buildingName !== '') {
								extraAddr += (extraAddr !== '' ? ', '
										+ data.buildingName : data.buildingName);
							}
							// 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
							fullAddr += (extraAddr !== '' ? ' (' + extraAddr
									+ ')' : '');
						}

						// 우편번호와 주소 정보를 해당 필드에 넣는다.
						document.getElementById('sample6_postcode').value = data.zonecode; //5자리 새우편번호 사용
						document.getElementById('sample6_address1').value = fullAddr;

						// 커서를 상세주소 필드로 이동한다.
						document.getElementById('sample6_address2').focus();
					}
				}).open();
	}
</script>
</head>
<body>
	<!-- 핵심 로직 -->
	<h1>회원 수정</h1>
	<div class="container-fluid">
		<hr>
		<!-- 핵심 로직 -->

		<form action="adminSellerModify" 
			method="post" name="adminSellerModify"
			onsubmit="return validateForm()">
	       <h4>아이디</h4>
	       
			<input type="hidden" name="id" maxlength="30" value="${memberlist.id}">

			<h5>패스워드</h5>
			<input type="password" name="password" style="width: 500px;"
				 placeholder="패스워드를 입력해주세요" />

			<h5>패스워드 확인</h5>
			<input type="password" name="password2" style="width: 500px;"
				placeholder="패스워드를 한번더 입력해주세요" />

			<h5>이름</h5>
			<input type="text" name="name" style="width: 500px;"
				 placeholder="이름을 입력해주세요" />

			<h5>이메일</h5>
			<input type="text" name="email" style="width: 500px;"
				 placeholder="이메일 주소를 입력해주세요" /><br />
				 
			<h5>휴대폰</h5>	 
			<input type="tel" name="phone" style="width: 500px;" 
			placeholder="휴대폰 번호를 입력해주세요" maxlength="11" /><br />
			
			<h5>우편번호</h5>
			<input placeholder="우편번호" type="text" id="sample6_postcode" name="zipcode" onclick="this.value=''" />
			<input type="button" onclick="sample6_execDaumPostcode()" value="우편번호 찾기" name="" />
			<h5>주소</h5>
			<input placeholder="주소" type="text" id="sample6_address1" name="address1" onclick="this.value=''" />
			<h5>상세 주소</h5>
			<input placeholder="상세주소" type="text" id="sample6_address2" name="address2" onclick="this.value=''" />
			
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="submit" value="회원 수정" /> 
			<input type="reset" value="되돌리기" /> 
				<input type="button" onclick="javascript:history.back()" value="목록으로">
		</form>
	</div>
</body>