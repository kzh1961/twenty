
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<title>회원가입</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="/resources/web/css/bootstrap.css" rel="stylesheet"
	type="text/css" />
<script type="text/javascript">
	function validateForm() {
		var a = document.forms["adminPensionWriteForm"]["pen_name"].value;
		var b = document.forms["adminPensionWriteForm"]["pen_code"].value;
		var c = document.forms["adminPensionWriteForm"]["pen_phone"].value;
		var d = document.forms["adminPensionWriteForm"]["content"].value;
		var e = document.forms["adminPensionWriteForm"]["pen_img1"].value;
		var f = document.forms["adminPensionWriteForm"]["pen_img2"].value;

		if (a == null || a == "") {
			alert("펜션 이름을 입력해야합니다");
			return false;
		} else if (b == null || b == "") {
			alert("펜션 주소를 입력해야합니다");
			return false;
		} else if (c == null || c == "") {
			alert("펜션 연락처를 입력해야합니다");
			return false;
		} else if (d == null || d == "") {
			alert("펜션 내용을 입력해야합니다");
			return false;
		} else if (e == null || e == "") {
			alert("메인이미지를 입력해야합니다");
			return false;
		} else if (f == null || f == "") {
			alert("서브이미지를 입력해야합니다");
			return false;
		}
	}
</script>
</head>
<body>
	<!-- 핵심 로직 -->
	<h1>펜션 수정</h1>
	<div class="container-fluid">
		<hr>
		<!-- 핵심 로직 -->

		<form action="adminPensionModify" enctype="multipart/form-data"
			method="post" name="adminPensionWriteForm"
			onsubmit="return validateForm()">
			<h5>펜션 이름</h5>
			<input readonly="readonly" type="text" name="pen_name" style="width: 500px;"
				value="${pension.pen_name }" />
			<h5>펜션 주소</h5>
			<input type="text" name="pen_code" style="width: 500px;"
				value="${pension.pen_code }"  />
			<h5>펜션 연락처</h5>
			<input type="text" name="pen_phone" style="width: 500px;"
				value="${pension.pen_phone }"  maxlength="11" /><br>
				<p class="help-block">-를 제외하고 숫자만 입력해주세요.</p>
			<h5>펜션 내용</h5>
			<input type="text" name="content" style="width: 500px;"
				value="${pension.content }"  /><br />
				 메인 이미지<input type="file"
				name="pen_img1" /><br />
			<p class="help-block">펜션 메인이미지 입니다 800x800 사이즈 권장합니다</p>
			서브 이미지<input type="file" name="pen_img2" /><br />
			<p class="help-block">펜션 서브이미지 입니다 1000x(2500~3800)사이즈 권장</p>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input
				type="reset" value="되돌리기" /> <input type="submit" value="펜션 수정" />
				<input type="hidden" name="old_img1" value="${pension.pen_image1 }"/>
				<input type="hidden" name="old_img2" value="${pension.pen_image2 }"/>
		</form>
	</div>
</body>
</html>