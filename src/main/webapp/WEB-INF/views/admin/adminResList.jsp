<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<head>
<script type="text/javascript">
	function delchk(res_num) {
		var con =  confirm("삭제하시겠습니까?");
		if(con==true){
			location.href="adminResDelete?res_num="+res_num;
		}
		false;
	}
</script>

<!-- 핵심 로직 -->
<h1>예약현황</h1>
<div class="container-fluid">
	<hr>
	<!-- 핵심 로직 -->


	<div class="widget-box">
		<div class="widget-title">
			<span class="icon"><i class="icon-th"></i></span>
			<h5>Reservation List</h5>
			
		</div>


		<div class="dataTables_filter" id="example_filter">
			<label>Search: <input type="text" aria-controls="example"></label>
		</div>


		<div class="widget-content nopadding">
			<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper"
				role="grid">
				<div class="">
					<div id="DataTables_Table_0_length" class="dataTables_length"></div>
				</div>
				<table class="table table-bordered data-table dataTable"
					id="DataTables_Table_0">
					<thead>
						<tr role="row">
						

						
						
						
							<th style="width: 7%; text-align: center;">예약번호</th>
							<th style="width: 7%; text-align: center;">아이디</th>
							<th style="width: 8%; text-align: center;">이름</th>
							<th style="width: 6%; text-align: center;">인원수</th>
							<th style="width: 8%; text-align: center;">폰번호</th>
							<th style="width: 10%; text-align: center;">내용</th>
							<th style="width: 10%; text-align: center;">펜션이름</th>
							<th style="width: 8%; text-align: center;">룸이름</th>
							<th style="width: 8%; text-align: center;">시작일</th>
							<th style="width: 8%; text-align: center;">종료일</th>
							<th style="width: 10%; text-align: center;">가격</th>
							<th style="width: 10%; text-align: center;">관리</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="adminResList" items="${adminResList}" varStatus="stat">
							<tr class="gradeA even" role="row">
							

							
								<td style="text-align: center; vertical-align: middle;">${adminResList.res_num}</td>
								<td style="text-align: center; vertical-align: middle;">${adminResList.id}</td>
								<td style="text-align: center; vertical-align: middle;">${adminResList.name}</td>
								<td style="text-align: center; vertical-align: middle;">${adminResList.people}</td>
								<td style="text-align: center; vertical-align: middle;">${adminResList.phone}</td>
								<td style="text-align: center; vertical-align: middle;">${adminResList.content}</td>
								<td style="text-align: center; vertical-align: middle;">${adminResList.pen_name}</td>
								<td style="text-align: center; vertical-align: middle;">${adminResList.room_num}</td>
								<td style="text-align: center; vertical-align: middle;">${adminResList.st_day}</td>
								<td style="text-align: center; vertical-align: middle;">${adminResList.end_day}</td>
					
								
										<td style="text-align: center; vertical-align: middle;">${adminResList.price}</td>
								<td style="text-align: center; vertical-align: middle;"><input type="image" onclick="javascript:location.href='adminResModifyForm?res_num=${adminResList.res_num}'"
											src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/92/Cog_font_awesome.svg/32px-Cog_font_awesome.svg.png">&nbsp;&nbsp;
									<input type="image"  src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Trash_font_awesome.svg/32px-Trash_font_awesome.svg.png"
										onclick="return delchk('${adminResList.res_num}')"/></td>
								
								
								
						


							</tr>
						</c:forEach>
						<!--  등록된 공지사항이 없을때 -->
						<c:if test="${fn:length(adminResList) le 0}">
							<tr>
								<td colspan="9" style="text-align: center;">예약 내역이 없습니다.</td>
							</tr>
						</c:if>
					</tbody>
				</table>
				<!-- <div><input class="btn-primary" type="submit" value="글쓰기" onclick="javascript:location.href='adminNoticeWriteForm'"  /></div> -->


			</div>
		</div>
	</div>