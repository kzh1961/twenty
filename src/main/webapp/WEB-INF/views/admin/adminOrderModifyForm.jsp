<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<title>주문 수정</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="text/javascript">
	function validateForm() {
		var a = document.forms["adminOrderModifyForm"]["goods_subtitle"].value;
		var b = document.forms["adminOrderModifyForm"]["goods_name"].value;
		var c = document.forms["adminOrderModifyForm"]["goods_amount"].value;
		var d = document.forms["adminOrderModifyForm"]["goods_price"].value;
		var e = document.forms["adminOrderModifyForm"]["file1"].value;
		var f = document.forms["adminOrderModifyForm"]["file2"].value;
		var g = document.forms["adminOrderModifyForm"]["file3"].value;

		if (a == null || a == "") {
			alert("상품부제를 입력해야합니다");
			return false;
		} else if (b == null || b == "") {
			alert("상품명을 입력해야합니다");
			return false;
		} else if (c == null || c == "") {
			alert("상품수량을 입력해야합니다");
			return false;
		} else if (d == null || d == "") {
			alert("상품가격를 입력해야합니다");
			return false;
		} else if (e == null || e == "") {
			alert("상단이미지를 입력해야합니다");
			return false;
		} else if (f == null || f == "") {
			alert("중단이미지를 입력해야합니다");
			return false;
		} else if (g == null || g == "") {
			alert("하단이미지를 입력해야합니다");
			return false;
		}
	}
</script>
</head>
<body>
	<!-- 핵심 로직 -->
	<h1>주문 수정</h1>
	<div class="container-fluid">
		<hr>
		<!-- 핵심 로직 -->

		<form action="adminOrderModify" enctype="multipart/form-data"
			method="post" name="adminOrderModifyForm"
			onsubmit="return validateForm()">
			
	<input type="hidden" name="order_num"
				value="${orderModel.order_num}" />
				
							<h5>송장번호</h5>
			<input type="text" name="order_trans_num" style="width: 500px;"
				value="${orderModel.order_trans_num}" placeholder="송장번호를 입력해주세요" />
				
			<h5>상품번호 : ${orderModel.order_goods_num}</h5>
			<h5>상품이름 : ${orderModel.order_goods_name}</h5>
			
												<h5>상품가격</h5>
			<input type="text" name="order_goods_price" style="width: 500px;"
				value="${orderModel.order_goods_price}" placeholder="상품가격" />
				
													<h5>주문개수</h5>
			<input type="text" name="order_goods_amount" style="width: 500px;"
				value="${orderModel.order_goods_amount}" placeholder="주문개수" />
				
			<h5>주문회원 : ${orderModel.order_member_id}</h5>
			
									<h5>받는사람</h5>
			<input type="text" name="order_receive_name" style="width: 500px;"
				value="${orderModel.order_receive_name}" placeholder="받는사람" />
						
						<h5>주소</h5>
			<input type="text" name="order_receive_addr" style="width: 500px;"
				value="${orderModel.order_receive_addr}" placeholder="주소" />
						
							<h5>우편번호</h5>
			<input type="text" name="order_receive_zipcode" style="width: 500px;"
				value="${orderModel.order_receive_zipcode}" placeholder="우편번호" />
				
				<h5>전화번호</h5>
				<input type="text" name="order_receive_mobile" style="width: 500px;"
				value="${orderModel.order_receive_mobile}" placeholder="전화번호" />
				
							<h5>메모</h5>
			<textarea name="order_memo" rows="5" style="width: 500px;" 
				value="${orderModel.order_memo}" placeholder="메모"></textarea>
			
			<h5>주문 총 금액</h5>
			<input type="text" name="order_sum_money" style="width: 500px;"
				value="${orderModel.order_sum_money}" placeholder="주문 총 금액을 입력해주세요" />
			
			<h5>주문날짜 : ${orderModel.order_date}</h5>
			
			<h5>주문 배송 상태</h5>
			<SELECT style="width: 510px;" name="order_status">
				<OPTION VALUE=배송준비 SELECTED>배송준비</OPTION>
				<OPTION VALUE=배송중>배송중</OPTION>
				<OPTION VALUE=배송완료>배송완료</OPTION>
			</SELECT>
			<p class="help-block">배송 상태 변경칸입니다.</p>
			
						<h5>사용포인트 : ${orderModel.usepoint}</h5>

			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input
				type="reset" value="되돌리기" /> <input type="submit" value="주문 수정" />
		</form>
	</div>
</body>
</html>