<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<title>공지사항 등록</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="text/javascript">
	function validateForm() {
		var a = document.forms["adminNoticeWriteForm"]["subject"].value;
		var b = document.forms["adminNoticeWriteForm"]["content"].value;

		if (a == null || a == "") {
			alert("제목을 입력해야합니다.");
			return false;
		} else if (b == null || b == "") {
			alert("내용을 입력해야합니다");
			return false;
		}
	}
</script>
</head>
<body>
	<!-- 핵심 로직 -->
	<h1>공지사항 등록</h1>
	<div class="container-fluid">
		<hr>
		<!-- 핵심 로직 -->

		<form action="adminNoticeWrite" enctype="multipart/form-data"
			method="post" name="adminNoticeWriteForm"
			onsubmit="return validateForm()">
			
			<h5>상품 부제</h5>
			<input type="text" name="subject" style="width: 500px;"
				placeholder="공지사항 제목을 입력해주세요" />
				
			<h5>내 용</h5>
			<textarea name="content" rows="10" style="width: 500px;"
				 placeholder="공지사항 제목을 입력해주세요"></textarea>
			<br/>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp; <input
				type="reset" value="되돌리기" /> <input type="submit" value="등록" />
		</form>
	</div>
</body>
</html>








<%-- 


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript">
	function list() {
		if (confirm("목록으로 가시겠습니까?") == true) {
			location.href = 'adminNoticeList';
		} else {
			return;
		}
	}
</script>
</head>
<body>
	<div class="container">
		<div class="register-top heading">
			<br> <br>
			<h2>공지사항</h2>
			<br> <br>
		</div>
		<form:form commandName="noticeModel" action="./adminNoticeWrite"
			method="post">
			<div class="col-sm-offset-1 col-sm-10 form-group">
				<label for="subject">제목:</label>
				<form:input class="form-control" type="text" path="subject"
					required="requiered" />
			</div>
			<div class="col-sm-offset-1 col-sm-10 form-group">
				<label for="content">내용:</label>
				<form:textarea class="form-control" rows="20" path="content"
					required="requiered"></form:textarea>

			</div>
			<br>
			<div class="col-sm-offset-1 col-sm-10 form-group"
				style="text-align: center">
				<button type="submit" class="btn btn-info">등록하기</button>
				<button type="button" class="btn btn-default" onclick="list();">취소</button>
			</div>
		</form:form>
	</div>
	<br>
	<br>
	<br>
	<br>
	<br>
</body>
</html> --%>