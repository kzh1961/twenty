<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<title>공지사항 수정</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="text/javascript">
	function validateForm() {
		var a = document.forms["adminNoticeModifyForm"]["subject"].value;
		var b = document.forms["adminNoticeModifyForm"]["content"].value;

		if (a == null || a == "") {
			alert("제목을 입력해야합니다.");
			return false;
		} else if (b == null || b == "") {
			alert("내용을 입력해야합니다");
			return false;
		}
	}
</script>
</head>
<body>
	<!-- 핵심 로직 -->
	<h1>공지사항 수정</h1>
	<div class="container-fluid">
		<hr>
		<!-- 핵심 로직 -->

		<form action="adminNoticeModify" enctype="multipart/form-data"
			method="post" name="adminNoticeModifyForm"
			onsubmit="return validateForm()">
			
			<input type="hidden" name="notice_num"
				value="${noticeModel.notice_num}" />

			<h5>공지사항 제목</h5>
			<input type="text" name="subject" style="width: 500px;"
				value="${noticeModel.subject}" placeholder="공지사항 제목을 입력해주세요" />
				
			<h5>내 용</h5>
			<textarea name="content" rows="10" style="width: 500px;" 
				value="${noticeModel.content}" placeholder="공지사항 제목을 입력해주세요"></textarea>
			<br/>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input
				type="reset" value="되돌리기" /> <input type="submit" value="수정" />
		</form>
	</div>
</body>
</html>