<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<head>
<script type="text/javascript">
	function delchk() {
		return confirm("삭제하시겠습니까?");
	}
</script>

<!-- 핵심 로직 -->
<h1>펜션 룸 등록</h1>
<div class="container-fluid">
	<hr>
<!-- 핵심 로직 -->


	<div class="widget-box">
		<div class="widget-title">
			<span class="icon"><i class="icon-th"></i></span>
			<h5>Pension List</h5> 
		</div>


		<div class="dataTables_filter" id="example_filter">
			<label>Search: <input type="text" aria-controls="example"></label>
		</div>


		<div class="widget-content nopadding">
			<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper"
				role="grid">
				<div class="">
					<div id="DataTables_Table_0_length" class="dataTables_length"></div>
				</div>
				<table class="table table-bordered data-table dataTable"
					id="DataTables_Table_0">
					<thead>
									<tr>
									

									
								<th style="width: 20%; text-align: center;">펜션 이름</th>
								<th style="width: 25%; text-align: center;">펜션 주소</th>
								<th style="width: 15%; text-align: center;">펜션 전화번호</th>
								<th style="width: 30%; text-align: center;">내용</th>
								<th style="width: 10%; text-align: center;">객실 등록</th>

								
								

							</tr>
								</thead>
					<tbody>
								<c:forEach var="adminPensionList"  items="${adminPensionList}" varStatus="stat">															
									<tr class="gradeA even" role="row">
									
										<td style="text-align: center; vertical-align: middle;">${adminPensionList.pen_name}</td>
										<td style="text-align: center; vertical-align: middle;">${adminPensionList.pen_code}</td>
										<td style="text-align: center; vertical-align: middle;">${adminPensionList.pen_phone}</td>
									
										<td style="text-align: center; vertical-align: middle;">${adminPensionList.content}</td>
										<td style="text-align: center; vertical-align: middle;">
										<input type="image" onclick="javascript:location.href='adminRoomWriteForm1?pen_name=${adminPensionList.pen_name}'"
											src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/92/Cog_font_awesome.svg/32px-Cog_font_awesome.svg.png">
										
										</td>
										
										
										
										
								
									
									
									</tr>
								</c:forEach>
								<!--  등록된 Pension이 없을때 -->
									<c:if test="${fn:length(adminPensionList) le 0}">
										<tr><td colspan="9" style="text-align:center;">등록된 펜션이 없습니다</td></tr>
									</c:if> 
								</tbody>
				</table>
				<!-- <div><input class="btn-primary" type="submit" value="글쓰기" onclick="javascript:location.href='adminNoticeWriteForm'"  /></div> -->
				
				
			</div>
		</div>
	</div>
	
	
	<%-- 
	<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<title>Insert title here</title>
</head>
<body>
<table style="border:1px solid #ccc">
    <colgroup>
        <col width="10%"/>
        <col width="*"/>
        <col width="15%"/>
        <col width="20%"/>
    </colgroup>
    <thead>
        <tr>
            <th scope="col">펜션이름</th>
            <th scope="col">펜션주소</th>
            <th scope="col">펜션전화번호</th>
            <th scope="col">펜션내용</th>
        </tr>
    </thead>
    <tbody>
        <c:choose>
            <c:when test="${fn:length(adminPensionList) > 0}">
                <c:forEach items="${adminPensionList }" var="adminPensionList">
                    <tr>
                        <td><a href="adminRoomWriteForm1?pen_name=${adminPensionList.pen_name }" >${adminPensionList.pen_name }</a></td>
                        <td>${adminPensionList.pen_code }</td>
                        <td>${adminPensionList.pen_phone }</td>
                        <td>${adminPensionList.content }</td>
                    </tr>
                </c:forEach>
            </c:when>
            <c:otherwise>
                <tr>
                    <td colspan="4">조회된 결과가 없습니다.</td>
                </tr>
            </c:otherwise>
        </c:choose>
         
    </tbody>
</table>

</body>
</html> --%>