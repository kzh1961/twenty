<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<head>
<script type="text/javascript">
	function delchk() {
		return confirm("삭제하시겠습니까?");
	}
</script>

<!-- 핵심 로직 -->
<h1>상품관리</h1>
<div class="container-fluid">
	<hr>
	<!-- 핵심 로직 -->


	<div class="widget-box">
		<div class="widget-title">
			<span class="icon"><i class="icon-th"></i></span>
			<h5>Goods List</h5>
		</div>


		<div class="dataTables_filter" id="example_filter">
			<label>Search: <input type="text" aria-controls="example"></label>
		</div>


		<div class="widget-content nopadding">
			<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper"
				role="grid">
				<div class="">
					<div id="DataTables_Table_0_length" class="dataTables_length"></div>
				</div>
				<table class="table table-bordered data-table dataTable"
					id="DataTables_Table_0">
					<thead>
						<tr>
							<th style="width: 6%; text-align: center;">번호</th>
							<th style="width: 8%; text-align: center;">카테고리</th>
							<th style="width: 8%; text-align: center;">상품사진</th>
							<th style="width: 32%; text-align: center;">상품명</th>
							<th style="width: 10%; text-align: center;">가격</th>
							<th style="width: 6%; text-align: center;">수량</th>
							<th style="width: 10%; text-align: center;">등록일자</th>
							<th style="width: 10%; text-align: center;">관리</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="goodsList" items="${goodsList}">
							<c:url var="viewURL" value="goodsView">
								<c:param name="goods_num" value="${goodsList.goods_num }" />


							</c:url>
							<tr>
								<td style="text-align: center; vertical-align: middle;">${goodsList.goods_num}</td>
								<td style="text-align: center; vertical-align: middle;">
								<c:if test="${goodsList.goods_category eq 1}">여성 의류</c:if>
								<c:if test="${goodsList.goods_category eq 2}">여성 시계&쥬얼리</c:if> <c:if
										test="${goodsList.goods_category eq 3}">여성 가방&신발</c:if> <c:if
										test="${goodsList.goods_category eq 4}">여성 패션 잡화</c:if> <c:if
										test="${goodsList.goods_category eq 5}">여성 언더웨어</c:if> <c:if
										test="${goodsList.goods_category eq 6}">남성 의류</c:if> <c:if
										test="${goodsList.goods_category eq 7}">남성 시계&쥬얼리</c:if>
										<c:if test="${goodsList.goods_category eq 8}">남성 가방&신발</c:if>
										<c:if test="${goodsList.goods_category eq 9}">남성 패션 잡화</c:if>
										<c:if test="${goodsList.goods_category eq 10}">남성 언더웨어</c:if>
										
										<c:if test="${goodsList.goods_category eq 11}">유아 의류</c:if>
										<c:if test="${goodsList.goods_category eq 12}">유아 신발</c:if>
										<c:if test="${goodsList.goods_category eq 13}">유아 잡화</c:if>
										
										<c:if test="${goodsList.goods_category eq 14}">스킨케어</c:if>
										<c:if test="${goodsList.goods_category eq 15}">메이크업</c:if>
										<c:if test="${goodsList.goods_category eq 16}">헤어</c:if>
										<c:if test="${goodsList.goods_category eq 17}">바디</c:if>
										<c:if test="${goodsList.goods_category eq 18}">네일</c:if>
										<c:if test="${goodsList.goods_category eq 19}">기저귀/물티슈</c:if>
										<c:if test="${goodsList.goods_category eq 20}">분유/유아식품</c:if>
										<c:if test="${goodsList.goods_category eq 21}">목욕/스킨케어</c:if>
										
										<c:if test="${goodsList.goods_category eq 22}">쌀/잡곡/채소/과일</c:if>
										<c:if test="${goodsList.goods_category eq 23}">축/수산</c:if>
										<c:if test="${goodsList.goods_category eq 24}">김치/반찬/젓갈</c:if>
										<c:if test="${goodsList.goods_category eq 25}">장/오일/조미료</c:if>
										<c:if test="${goodsList.goods_category eq 26}">건강식품</c:if>
										<c:if test="${goodsList.goods_category eq 27}">즉석식품</c:if>
										<c:if test="${goodsList.goods_category eq 28}">스낵</c:if>
										<c:if test="${goodsList.goods_category eq 29}">침대/매트릭스/매트</c:if>
										<c:if test="${goodsList.goods_category eq 30}">쇼파/의자/책상</c:if>
										</td>
								<td style="text-align: center; vertical-align: middle;"><img
									src="/twenty/resources/goods_images/${goodsList.goods_image1}"
									style="width: 60px; height: 60px;"/></td>
								<td style="text-align: center; vertical-align: middle;"><a
									href="/twenty/goodsView?goods_num=${goodsList.goods_num }">${goodsList.goods_name}</a></td>
								<td style="text-align: center; vertical-align: middle;"><fmt:formatNumber
										value="${goodsList.goods_price}" type="number" /></td>
								<td style="text-align: center; vertical-align: middle;">${goodsList.goods_amount}</td>

								<td style="text-align: center; vertical-align: middle;"><fmt:formatDate
										value="${goodsList.goods_date}" pattern="YY.MM.dd HH:mm" /></td>
								<td style="text-align: center; vertical-align: middle;"><a
									href="/twenty/sellerGoodsModifyForm?goods_num=${goodsList.goods_num }"><input
										type="image"
										src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/92/Cog_font_awesome.svg/32px-Cog_font_awesome.svg.png"></a>&nbsp;&nbsp;
									<c:url var="viewURL2" value="sellerGoodsDelete">
										<c:param name="goods_num" value="${goodsList.goods_num }" />
									</c:url> <a href="${viewURL2}"><input type="image"
										src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Trash_font_awesome.svg/32px-Trash_font_awesome.svg.png"
										onclick="return delchk()"></a></td>
							</tr>
						</c:forEach>
						<!--  등록된 상품이 없을때 -->
						<c:if test="${fn:length(goodsList) le 0}">
							<tr>
								<td>등록된 상품이 없습니다</td>
							</tr>
						</c:if>
					</tbody>
				</table>
				<!-- <div><input class="btn-primary" type="submit" value="글쓰기" onclick="javascript:location.href='adminNoticeWriteForm'"  /></div> -->


			</div>
		</div>
	</div>
</div>
</head>

<%-- 





							<c:forEach var="goodsList" items="${goodsList}">
								<c:url var="viewURL" value="goodsView">
									<c:param name="goods_num" value="${goodsList.goods_num }" />


								</c:url>
								<tr>
									<td style="text-align: center; vertical-align: middle;">${goodsList.goods_num}</td>
									<td style="text-align: center; vertical-align: middle;"><c:if test="${goodsList.goods_category eq 0}">사료</c:if>
										<c:if test="${goodsList.goods_category eq 1}">남성의류</c:if> <c:if
											test="${goodsList.goods_category eq 2}">시계</c:if> <c:if
											test="${goodsList.goods_category eq 3}">등등</c:if> <c:if
											test="${goodsList.goods_category eq 4}">미정</c:if></td>
									<td style="text-align: center; vertical-align: middle;"><img src="/twenty/resources/goods_images/${goodsList.goods_image1}" width="170" height="170"/></td>
									<td style="text-align: center; vertical-align: middle;"><a
										href="/twenty/goods/goodsView?goods_num=${goodsList.goods_num }&currentPage=${currentPage}">${goodsList.goods_name}</a></td>
									<td style="text-align: center; vertical-align: middle;"><fmt:formatNumber value="${goodsList.goods_price}"
											type="number" /></td>
									<td style="text-align: center; vertical-align: middle;">${goodsList.goods_amount}</td>

									<td style="text-align: center; vertical-align: middle;"><fmt:formatDate value="${goodsList.goods_date}"
											pattern="YY.MM.dd HH:mm" /></td>
									<td style="text-align: center; vertical-align: middle;"><a
										href="/twenty/admin/adminGoodsModifyForm?goods_num=${goodsList.goods_num }"><input
											type="image"
											src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/92/Cog_font_awesome.svg/32px-Cog_font_awesome.svg.png"></a>&nbsp;&nbsp;
										<c:url var="viewURL2" value="adminGoodsDelete">
											<c:param name="goods_num" value="${goodsList.goods_num }" />
										</c:url> <a href="${viewURL2}"><input type="image"
											src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Trash_font_awesome.svg/32px-Trash_font_awesome.svg.png"
											onclick="return delchk()"></a></td>
								</tr>
							</c:forEach>
							<!--  등록된 상품이 없을때 -->
							<c:if test="${fn:length(goodsList) le 0}">
								<tr>
									<td>등록된 상품이 없습니다</td>
								</tr>
							</c:if>
						</tbody>
				</table>
				<!-- <div><input class="btn-primary" type="submit" value="글쓰기" onclick="javascript:location.href='adminNoticeWriteForm'"  /></div> -->
				
				
			</div>
		</div>
	</div>
</div></head>
 --%>