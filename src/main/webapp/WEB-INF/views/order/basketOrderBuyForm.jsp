<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>  
<link href="/resources/web/css/bootstrap.css" rel="stylesheet"
	type="text/css" />
<script>
$(document).ready(function() {
	$('body,html').animate({
		scrollTop : 800
	}, 700);
	return false;
});
</script>
<style type="text/css">
#basket > li{float:left;font-size:20px;margin-top:0.7%;}
#basketList > li{float:left;background-color:#f9f9f9;height:150px;line-height:150px;font-size:22px}
	.orderArea h3{
	    margin: 30px 0 5px 5px;
	    font-size: 12px;
	    color: #888;
	    font-weight: bold;
	}
	.orderArea .boardWrite table{
		table-layout: fixed;
	    border-top: 1px solid #ccc;
	    border-bottom: 1px solid #ccc;
	    font-size: 12px;
	    width: 100%;
	    border: 0;
	    border-spacing: 0;
	    border-collapse: collapse;
	    font-weight: 800;
	}
	.orderArea .boardWrite table th{
	    width: 140px;
	    padding: 5px 0 5px 22px;
	    border-top: 1px solid #e7e7e7;
	    background: #FAFAFA;
	    color: #888;
	    text-align: left;
	    line-height: 140%;
	    border-left: 0px;
	}
	.orderArea .boardWrite table td{
		width: auto;
	    padding: 5px 0 3px 22px;
	    border-top: 1px solid #e7e7e7;
	    color: #8f8f8f;
	    line-height: 140%;
	    font-size: 12px;
	    border-right: 0px;
	}
	.boardWrite input[type="text"]{
		text-indent: 5px;
	    font-size: 12px;
	    border: 1px solid #cfcecd;
	    background: #fff;
	}
</style>
<div class="basket_list">
	<div class="basket_list_top" align="center">
		<h2 class="basketcart"><img src="http://okidogki.com/web/upload/goodymallSkin/title/order.gif" alt="장바구니"></h2>
		<img style="width:1000px;" src="https://pics.auction.co.kr/myauction/order/title_order_step.gif" alt="step 01 장바구니">
	</div></div>
	<br>
	<br>
	<center>
	<div style="width:1000px">
	 <h3 style="text-align:left;">장바구니 수량 (${fn:length(basketList) })</h3>
			
				<ul id="basket" style="border:1px solid #BCBBBB;">
					<li style="width:25%"><span>상품 이미지</span></li>
					<li style="width:25%"><span>상품명</span></li>		
					<li style="width:25%"><span>수량</span></li>
					<li style="width:25%"><span>상품 금액</span></li>
					<hr/>
				</ul>
			
			<c:forEach var="basketList"  items="${basketList}" varStatus="stat">
				<ul id="basketList">
					<li style="width:25%"><span><a href="/twenty/goodsView?goods_num=${basketList.basket_goods_num }"><img width="100px" height="125px" src="/twenty/resources/goods_images/${basketList.basket_goods_image1 }"/></a></span></li>
					<li style="width:25%"><span>${basketList.basket_goods_name}</span></li>		
					<li style="width:25%"><span>${basketList.basket_goods_amount}EA</span></li>
					<li style="width:25%"><span><fmt:formatNumber value="${basketList.basket_goods_price*basketList.basket_goods_amount}" type="number"/>&nbsp;원</span></li>
					<hr/>
				</ul>	
				<div class="clearfix" style="border:1px solid black"> </div>
				<c:set var= "sum" value="${sum + basketList.basket_goods_price * basketList.basket_goods_amount}"/>
				
			</c:forEach>

			<br>
							<div align="right" style="background-color:#f3f3f3;border-top:1px solid black;border-bottom:1px solid black">
							상품구매금액 <strong><fmt:formatNumber value="${sum}" type="number"/> </strong> + 배송비 <strong>0</strong> - 포인트 <strong><fmt:formatNumber value="${usepoint}" type="number"/></strong> = <strong style="color: #f8941d;font-size: 14px;" >합계 : <fmt:formatNumber value="${sum - usepoint}" type="number"/>원 </strong></div>

<div class="orderArea" style="margin-bottom: 50px;">
		<div class="boardWrite" style="text-align:left">
			<table border="1" summary="">
				<caption>배송지 정보</caption>
				<tbody>
					<tr>
						<th scope="row">주소</th>
						<td>
							${orderModel.order_receive_zipcode}<br>${orderModel.order_receive_addr}
						</td>
					</tr>
					<tr>
						<th scope="row">이름</th>
						<td>${orderModel.order_receive_name}</td>
					</tr>
					<tr>
						<th scope="row">휴대폰</th>
						<td>${orderModel.order_receive_mobile}</td>						
					</tr> 
					<tr>
						<th scope="row">배송요청사항</th>
						<td>
							${orderModel.order_memo}</td>
					</tr>       
				</tbody>
			</table>
		</div>
</div>

<form action="basketOrderOk" method="post">
<input type="hidden" name="order_member_id" value="${orderModel.order_member_id }" />
<input type="hidden" name="order_receive_name" value="${orderModel.order_receive_name }" />
<input type="hidden" name="order_receive_addr" value="${orderModel.order_receive_addr }" />
<input type="hidden" name="order_receive_mobile" value="${orderModel.order_receive_mobile }" />
<input type="hidden" name="order_receive_zipcode" value="${orderModel.order_receive_zipcode }" />
<input type="hidden" name="order_memo" value="${orderModel.order_memo }" />
<input type="hidden" name="usepoint" value="${usepoint }" />
<input type="hidden" name="order_sum_money" value="${orderModel.order_sum_money }" />


<div class="col-1g-9" align="center">
<button type="submit" class="btn btn-info" style="width: 800px; height: 50px; font-size: 20pt; font-weight: bold; margin-bottom: 50px;">결제하기</button>				

</div>	
</div>

</form>














