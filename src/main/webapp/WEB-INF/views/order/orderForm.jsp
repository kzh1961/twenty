<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@	taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="/resources/web/css/bootstrap.css" rel="stylesheet"
	type="text/css" />
<script type="text/javascript">
 
 function validateForm() {
	
	var a = document.forms["order"]["usepoint"].value;


	if (a == null || a == "") {
		alert("최소 사용포인트는 1000포인트 입니다");
		return false;
}
</script>
<style type="text/css">
#basket > li{float:left;font-size:20px;margin-top:0.7%;}
#basketList > li{float:left;background-color:#f9f9f9;height:150px;line-height:150px;font-size:22px}
	.orderArea h3{
	    margin: 30px 0 5px 5px;
	    font-size: 12px;
	    color: #888;
	    font-weight: bold;
	}
	.orderArea .boardWrite table{
		table-layout: fixed;
	    border-top: 1px solid #ccc;
	    border-bottom: 1px solid #ccc;
	    font-size: 12px;
	    width: 100%;
	    border: 0;
	    border-spacing: 0;
	    border-collapse: collapse;
	    font-weight: 800;
	}
	.orderArea .boardWrite table th{
	    width: 140px;
	    padding: 5px 0 5px 22px;
	    border-top: 1px solid #e7e7e7;
	    background: #FAFAFA;
	    color: #888;
	    text-align: left;
	    line-height: 140%;
	    border-left: 0px;
	}
	.orderArea .boardWrite table td{
		width: auto;
	    padding: 5px 0 3px 22px;
	    border-top: 1px solid #e7e7e7;
	    color: #8f8f8f;
	    line-height: 140%;
	    font-size: 12px;
	    border-right: 0px;
	}
	.boardWrite input[type="text"]{
		text-indent: 5px;
	    font-size: 12px;
	    border: 1px solid #cfcecd;
	    background: #fff;
	}

</style>
</head>

<body>
<div style="margin-bottom: 70px">
<div  class="basket_list" align="center">
	<div class="basket_list_top">
		<h2 class="basketcart"><img src="http://okidogki.com/web/upload/goodymallSkin/title/order.gif" alt="장바구니"></h2>
		<img style="width:1000px;" src="https://pics.auction.co.kr/myauction/order/title_order_step.gif" alt="step 01 장바구니">
	</div>
	<br>
	<div style="width:1000px">			
				<ul id="basket" style="border:1px solid #BCBBBB;">
					<li style="width:25%"><span>상품 이미지</span></li>
					<li style="width:45%"><span>상품명</span></li>		
					<li style="width:10%"><span>수량</span></li>
					<li style="width:20%"><span>상품 금액</span></li>
					<hr/>
				</ul>
				<ul id="basketList">
					<li style="width:25%"><span><a href="/twenty/goodsView?goods_num=${goodsModel.goods_num }"><img width="100px" height="125px" src="/twenty/resources/goods_images/${goodsModel.goods_image1 }"/></a></span></li>
					<li style="width:45%"><span>${goodsModel.goods_name }</span></li>		
					<li style="width:10%"><span>${goodsModel.goods_amount}EA</span></li>
					<li style="width:20%"><span><fmt:formatNumber value="${goodsModel.goods_price * goodsModel.goods_amount}" type="number"/>&nbsp;원</span></li>
					<hr/>
				</ul>	
				<div class="clearfix" style="border:1px solid black"> </div>
				

			<br>
							<div align="right" style="background-color:#f3f3f3;border-top:1px solid black;border-bottom:1px solid black">
							상품구매금액 <strong><fmt:formatNumber value="${goodsModel.goods_price * goodsModel.goods_amount}" type="number"/> </strong> + 배송비 <strong>0</strong> = <strong style="color: #f8941d;font-size: 14px;">합계 : <fmt:formatNumber value="${goodsModel.goods_price * goodsModel.goods_amount}" type="number"/>원 </strong></div>	
</div>
</div>

<form name="order" action="orderForm1" method="post" onsubmit="return validateForm()" >
<input type="hidden" name="order_sum_money" value="${goodsModel.goods_price * goodsModel.goods_amount}" />
<input type="hidden" name="id" value="${memberModel.id }" />
<input type="hidden" name="goods_num" value="${goodsModel.goods_num}" />
<input type="hidden" name="goods_amount" value="${goodsModel.goods_amount}"/>


<center>
<div class="orderArea" style="width:1000px;text-align:left;">
	
		<div class="boardWrite">
			<table border="1" summary="">
				<caption>결재자 정보</caption>
				<tbody>
					<tr>
						<th scope="row">성명</th>
						<td>${memberModel.name}</td>
					</tr>
					<tr>
						<th scope="row">휴대폰</th>
						<td>${memberModel.phone}</td>
					</tr>
					<tr>
						<th scope="row">이메일</th>
						<td>${memberModel.email}&nbsp;&nbsp;&nbsp;&nbsp;<font style="color:#cfcfcf;">*제품구입시 E-mail을 통해 주문처리과정을 보내 드립니다.</font></td>
					</tr>              
				</tbody>
			</table>
		</div>
</div>
<div class="orderArea" style="width:1000px;text-align:left; margin-bottom: 50px;">
	
		<div class="boardWrite">
			<table border="1" summary="">
				<caption>배송지 정보</caption>
				<tbody>
					<tr>
						<th scope="row">주소</th>
						<td><input type="text"  name="zipcode" onclick="this.value=''" id="zipcode" value="${memberModel.zipcode}" style="margin-bottom:2px;"/> 
								<a href="#none" title="우편번호(새창으로 열기)" id="postBtn"><img style="margin-bottom:5px;" src="http://img.echosting.cafe24.com/design/skin/default/member/btn_zip.gif" alt="우편번호"></a><br>
							<input type="text"  style="width:100%;margin-bottom:2px;" name="address1" id="address1" value="${memberModel.address1}"  style="margin-bottom:5px;"/><br>
                   	 		<input type="text" style="width:100%;" name="address2" id="address2" />
						</td>
					</tr>
					<tr>
						<th scope="row">이름</th>
						<td><input type="text" name="order_receive_name" value="${memberModel.name}" /></td>
					</tr>
					<tr>
						<th scope="row">휴대폰</th>
						<td><input type="text" name="order_receive_mobile" value="${memberModel.phone}" /></td>						
					</tr>
					<tr>
						<th scope="row">사용할 포인트(<br>최소 1000p<br> 보유포인트 ${memberModel.point }p)</th>
						<td><input type="text" name="usepoint" value="0"/>&nbsp;&nbsp;<font color="red">${me }</font></td>
						</tr>
					<tr>
						<th scope="row">배송요청사항</th>
						<td>
						<textarea style="width:100%;" name="order_memo"  placeholder=" 배송시요청사항 예)부재시 경비실에 맡겨주세요"></textarea></td>
					</tr> 
					
					          
				</tbody>
			</table>
		</div>
			
</div>
		
			

</center>
<!-- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->

<div class="col-1g-9" align="center">
<button type="submit" class="btn btn-info" style="width: 800px; height: 50px; font-size: 20pt; font-weight: bold;">주문하기</button>
</div>
</form>
</div>
</body>
</html>










