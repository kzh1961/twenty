<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type="text/javascript">


function delchk(){
    return confirm("삭제하시겠습니까?");
    
    
}
 function test(orderList)
 {
         var div_test = document.getElementById('test');
         div_test.innerHTML = orderList.order_receive_name;
        
 }
 
</script>
<style>
#basket > li{float:left;font-size:20px;margin-top:0.4%;}
#basketList > li{float:left;background-color:#f9f9f9;height:150px;font-size:22px;line-height:150px}
.paging {
	text-align: center;
	height: 32px;
	margin-top: 5px;
	margin-bottom: 15px;
}

.paging a, .paging strong {
	display: inline-block;
	width: 36px;
	height: 32px;
	line-height: 28px;
	font-size: 14px;
	border: 1px solid #e0e0e0;
	margin-left: 5px;
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius: 3px;
	-webkit-box-shadow: 1px 1px 1px 0px rgba(235, 235, 235, 1);
	-moz-box-shadow: 1px 1px 1px 0px rgba(235, 235, 235, 1);
	box-shadow: 1px 1px 1px 0px rgba(235, 235, 235, 1);
}

.paging a:first-child {
	margin-left: 0;
}

.paging strong {
	color: #fff;
	background: skyblue;
	border: 1px solid skyblue;
}

.paging .page_arw {
	font-size: 11px;
	line-height: 30px;
}
</style>

<div class="basket_list" align="center">
	<div class="basket_list_top">
		<h2 class="basketcart"><img src="http://okidogki.com/web/upload/goodymallSkin/title/order_list.gif" alt="장바구니"></h2>
	</div>
	<div style="width:1000px">
		
			<ul id="basket" style="border:1px solid #BCBBBB;">
					
						<li style="width:13%;text-align:right">주문일자</li>
						<li style="width:18%">이미지</li>
						<li style="width:18%">상품명</li>
						<li style="width:15%">수량</li>
						<li style="width:18%">상품 금액</li>
						<li style="width:18%">주문처리상태</li>
						<hr/>
			</ul>
			<c:set var="check" value="0"/>
 				<c:forEach var="order"  items="${orderList}" varStatus="status">	
					<ul id="basketList">
						<li style="width:13%;text-align:right;line-height:70px;"><c:if test="${check==0 }"><fmt:formatDate value="${order.order_date}" pattern="YY-MM-dd" /><c:set var="check" value="1"/><br>
						<font size="2">[ 송장 : ${order.order_trans_num} ]</font></c:if></li>
						<li style="width:18%"><a href="/twenty/goodsView?goods_num=${order.order_goods_num}"><img src="/twenty/resources/goods_images/${order.order_goods_image}" width="100" height="125"></a></li>
						<li style="width:18%"> ${order.order_goods_name}</a></li>
						<li style="width:15%">${order.order_goods_amount}EA</li>
						<li style="width:18%"><strong id="id2"><fmt:formatNumber value="${order.order_sum_money}" type="number"/>원</strong></li>
						<li style="width:18%">${order.order_status}</li>
					
					<c:set var="sum" value="${sum+(order.order_sum_money) }"/>
				
					<c:if test="${orderList[status.index].order_date != orderList[status.index+1].order_date}">
					<br><br>
				
						<div align="right" style="background-color:#f3f3f3;border-top:1px solid black;border-bottom:1px solid black">
						<font size="4">주문 금액 : <fmt:formatNumber value="${sum}" type="number"/> - 포인트 : <fmt:formatNumber value="${order.usepoint }" type="number"/>&nbsp;결제금액 : <fmt:formatNumber value="${sum - order.usepoint}"/></font>원
						<c:set var="sum" value="0"/></div>
						<div style="height:40px"></div>
						<c:set var="check" value="0"/>
					</c:if>
					
					</ul>
				</c:forEach>
					<c:if test="${fn:length(orderList) <= 0}">
						<div style="height:30px">
						<font size="2">주문리스트 담긴 상품이 없습니다.</font>
						</div>
					</c:if>
					<form>
				
				<div class="col-sm-offset-5 paging" style="display: inline;">${pagingHtml}	</div>                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			
		
			</form>

		</div>
</div>