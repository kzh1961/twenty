<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>    

	<html>
	<head>
	<link href="/resources/web/css/bootstrap.css" rel="stylesheet"
	type="text/css" />
	<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
    
<script type="text/javascript">
$(document).ready(function() {
		$('body,html').animate({
			scrollTop : 800
		}, 700);
		return false;
});
	
	function openZipcode(){
		var url="zipcodeCheckForm.dog";
		open(url, "confirm","toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=no, width=410, height=400");
	}


	
</script>
<style type="text/css">

#basket > li{float:left;font-size:20px;margin-top:0.7%;}
#basketList > li{float:left;background-color:#f9f9f9;height:150px;line-height:150px;font-size:22px}
	.orderArea h3{
	    margin: 30px 0 5px 5px;
	    font-size: 12px;
	    color: #888;
	    font-weight: bold;
	}
	.orderArea .boardWrite table{
		table-layout: fixed;
	    border-top: 1px solid #ccc;
	    border-bottom: 1px solid #ccc;
	    font-size: 12px;
	    width: 100%;
	    border: 0;
	    border-spacing: 0;
	    border-collapse: collapse;
	    font-weight: 800;
	}
	.orderArea .boardWrite table th{
	    width: 140px;
	    padding: 5px 0 5px 22px;
	    border-top: 1px solid #e7e7e7;
	    background: #FAFAFA;
	    color: #888;
	    text-align: left;
	    line-height: 140%;
	    border-left: 0px;
	}
	.orderArea .boardWrite table td{
		width: auto;
	    padding: 5px 0 3px 22px;
	    border-top: 1px solid #e7e7e7;
	    color: #8f8f8f;
	    line-height: 140%;
	    font-size: 12px;
	    border-right: 0px;
	}
	.boardWrite input[type="text"]{
		text-indent: 5px;
	    font-size: 12px;
	    border: 1px solid #cfcecd;
	    background: #fff;
	}

</style>

</head>

<body>



<form method="post" name="order" action="basketOrderBuyForm">
<input type="hidden" name="id" value="${memberModel.id }" />


<div class="basket_list">
	<div class="basket_list_top" align="center">
		<h2 class="basketcart"><img src="http://okidogki.com/web/upload/goodymallSkin/title/order.gif" alt="장바구니"></h2>
		<img style="width:1000px;" src="http://pics.auction.co.kr/myauction/buy/2012/title_basket_step.gif" alt="step 01 장바구니">
	</div>
	<br>
	<br>
	<center>
	<div style="width:1000px">
	 <h3 style="text-align:left;">장바구니 수량 (${fn:length(basketList) })</h3>
				<ul id="basket" style="border:1px solid #BCBBBB;">
					<li style="width:25%"><span>상품 이미지</span></li>
					<li style="width:45%"><span>상품명</span></li>		
					<li style="width:10%"><span>수량</span></li>
					<li style="width:20%"><span>상품 금액</span></li>
					<hr/>
				</ul>
			
			<c:forEach var="basketList"  items="${basketList}" varStatus="stat">
				<ul id="basketList">
					<li style="width:25%"><span><a href="/twenty/goodsView?goods_num=${basketList.basket_goods_num }"><img width="100px" height="125px" src="/twenty/resources/goods_images/${basketList.basket_goods_image1 }"/></a></span></li>
					<li style="width:45%"><span>${basketList.basket_goods_name}</span></li>		
					<li style="width:10%"><span>${basketList.basket_goods_amount}EA</span></li>
					<li style="width:20%"><span><fmt:formatNumber value="${basketList.basket_goods_price*basketList.basket_goods_amount}" type="number"/>&nbsp;원</span></li>
					<hr/>
				</ul>	
				<div class="clearfix" style="border:1px solid black"> </div>
				<c:set var= "sum" value="${sum + (basketList.basket_goods_price * basketList.basket_goods_amount)}"/>
				
			</c:forEach>

			<br>
							<div align="right" style="background-color:#f3f3f3;border-top:1px solid black;border-bottom:1px solid black">
							상품구매금액 <strong><fmt:formatNumber value="${sum}" type="number"/> </strong> + 배송비 <strong>0</strong> = <strong style="color: #f8941d;font-size: 14px;">합계 : <fmt:formatNumber value="${sum}" type="number"/>원 </strong></div>


<div class="orderArea" style="text-align:left">
		<div class="boardWrite">
			<table border="1" summary="">
				<caption>결재자 정보</caption>
				<tbody>
					<tr>
						<th scope="row">성명</th>
						<td>${memberModel.name}</td>
					</tr>
					<tr>
						<th scope="row">휴대폰</th>
						<td>${memberModel.phone}</td>
					</tr>
					<tr>
						<th scope="row">이메일</th>
						<td>${memberModel.email}&nbsp;&nbsp;&nbsp;&nbsp;<font style="color:#cfcfcf;">*제품구입시 E-mail을 통해 주문처리과정을 보내 드립니다.</font></td>
					</tr>              
				</tbody>
			</table>
		</div>
</div>
<div class="orderArea" style="text-align:left; margin-bottom: 50px;">
		<div class="boardWrite">
			<table border="1" summary="">
				<caption>배송지 정보</caption>
				<tbody>
					<tr>
						<th scope="row">주소</th>
						<td><input type="text"  name="order_receive_zipcode" onclick="this.value=''" id="zipcode" readonly value="${memberModel.zipcode}" style="margin-bottom:2px;"/> 
								<a href="#none" title="우편번호(새창으로 열기)" onclick="return openZipcode()" id="postBtn"><img style="margin-bottom:5px;" src="http://img.echosting.cafe24.com/design/skin/default/member/btn_zip.gif" alt="우편번호"></a><br>
							<input type="text"  style="width:100%;margin-bottom:2px;" name="addr" onclick="this.value=''" id="addr1" readonly value="${memberModel.address1}"  style="margin-bottom:5px;"/><br>
                   	 		<input type="text" style="width:100%;" name="adderss2" onclick="this.value=''" id="address2" />
						</td>
					</tr>
					<tr>
						<th scope="row">이름</th>
						<td><input type="text" name="order_receive_name" value="${memberModel.name}" /></td>
					</tr>
					<tr>
						<th scope="row">휴대폰</th>
						<td><input type="text" name="order_receive_mobile" value="${memberModel.phone}" /></td>						
					</tr> 
					<tr>
						<th scope="row">사용할 포인트(<br>최소 1000p<br> 보유포인트 ${memberModel.point }p)</th>
						<td><input type="text" name="usepoint" value="0"/>&nbsp;&nbsp;<font color="red">${me }</font></td>
						</tr>
					<tr>
					<tr>
						<th scope="row">배송요청사항</th>
						<td>
						<textarea style="width:100%;" name="order_memo"  placeholder=" 배송시요청사항 예)부재시 경비실에 맡겨주세요"></textarea></td>
					</tr>       
				</tbody>
			</table>
		</div>
</div>
</div>
</center>
<!-- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->
<div class="col-1g-9" align="center">
<button type="submit" class="btn btn-info" style="width: 800px; height: 50px; font-size: 20pt; font-weight: bold; margin-bottom: 50px;">주문하기</button>
</div>
</form></body></html>