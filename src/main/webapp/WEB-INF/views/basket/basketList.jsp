<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<header>
<style>
#basket > li{float:left;font-size:20px;margin-top:0.7%;}
#basketList > li{float:left;background-color:#f9f9f9;height:150px;line-height:150px;font-size:22px}
</style>
</header>

<body>
<div class="basket_list" align="center">
	<div class="basket_list_top">
		<h2 class="basketcart"><img src="http://okidogki.com/web/upload/goodymallSkin/title/cart.gif" alt="장바구니"></h2>
		<img style="width:1000px;" src="http://pics.auction.co.kr/myauction/buy/2012/title_basket_step.gif" alt="step 01 장바구니">
	</div>
	<br>
	<div style="width:1000px">
	 <h3 style="text-align:left;">장바구니 수량 (${fn:length(basketList) })</h3>
				<ul id="basket" style="border:1px solid #BCBBBB;">
					<li style="width:23.5%"><span>상품 이미지</span></li>
					<li style="width:43.5%"><span>상품명</span></li>		
					<li style="width:8.5%"><span>수량</span></li>
					<li style="width:18.5%"><span>상품 금액</span></li>
					<hr/>
				</ul>
			
			<c:forEach var="basketList"  items="${basketList}" varStatus="stat">
				<ul id="basketList">
					<li style="width:23.5%"><span><a href="/twenty/goodsView?goods_num=${basketList.basket_goods_num }"><img width="100px" height="125px" src="/twenty/resources/goods_images/${basketList.basket_goods_image1 }"/></a></span></li>
					<li style="width:43.5%"><span>${basketList.basket_goods_name}</span></li>		
					<li style="width:8.5%"><span>${basketList.basket_goods_amount}EA</span></li>
					<li style="width:18.5%"><span><fmt:formatNumber value="${basketList.basket_goods_price*basketList.basket_goods_amount}" type="number"/>&nbsp;원</span></li>
					<li style="width:6%;text-align:left"><span><a href="deleteBasket?basket_num=${basketList.basket_num}"><img src="/twenty/resources/images/close.png"></a></span></li>
					<hr/>
				</ul>	
				<div class="clearfix" style="border:1px solid black"> </div>
				<c:set var= "sum" value="${sum + (basketList.basket_goods_price * basketList.basket_goods_amount)}"/>
				
			</c:forEach>
			<c:if test="${fn:length(basketList) le 0}">
				<br/><center>장바구니가 비어있습니다.</center>
			</c:if>
			<br>
							<div align="right" style="background-color:#f3f3f3;border-top:1px solid black;border-bottom:1px solid black">
							상품구매금액 <strong><fmt:formatNumber value="${sum}" type="number"/> </strong> + 배송비 <strong>0</strong> = <strong style="color: #f8941d;font-size: 14px;">합계 : <fmt:formatNumber value="${sum}" type="number"/>원 </strong></div>
			<div class="basket_button" align="right">			<br>
					<a href="/twenty/main" ><img src="http://okidogki.com/web/upload/goodymallSkin/product/btn_order_ing.gif" alt="쇼핑계속하기"></a>
					<a href="/twenty/basketOrderForm" ><img src="http://okidogki.com/web/upload/goodymallSkin/product/btn_order_all.gif" alt="전체상품주문"></a>  		
			</div>		
	
</div>
</body>








