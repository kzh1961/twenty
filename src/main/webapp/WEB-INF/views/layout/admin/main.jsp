<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<h1>관리자 페이지</h1>

<div class="container-fluid">

	<hr>
	<div class="panel panel-default">
		<!-- <div class="panel-heading">
			<i class="fa fa-clock-o fa-fw"></i> 관리자 공지사항
		</div> -->

		<div class="panel-body">
			<ul class="timeline">

				<li class="timeline-inverted">

					<div class="timeline-panel">
						<div class="timeline-heading">
							<h4 class="timeline-title">Home</h4>
						</div>
						<div class="timeline-body">
							<p>쇼핑몰 홈 바로가기</p>
						</div>
					</div>
				</li>
				<li>



					<div class="timeline-panel">
						<div class="timeline-heading">
							<h4 class="timeline-title">관리자 홈</h4>
						</div>
						<div class="timeline-body">
							<p>관리자 홈 바로가기</p>
						</div>
					</div>
				</li>

				<li>

					<div class="timeline-panel">
						<div class="timeline-heading">
							<h4 class="timeline-title">상품관리</h4>
						</div>
						<div class="timeline-body">
							<p>상품등록 - 상품등록을 할 수있습니다</p>
							<p>상품목록 - 등록된 상품목록을 확인할 수 있습니다</p>
							<p>상품주문현황 - 주문된 상품목록을 확인할 수 있습니다</p>
						</div>
					</div>
				</li>

				<li>

					<div class="timeline-panel">
						<div class="timeline-heading">
							<h4 class="timeline-title">펜션관리</h4>
						</div>
						<div class="timeline-body">
							<p>펜션등록 - 펜션등록을 할 수 있습니다</p>
							<p>펜션 룸등록 - 펜션 룸등록을 할 수 있습니다</p>
							<p>펜션 목록 - 등록된 펜션 목록을 확인할 수 있습니다</p>
							<p>펜션 룸 목록 - 등록된 룸 목록을 확인할 수 있습니다</p>
							<p>펜션 예약현황 - 예약된 펜션목록을 확인할 수 있습니다</p>
						</div>
					</div>
				</li>




				<li class="timeline-inverted">
					<div class="timeline-panel">
						<div class="timeline-heading">
							<h4 class="timeline-title">회원관리</h4>
						</div>
						<div class="timeline-body">
							<p>일반회원 리스트, 판매자 리스트, 일반회원 분포도</p>
						</div>
					</div>
				</li>

				<li>
					<div class="timeline-panel">
						<div class="timeline-heading">
							<h4 class="timeline-title">공지사항</h4>
						</div>
						<div class="timeline-body">
							<p>공지사항 등록, 공지사항 리스트</p>
						</div>
					</div>
				</li>


				<li>
					<div class="timeline-panel">
						<div class="timeline-heading">
							<h4 class="timeline-title">QnA</h4>
						</div>
						<div class="timeline-body">
							<p>QnA 리스트 확인, 삭제</p>
						</div>
					</div>
				</li>

				<li>
					<div class="timeline-panel">
						<div class="timeline-heading">
							<h4 class="timeline-title">Comment</h4>
						</div>
						<div class="timeline-body">
							<p>Comment 리스트 확인, 삭제</p>
						</div>
					</div>
				</li>

			</ul>
		</div>
		<!-- /.panel-body -->
	</div>


</div>