<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<link href="/twenty/resources/web/css/bootstrap.css" rel="stylesheet"
	type="text/css" media="all" />
<!--jQuery(necessary for Bootstrap's JavaScript plugins)-->
<script src="/twenty/resources/web/js/jquery-1.11.0.min.js"></script>
<!--Custom-Theme-files-->
<!--theme-style-->
<link href="/twenty/resources/web/css/style.css" rel="stylesheet"
	type="text/css" media="all" />
</head>
<body>
	<!--banner-starts-->
	<div class="bnr" id="home">
		<div id="top" class="callbacks_container">
			<ul class="rslides" id="slider4">
				<li><img src="resources/images/bnr-1.jpg" alt="" /></li>
				<li><img src="resources/images/bnr-2.jpg" alt="" /></li>
				<li><img src="resources/images/bnr-3.jpg" alt="" /></li>
			</ul>
		</div>
		<div class="clearfix"></div>
	</div>
	<!--banner-ends-->
	<!--Slider-Starts-Here-->
	<script src="/twenty/resources/web/js/responsiveslides.min.js"></script>
	<script>
		// You can also use "$(window).load(function() {"
		$(function() {
			// Slideshow 4
			$("#slider4").responsiveSlides({
				auto : true,
				pager : true,
				nav : true,
				speed : 500,
				namespace : "callbacks",
				before : function() {
					$('.events').append("<li>before event fired.</li>");
				},
				after : function() {
					$('.events').append("<li>after event fired.</li>");
				}
			});

		});
	</script>
	<!--End-slider-script-->
	<!--about-starts-->
	<div class="about">
		<div class="container">
			<div class="about-top grid-1">
				<div class="col-md-4 about-left">
					<figure class="effect-bubba">
						<img class="img-responsive" src="resources/images/event1.jpg"
							alt="이벤트상품1" />
					</figure>
				</div>
				<div class="col-md-4 about-left">
					<figure class="effect-bubba">
						<img class="img-responsive" src="resources/images/event2.jpg"
							alt="이벤트상품2" />
					</figure>
				</div>
				<div class="col-md-4 about-left">
					<figure class="effect-bubba">
						<img class="img-responsive" src="resources/images/event3.jpg"
							alt="이벤트상품3" />
					</figure>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!--about-end-->
	<!--카테고리1 product-starts-->
	<div class="product">
		<div class="container">
			<img class="main" src="/twenty/resources/images/title1.jpg"
				alt="카테고리1"><br />&nbsp;
			<div class="product-top">
				<div class="product-one">
					<c:forEach var="goodsSelectList1" items="${goodsSelectList1}"
						varStatus="stat">
						<div class="col-md-4 product-left" style="margin-bottom: 15px;">
							<div class="product-main simpleCart_shelfItem">
								<a
									href="/twenty/goodsView?goods_num=${goodsSelectList1.goods_num}"
									class="mask"><img class="img-responsive zoom-img"
									src="/twenty/resources/goods_images/${goodsSelectList1.goods_image1}"
									alt="카테고리1"
									onerror="this.src='/twenty/resources/images/no_image.gif'" /></a>
								<div class="product-bottom">
									<h3>${goodsSelectList1.goods_subtitle}</h3>
									<h3>${goodsSelectList1.goods_name}</h3>
									<p>판매량 : ${goodsSelectList1.goods_order_sum}</p>
									<h4>
										<a class="item_add" href="#"><i></i></a> <span
											class=" item_price"><font style="color: grey;"><del>
													<fmt:formatNumber
														value="${goodsSelectList1.goods_price * 1.25}"
														type="number" />
													원
												</del></font>&nbsp;&nbsp;&nbsp; <b> <fmt:formatNumber
													value="${goodsSelectList1.goods_price}" type="number" />원
										</b></span>



									</h4>

									<p
										style="text-align: left; font-size: 11px; background: #5DB7F9; color: white; width: 54px; margin-left: 250px; padding: 2px;">&nbsp;무료배송</p>
								</div>
								<div class="srch">
									<span>-20%</span>
								</div>
							</div>
						</div>
					</c:forEach>
					<!--  등록된 상품이 없을때 -->
					<%-- <c:if test="${fn:length(goodsSelectList1) le 0}"> 등록된 상품이 없습니다</c:if>  --%>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
	<!--카테고리1 product-end-->
	<!--카테고리2 product-starts-->
	<div class="product">
		<div class="container">
			<img class="main" src="/twenty/resources/images/title2.jpg"
				alt="카테고리2"><br />&nbsp;
			<div class="product-top">
				<div class="product-one">
					<c:forEach var="goodsSelectList3" items="${goodsSelectList3}"
						varStatus="stat">
						<div class="col-md-4 product-left">
							<div class="product-main simpleCart_shelfItem">
								<a
									href="/twenty/goodsView?goods_num=${goodsSelectList3.goods_num}"
									class="mask"><img class="img-responsive zoom-img"
									src="/twenty/resources/goods_images/${goodsSelectList3.goods_image1}"
									alt="카테고리2"
									onerror="this.src='/twenty/resources/images/no_image.gif'" /></a>
								<div class="product-bottom">
									<h3>${goodsSelectList3.goods_subtitle}</h3>
									<h3>${goodsSelectList3.goods_name}</h3>
									<p>판매량 : ${goodsSelectList3.goods_order_sum}</p>

									<h4>
										<a class="item_add" href="#"><i></i></a> <span
											class=" item_price"><font style="color: grey;"><del>
													<fmt:formatNumber
														value="${goodsSelectList3.goods_price * 1.25}"
														type="number" />
													원
												</del></font>&nbsp;&nbsp;&nbsp;<b><fmt:formatNumber
													value="${goodsSelectList3.goods_price}" type="number" />원</b></span>
									</h4>
									<p
										style="text-align: left; font-size: 11px; background: #5DB7F9; color: white; width: 54px; margin-left: 250px; padding: 2px;">&nbsp;무료배송</p>
								</div>
								<div class="srch">
									<span>-20%</span>
								</div>
							</div>
						</div>
					</c:forEach>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
	<!--카테고리2 product-end-->
</body>
</html>