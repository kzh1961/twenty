<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>Twenty</title>
<link href="/twenty/resources/web/css/bootstrap.css" rel="stylesheet"
	type="text/css" media="all" />
<!--jQuery(necessary for Bootstrap's JavaScript plugins)-->
<script src="/twenty/resources/web/js/jquery-1.11.0.min.js"></script>
<!--Custom-Theme-files-->
<!--theme-style-->
<link href="/twenty/resources/web/css/style.css" rel="stylesheet"
	type="text/css" media="all" />
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="application/x-javascript">
	
	
	
	
	
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 





</script>
<!--start-menu-->
<script src="/twenty/resources/web/js/simpleCart.min.js">
	
</script>
<link href="/twenty/resources/web/css/memenu.css" rel="stylesheet"
	type="text/css" media="all" />
<script type="text/javascript" src="/twenty/resources/web/js/memenu.js"></script>
<script>
	$(document).ready(function() {
		$(".memenu").memenu();
	});
</script>

<!--dropdown-->
<script src="/twenty/resources/web/js/jquery.easydropdown.js"></script>
</head>
<body>

	<div id="status"></div>


	<!--top-header-->
	<div class="top-header">
		<div class="container">
			<div class="top-header-main">
				<div class="col-md-6 top-header-left">
					<div class="drop">
						<!--비회원 일 경우 -->
						<c:if test="${session_name == null }">
							<div class="box">
								<select tabindex="4" class="dropdown"
									onchange="location.href=this.value">
									<option class="label">고객센터</option>
									<option value="/twenty/noticeList">공지사항</option>
									<option value="/twenty/loginForm">1:1문의하기</option>
								</select>
							</div>
							<div class="box1">
								<select tabindex="4" class="dropdown"
									onchange="location.href=this.value">
									<option class="label">마이페이지</option>
									<option value="/twenty/loginForm">구매내역</option>
									<option value="/twenty/loginForm">예약내역</option>
									<option value="/twenty/loginForm">문의/답변</option>
									<option value="/twenty/loginForm">개인정보</option>
								</select>
							</div>
						</c:if>
						<!--회원 일 경우 -->
						<c:if test="${session_name != null }">
							<div class="box">
								<select tabindex="4" class="dropdown drop"
									onchange="location.href=this.value">
									<option value="" class="label">고객센터</option>
									<option value="/twenty/noticeList">공지사항</option>
									<option value="/twenty/qnaWriteForm">1:1문의하기</option>
								</select>
							</div>

							<div class="box1">
								<select tabindex="4" class="dropdown"
									onchange="location.href=this.value">
									<option value="" class="label">마이페이지</option>
									<option value="/twenty/orderList">구매내역</option>
									<option value="/twenty/res/resList">예약내역</option>
								<!-- 	<option value="./penResInfo">취소,환불,교환</option> -->
									<option value="/twenty/qnaList">문의/답변</option>
									<option value="/twenty/memberModify">개인정보</option>


								</select>
							</div>
						</c:if>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="col-md-6 top-header-right">
					<div class="cart box_1">
						<div class="total">
							<p>
								<c:if test="${session_name == null }">
									<a href="javascript:location.href='/twenty/loginForm';"
										class="simpleCart_empty">장바구니</a>
								</c:if>
								<c:if test="${session_name != null }">
									<a href="javascript:location.href='/twenty/basketList';"
										class="simpleCart_empty">장바구니</a>
								</c:if>
							</p>
						</div>
						<c:if test="${session_name == null }">
							<a href="javascript:location.href='/twenty/loginForm';"><img
								src="/twenty/resources/web/images/cart-1.png" alt="" /></a>
						</c:if>
						<c:if test="${session_name != null }">
							<a href="javascript:location.href='/twenty/basketList';"><img
								src="/twenty/resources/web/images/cart-1.png" alt="" /></a>
						</c:if>
						<p></p>
						<div class="clearfix"></div>
					</div>
					<div class="box_1" style="">
						<div class="total">
							<!--비회원 일 경우 -->
							<c:if test="${session_name == null }">
								<div style="display: inline-block;">
									<a href="/twenty/memberForm">회원가입</a>&nbsp;&nbsp;&nbsp;&nbsp;
								</div>
								<div style="display: inline-block;">
									<a href="/twenty/loginForm">로그인</a>&nbsp;&nbsp;&nbsp;&nbsp;
								</div>
							</c:if>
							<c:if test="${session_name != null }">
								<!-- 관리자 일 경우 -->
								<c:if test="${session_admincheck == '1' }">
									<a href="/twenty/adminForm">관리자페이지</a>&nbsp;&nbsp;&nbsp;&nbsp;
									<a href="/twenty/logout">로그아웃</a>&nbsp;&nbsp;&nbsp;&nbsp;
								</c:if>
								<!-- 판매자 일 경우 -->
								<c:if test="${session_admincheck == '2'}">
									<a href="/twenty/sellerForm">판매자페이지</a>&nbsp;&nbsp;&nbsp;&nbsp;
									<a href="/twenty/logout">로그아웃</a>&nbsp;&nbsp;&nbsp;&nbsp;
								</c:if>
								<!--회원 일 경우 -->
								<c:if test="${session_admincheck == '0'}">
									${session_name}&nbsp;님 &nbsp;&nbsp;&nbsp;&nbsp;
									<a href="/twenty/logout">로그아웃</a>&nbsp;&nbsp;&nbsp;&nbsp;
								</c:if>
							</c:if>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--top-header-->
	<!--start-logo-->
	<div class="logo">
		<a href="/twenty/index.jsp"><h1>Twenty</h1></a>
	</div>
	<!--start-logo-->
	<!--bottom-header-->
	<div class="header-bottom">
		<div class="container">
			<div class="header">
				<div class="col-md-8 header-left">
					<div class="top-nav">
						<ul class="memenu skyblue">
							<li class="active"><a href="index.jsp">Home</a></li>
							<li class="grid"><a href="#">패션의류/잡화</a>
								<div class="mepanel">
									<div class="row">
										<div class="col1 me-one">
											<h4>여성패션</h4>
											<ul>
												<li><a href="/twenty/goodsCategoryList?goods_category=1">여성 의류</a></li>
												<li><a href="/twenty/goodsCategoryList?goods_category=2">여성 시계&쥬얼리</a></li>
												<li><a href="/twenty/goodsCategoryList?goods_category=3">여성 가방&신발</a></li>
												<li><a href="/twenty/goodsCategoryList?goods_category=4">여성 패션 잡화</a></li>
												<li><a href="/twenty/goodsCategoryList?goods_category=5">여성 언더웨어</a></li>
											</ul>
										</div>
										<div class="col1 me-one">
											<h4>남성패션</h4>
											<ul>
												<li><a
													href="/twenty/goodsCategoryList?goods_category=6">남성 의류</a></li>
												<li><a href="/twenty/goodsCategoryList?goods_category=7">남성 시계&쥬얼리</a></li>
												<li><a href="/twenty/goodsCategoryList?goods_category=8">남성 가방&신발</a></li>
												<li><a href="/twenty/goodsCategoryList?goods_category=9">남성 패션 잡화</a></li>
												<li><a href="/twenty/goodsCategoryList?goods_category=10">남성 언더웨어</a></li>
											</ul>
										</div>
										<div class="col1 me-one">
											<h4>유아패션</h4>
											<ul>
												<li><a href="/twenty/goodsCategoryList?goods_category=11">유아 의류</a></li>
												<li><a href="/twenty/goodsCategoryList?goods_category=12">유아 신발</a></li>
												<li><a href="/twenty/goodsCategoryList?goods_category=13">유아 잡화</a></li>
											</ul>
										</div>
									</div>
								</div></li>
							<li class="grid"><a href="#">뷰티</a>
								<div class="mepanel">
									<div class="row">
										<div class="col1 me-one">
											<ul>
												<li><a href="/twenty/goodsCategoryList?goods_category=14">스킨케어</a></li>
												<li><a href="/twenty/goodsCategoryList?goods_category=15">메이크업</a></li>
												<li><a href="/twenty/goodsCategoryList?goods_category=16">헤어</a></li>
												<li><a href="/twenty/goodsCategoryList?goods_category=17">바디</a></li>
												<li><a href="/twenty/goodsCategoryList?goods_category=18">네일</a></li>
											</ul>
										</div>

									</div>
								</div></li>
							<li class="grid"><a href="#">출산/유아동</a>
								<div class="mepanel">
									<div class="row">
										<div class="col1 me-one">
											<ul>
												<li><a href="/twenty/goodsCategoryList?goods_category=19">기저귀/물티슈</a></li>
												<li><a href="/twenty/goodsCategoryList?goods_category=20">분유/유아식품</a></li>
												<li><a href="/twenty/goodsCategoryList?goods_category=21">목욕/스킨케어</a></li>
											</ul>
										</div>
									</div>
								</div></li>
							<li class="grid"><a href="#">식품</a>
								<div class="mepanel">
									<div class="row">
										<div class="col1 me-one">
											<ul>
												<li><a href="/twenty/goodsCategoryList?goods_category=22">쌀/잡곡/채소/과일</a></li>
												<li><a href="/twenty/goodsCategoryList?goods_category=23">축/수산</a></li>
												<li><a href="/twenty/goodsCategoryList?goods_category=24">김치/반찬/젓갈</a></li>
												<li><a href="/twenty/goodsCategoryList?goods_category=25">장/오일/조미료</a></li>
												<li><a href="/twenty/goodsCategoryList?goods_category=26">건강식품</a></li>
												<li><a href="/twenty/goodsCategoryList?goods_category=27">즉석식품</a></li>
												<li><a href="/twenty/goodsCategoryList?goods_category=28">스낵</a></li>
											</ul>
										</div>
									</div>
								</div></li>
							<li class="grid"><a href="#">가구/홈</a>
								<div class="mepanel">
									<div class="row">
										<div class="col1 me-one">
											<ul>
												<li><a href="/twenty/goodsCategoryList?goods_category=29">침대/매트릭스/매트</a></li>
												<li><a href="/twenty/goodsCategoryList?goods_category=30">쇼파/의자/책상</a></li>
											</ul>
										</div>
									</div>
								</div></li>
							<li class="grid"><a href="#">숙박</a>
								<div class="mepanel">
									<div class="row">
										<div class="col1 me-one">
											<h4>펜션</h4>
											<ul>
												<li><a href="/twenty/pensionList/서울">서울</a></li>
												<li><a href="/twenty/pensionList/경기">경기</a></li>
												<li><a href="/twenty/pensionList/인천">인천</a></li>
												<li><a href="/twenty/pensionList/충청">충청도</a></li>
												<li><a href="/twenty/pensionList/강원">강원도</a></li>
												<li><a href="/twenty/pensionList/전라">전라도</a></li>
												<li><a href="/twenty/pensionList/경상">경상도</a></li>
												<li><a href="/twenty/pensionList/제주">제주도</a></li>
											</ul>
										</div>
									</div>
								</div></li>
								
						</ul>
					</div>
					<div class="clearfix"></div>
				</div>
				<form action="/twenty/search" method="post">
					<div>
						<div class="col-md-2" style="display: inline-block;">
							<select class="form-control input-sm " id="sel1" name="searchNum"
								style="display: inline-block;">
								<option value="1">상품명</option>
								<option value="0">펜션지역</option>
								<option value="2">펜션명</option>
							</select>
						</div>
						<div class="col-md-2" style="display: inline-block;">
							<div class="search-bar">
								<input type="text" value="Search" name="search"
									onfocus="this.value = '';"
									onblur="if (this.value == '') {this.value = 'Search';}">
								<input type="submit" value="">
							</div>
						</div>
					</div>
				</form>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!--bottom-header-->