<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<!DOCTYPE html>
<html lang="ko">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <title>코딩몬스터</title>

<!-- <link rel="stylesheet" href="/twenty/resources/admin/css/select2.css" />
<link rel="stylesheet" href="/twenty/resources/admin/css/uniform.css" />
<link rel="stylesheet" href="/twenty/resources/admin/css/bootstrap.min.css" />
<link rel="stylesheet" href="/twenty/resources/admin/css/bootstrap.css" />
<link rel="stylesheet" href="/twenty/resources/admin/css/bootstrap-responsive.min.css" />
<link rel="stylesheet" href="/twenty/resources/admin/css/fullcalendar.css" />
<link rel="stylesheet" href="/twenty/resources/admin/css/matrix-style.css" />
<link rel="stylesheet" href="/twenty/resources/admin/css/matrix-media.css" />
<link rel="stylesheet" href="/twenty/resources/admin/css/font-awesome.css" />
<link rel="stylesheet" href="/twenty/resources/admin/css/jquery.gritter.css" />
<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' type='text/css'>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">-->
 <link rel="stylesheet" href="/twenty/resources/admin/css/select2.css" />
<link rel="stylesheet" href="/twenty/resources/admin/css/uniform.css" />
<link rel="stylesheet" href="/twenty/resources/admin/css/bootstrap.min.css" />
<link rel="stylesheet" href="/twenty/resources/admin/css/bootstrap-responsive.min.css" />
<link rel="stylesheet" href="/twenty/resources/admin/css/fullcalendar.css" />
<link rel="stylesheet" href="/twenty/resources/admin/css/matrix-style.css" />
<link rel="stylesheet" href="/twenty/resources/admin/css/matrix-media.css" />
<link rel="stylesheet" href="/twenty/resources/admin/css/font-awesome.css" />
<link rel="stylesheet" href="/twenty/resources/admin/css/jquery.gritter.css" />
<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' type='text/css'>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
 


<!-- <script src="/twenty/resources/admin/js/jquery.min.js"></script>
<script src="/twenty/resources/admin/js/matrix.js"></script>
<script src="/twenty/resources/admin/js/bootstrap.min.js"></script>
<script src="/twenty/resources/admin/js/jquery.uniform.js"></script>
<script src="/twenty/resources/admin/js/select2.min.js"></script>
<script src="/twenty/resources/admin/js/jquery.dataTables.min.js"></script>
<script src="/twenty/resources/admin/js/matrix.tables.js"></script>
<script src="/twenty/resources/admin/js/jquery.ui.custom.js"></script>
<script src="/twenty/resources/admin/js/dataTables.bootstrap.min.js"></script> -->
<script src="/twenty/resources/admin/js/jquery.min.js"></script>
<script src="/twenty/resources/admin/js/matrix.js"></script>
<script src="/twenty/resources/admin/js/bootstrap.min.js"></script>
<script src="/twenty/resources/admin/js/jquery.uniform.js"></script>
<script src="/twenty/resources/admin/js/select2.min.js"></script>
<script src="/twenty/resources/admin/js/jquery.dataTables.min.js"></script>
<script src="/twenty/resources/admin/js/matrix.tables.js"></script>
<script src="/twenty/resources/admin/js/jquery.ui.custom.js"></script>

</head>

<body>

<!--Header-part-->
<div id="header">
  <h2><a href="">Twenty</a></h2>
</div>
<!--close-Header-part--> 


<!--top-Header-menu-->
<div id="user-nav" class="navbar navbar-inverse">
  <ul class="nav">
    <li class="dropdown" id="profile-messages"><a title="" href="#" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle"><i class="icon icon-user"></i>  <span class="text">Welcome User</span><b class="caret"></b></a>
      <ul class="dropdown-menu">
        <li><a href="#"><i class="icon-user"></i> 준비중</a></li>
        <li class="divider"></li>
        <li><a href="#"><i class="icon-check"></i> 준비중</a></li>
        <li class="divider"></li>
        <li><a href="login.html"><i class="icon-key"></i> 준비중</a></li>
      </ul>
    </li>
    <li class=""><a title="" href="#"><i class="icon icon-cog"></i> <span class="text"> 준비중</span></a></li>
    <li class=""><a title="" href="/twenty/logout"><i class="icon icon-share-alt"></i> <span class="text">Logout</span></a></li>
  </ul>
</div>
<!--close-top-Header-menu-->
<!--start-top-serch-->
<div id="search">
  <input type="text" placeholder="준비중...">
  <button type="submit" class="tip-bottom" data-original-title="Search"><i class="icon-search icon-white"></i></button>
</div>
<!--close-top-serch-->
<!--sidebar-menu-->
<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i>Dashboard</a>
  <ul style="display: block;">
<li> <a href="/twenty/sellerForm"><i class="icon icon-inbox"></i> <span>판매자 홈</span></a> </li>      <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>상품관리</span> <span class="label label-important">2</span></a>
      <ul>
        <li><a href="/twenty/sellerGoodsWriteForm">상품 등록</a></li>
        <li><a href="/twenty/sellerGoodsList">상품 목록</a></li>
       <!--  <li><a href="/twenty/adminOrderList">상품 주문현황</a></li> -->
      </ul>
    </li>
         <!--  <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>펜션관리</span> <span class="label label-important">5</span></a>
      <ul>
        <li><a href="/twenty/adminPensionWriteForm">펜션 등록</a></li>
        <li><a href="/twenty/adminRoomWriteForm">펜션 룸등록</a></li>
        <li><a href="/twenty/adminPensionList">펜션 목록</a></li>
        <li><a href="/twenty/adminRoomList">펜션 룸 목록</a></li>
        <li><a href="/twenty/adminResList">펜션 예약현황 </a></li>
      </ul>
    </li>
     <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>회원 관리</span> <span class="label label-important">2</span></a>
      <ul>
        <li><a href="/twenty/adminMemberList">회원 리스트</a></li>
        <li><a href="/twenty/adminMemberMap">회원 분포도</a></li>
      </ul> -->
    <!-- <li> <a href="/twenty/admin/adminnoticeList"><i class="icon icon-signal"></i> <span>Charts &amp; graphs</span></a> </li> -->
    <!-- <li> <a href="/twenty/adminNoticeList"><i class="icon icon-inbox"></i> <span>공지사항</span></a> </li>
    <li><a href="/twenty/adminQnAList"><i class="icon icon-th"></i> <span>QnA</span></a></li>
    <li><a href="/twenty/adminReviewList"><i class="icon icon-fullscreen"></i> <span>Review</span></a></li>

    <li><a href="준비중"><i class="icon icon-tint"></i> <span>상품 판매량</span></a></li> -->
<!--     <li><a href="준비중"><i class="icon icon-pencil"></i> <span>상품 인기순위</span></a></li> -->
    <!-- <li class="submenu"> <a href="#"><i class="icon icon-file"></i> <span>회원 세대별 선호상품</span> <span class="label label-important">5</span></a>
      <ul>
        <li><a href="준비중">Dashboard2</a></li>
        <li><a href="준비중">Gallery</a></li>
        <li><a href="준비중">Calendar</a></li>
        <li><a href="준비중">Invoice</a></li>
        <li><a href="준비중">Chat option</a></li>
      </ul>
    </li> -->
    <!-- <li class="submenu"> <a href="#"><i class="icon icon-info-sign"></i> <span>Error</span> <span class="label label-important">4</span></a>
      <ul>
        <li><a href="준비중">Error 403</a></li>
        <li><a href="준비중">Error 404</a></li>
        <li><a href="준비중">Error 405</a></li>
        <li><a href="준비중">Error 500</a></li>
      </ul>
    </li>
    <li class="content"> <span>Monthly Bandwidth Transfer</span>

      <div class="progress progress-mini progress-danger active progress-striped">

        <div style="width: 77%;" class="bar"></div>

      </div>

      <span class="percent">77%</span>

      <div class="stat">21419.94 / 14000 MB</div>

    </li>
    <li class="content"> <span>Disk Space Usage</span>

      <div class="progress progress-mini active progress-striped">

        <div style="width: 87%;" class="bar"></div>

      </div>

      <span class="percent">87%</span>

      <div class="stat">604.44 / 4000 MB</div>

    </li> -->
  </ul>
</div>
<!--sidebar-menu-->

<!--main-container-part-->
<div id="content">

<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="http://localhost:8080/twenty/main" class="tip-bottom" data-original-title="메인화면으로 이동합니다"><i class="icon-home"></i> Home</a></div>
                <!-- 메인container-->
           	 <tiles:insertAttribute name="body"/>
            <!-- // container -->
  </div>
<!--End-breadcrumbs-->

<!--Action boxes-->
  
</div>

<!--end-main-container-part-->

<!--Footer-part-->

<div class="row-fluid">
  <div id="footer" class="span12"> 2013 © Matrix Admin. Brought to you by <a href="http://themedesigner.in">Themedesigner.in</a> </div>
</div>


<ul class="typeahead dropdown-menu"></ul>
</body>


