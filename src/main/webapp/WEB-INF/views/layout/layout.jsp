<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<!DOCTYPE html>
<html>
<head>
<title>Twenty</title>
<link href="/twenty/resources/web/css/bootstrap.css" rel="stylesheet"
	type="text/css" media="all" />
<!--jQuery(necessary for Bootstrap's JavaScript plugins)-->
<script src="/twenty/resources/web/js/jquery-1.11.0.min.js"></script>
<!--Custom-Theme-files-->
<!--theme-style-->
<link href="/twenty/resources/web/css/style.css" rel="stylesheet"
	type="text/css" media="all" />
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript">
	
	
	
	
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 




</script>
<!--start-menu-->
<script src="/twenty/resources/web/js/simpleCart.min.js">
	
</script>
<link href="/twenty/resources/web/css/memenu.css" rel="stylesheet"
	type="text/css" media="all" />
<script type="text/javascript" src="/twenty/resources/web/js/memenu.js"></script>
<script>
	$(document).ready(function() {
		$(".memenu").memenu();
	});
</script>
<!--dropdown-->
<script src="/twenty/resources/web/js/jquery.easydropdown.js"></script>
<style type="text/css">
#footer { 
	bottom: 0;
	width: 100%;
	height: 50px;
}
</style>
</head>
<body>
	<div id="header">
		<tiles:insertAttribute name="header" />
		<!-- // header -->
	</div>
	<div id="container" style="min-height: 500px;">
		<tiles:insertAttribute name="body" />
		<!-- // container -->
	</div>
	<div id="footer">
		<tiles:insertAttribute name="footer" />
	</div>
</body>
</html>