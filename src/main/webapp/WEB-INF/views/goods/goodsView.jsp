<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<head>
<!-- <meta http-equiv="X-UA-compatible" content="IE=edge"> -->
<title>Twenty</title>

<link rel="stylesheet" href="/twenty/resources/goods/css/common_new.css" />
<link rel="stylesheet" href="/twenty/resources/goods/css/deal.css" />

<script type="text/javascript">
	/* 숫자 , 표시 함수 */
	var funcSetCurrency = function($) {
		$('span,p.custom,.custom').each(
				function() {
					if (!isNaN(Number($(this).text())) && $(this).text())
						$(this).text(
								Number($(this).text()).toLocaleString().split(
										'.')[0]);
				});

	};

	$(document).ready(function() {
		funcSetCurrency($);
	});

	//구매갯수 변환 함수
	function count_change(temp) {
		var test = document.goodsform.amount.value;
		var price = "${goodsModel.goods_price}";//숫자계산위해필요
		if (temp == 0) {
			test++;
		} else if (temp == 1) {
			if (test > 1)
				test--;
		}
		if (test > "${goodsModel.goods_amount}") {
			alert("잔여수량만큼 구매하세요");
			test = 1;
		}

		document.goodsform.amount.value = test;
		var value2 = $("#span1").html(price * test); //숫자계산위해필요
		var value3 = $("em").html(price * test);//숫자계산위해필요
		var value4 = $("#am2").html("(" + test + "개)");//숫자계산위해필요
	}

	//span값변경
	$(window).load(
			function() {

				var amount = document.goodsform.amount.value;
				var price = "${goodsModel.goods_price}";
				var value2 = $("#span1").html(price * amount);
				var value3 = $("em").html(price * amount);
				var value4 = $("#am2").html("(" + amount + "개)");

				$('em, #span1').each(
						function() {
							if (!isNaN(Number($(this).text()))
									&& $(this).text())
								$(this).text(
										Number($(this).text()).toLocaleString()
												.split('.')[0]);
						});

			});

	//장바구니 처리
	var onBasket = function() {

		var num = "${goodsModel.goods_num}";
		var amount = document.goodsform.amount.value;
		var id = "${session_id}";
		location.href = '/twenty/basketAdd?goods_num=' + num + '&goods_amount='
				+ amount + '&basket_id=' + id;

	};

	//주문처리
	var onOrder = function() {

		var num = "${goodsModel.goods_num}";
		var amount = document.goodsform.amount.value;
		var id = "${session_id}";
		location.href = '/twenty/orderForm?goods_num=' + num + '&goods_amount='
				+ amount + '&basket_member_id=' + id;

	};

	//코멘트 처리
	var onComment = function() {
		if (document.f.comments.value == '')
			alert('내용을 입력해주세요');
		else {
			var form = $('.commentForm')[0];
			form.action = 'goodsCommentWrite';
			form.submit();
		}
	};
</script>
<style type="text/css">
.btn1 {
	vertical-align: middle;
	text-align: center;
	overflow: visible;
}

.btn1 {
	width: 190px;
	height: 30px;
}

.btn-primary1 {
	color: #fff;
	background-color: #2a2e33;
	border-color: #2a2e33;
}

button {
	cursor: pointer;
}

.reply_grp {
	margin-top: 50px;
	margin-bottom: 77px;
}

.reply_grp .reply_view .reply_tit .btn {
	position: absolute;
	top: 50%;
	right: 0;
	margin-top: 14px;
	width: 30px;
}

p {
	display: inline-block;
}

#regdate {
	float: right;
}

.btn1:hover {
	background: #73b6e1;
	transition: 0.5s all;
	-webkit-transition: 0.5s all;
	-o-transition: 0.5s all;
	-moz-transition: 0.5s all;
	-ms-transition: 0.5s all;
}
</style>
</head>
<body>
	<div id="container" style="margin: 0 250px 90px 250px">
		<div id="content">
			<div class="deal_detail_wrap deal_detail_type2">
				<div class="img_area">
					<div class="main_img" id="main_img">
						<div class="condition" style="z-index: 100"></div>
						<ul class="roll">
							<div class="slides_control">
								<li><img
									src="/twenty/resources/goods_images/${goodsModel.goods_image1}"
									onerror="this.src='/twenty/resources/images/no_image'"
									id="front_image_area" class="main_img_slide"
									style="width: 476px; height: 502px; visibility: visible;"
									alt=""></li>
							</div>
						</ul>
					</div>
				</div>
				<form name="goodsform" action="#" method="post">
					<div class="ct_area">
						<div class="deal_info"></div>
						<h5>${goodsModel.goods_subtitle}</h5>
						<h3>${goodsModel.goods_name}</h3>
						<div id="promotion_block">
							<div id="price_info" class="price_info">


								<div class="price">
									<p class="custom">
										<b><font
											style="color: grey; font-size: large; text-decoration: line-through;"><fmt:formatNumber
													value="${goodsModel.goods_price * 1.25}" type="number"
													pattern="00,000" />원 </font>&nbsp;<font
											style="font-size: xx-large;"><fmt:formatNumber
													value="${goodsModel.goods_price}" type="number"
													pattern="0,000" />원</font></b>
									</p>
								</div>
							</div>

						</div>
						<!-- promotion_block -->
						<div></div>
						<div class="people_time">

							<span class="stock">현재<strong class="num buycount"
								style="display: inline;">${goodsModel.goods_order_sum}</strong>
								<span class="txt2">개 구매</span>
							</span>
						</div>
						<div id="totalProducts" class="">
							<p class="info ">
								<img
									src="http://img.echosting.cafe24.com/skin/base_ko_KR/product/ico_information.gif"
									alt="" /> 수량을 선택해주세요.
							</p>
							<table summary="">
								<caption>상품 목록</caption>
								<colgroup>
									<col style="width: 284px;" />
									<col style="width: 80px;" />
									<col style="width: 110px;" />
								</colgroup>
								<tbody class="">
									<tr>

										<td><p class="quantity">
												<input type="text" name="amount" class="quantity_opt"
													value="1" style="text-align: center; ime-mode: Disabled;"
													onkeypress="checknum()" readonly /> <a
													href="JavaScript:count_change(0)"><img
													src="http://img.echosting.cafe24.com/design/skin/default/product/btn_count_up.gif"
													alt="수량증가" class="up" /></a> <a
													href="JavaScript:count_change(1)"><img
													src="http://img.echosting.cafe24.com/design/skin/default/product/btn_count_down.gif"
													alt="수량감소" class="down" /></a>
											</p></td>


									</tr>
								</tbody>
								<tfoot>
									<tr>
										<td colspan="3"><strong>총 상품금액</strong>(수량) : <span><strong><em>0</em>원&nbsp;</strong><span
												id="am2">(${goodsModel.goods_amount}개)</span></span></td>
									</tr>
								</tfoot>
							</table>
						</div>

						<div class="option_info2" style="display: none">
							<ul class="uio_option_area uio_scroll" style="display: block;">
							</ul>
						</div>
						<div class="order_price1 sell" style="display: none;">
							<div class="total">
								<em>총 상품 금액 : </em> <strong><span class="total_amount">0</span><span
									class="won">원</span></strong>
							</div>
						</div>

						<!-- 잔여수량이 0 이면 솔드아웃 처리 -->
						<c:if test="${goodsModel.goods_amount > 0 }">
							<div align="center">
								<a href="JavaScript:onOrder()" class="add-cart2 item_add"
									style="padding: 20px 60px;">바로구매</a>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="hidden"><a
									href="JavaScript:onBasket()" class="add-cart item_add">장바구니
									담기</a>
							</div>
						</c:if>

						<c:if test="${goodsModel.goods_amount <= 0 }">
							<img src="http://ppoya.co.kr/images/btn_soldout.gif" alt="품절">
						</c:if>

					</div>
				</form>
			</div>

			<div class="bigevent_banner_lst">
				<div id="banner_container"
					style="overflow: hidden; position: relative; display: block;">
					<div class="slides_control"
						style="position: relative; width: 2970px; height: 148px; left: -990px;">
						<ul class="banner_lst"
							style="position: absolute; top: 0px; left: 990px; z-index: 5; display: block;">
							<li><img src="/twenty/resources/images/banner.png"
								width="990" height="148" alt="[11월]따뜻한 세일"> <span
								class="edge"></span></li>
						</ul>
					</div>
				</div>
			</div>

			<div id="deal_display" class="wrap_deal_display">
				<div class="tabs">
					<ul class="menu_drop">
						<li class="item3"><a href=""><img
								src="./resources/web/images/arrow.png" alt="">상품설명</a></li>
					</ul>
				</div>
				<div class="deal_content">
					<!-- 상품설명 시작 -->
					<div id="md_area" class="tmon_review">
						<div class="buybefore_info_area">
							<h3 class="title">구매전 꼭 읽어주세요!</h3>
							<!-- 배송 배너 타입 -->
							<div class="ct_deal_condition">
								<!-- [D] 텝메뉴 노출시 min_h1 클래스명 추가 적용, 해외구매대행 배너와 텝메뉴 노출시 min_h2 클래스명 추가 적용 -->
								<div class="deal_condition_box refund" style="display: block;">
									<div class="thmb">환불적용</div>
									<h4 class="tit">미사용 티켓 환불제 적용 상품입니다.</h4>
									<p class="txt_type1">
										기간 내 사용하지 못한 티켓은 유효기간 종료 7일 후, <br> <strong>구매금액의
											100%를 Twenty 적립금(유효기간 180일)</strong>으로 돌려드립니다.
									</p>
									<div class="btn_view"></div>
								</div>
								<!-- //지역 배달딜의 경우 안내노출-->
							</div>
							<div id="useInfoArea" class="deal_info_area">

								<div id="useInfo1" class="info_box1">
									<h4 id="useinfo_title1" class="tit">기본 정보</h4>
									<ul>
										<li><b>유효기간 : 구매 후 30일</b></li>
										<li>구매 가능 수량 : 1인 100매까지 구매 가능</li>
										<li>결제제한 :&nbsp;휴대폰,계좌이체,무통장입금</li>
									</ul>
								</div>

								<div id="useInfo2" class="info_box2">
									<h4 id="useinfo_title2" class="tit">업체 이용 정보</h4>
									<ul>
										<li>업체주소 및 번호 : 매장별 상이 (홈페이지 링크 참조)</li>
										<li>수용인원 및 주차 : 매장별 상이 (각 매장 별도 문의)</li>
										<li>운영시간 및 휴무 : 매장별 상이 (각 매장 별도 문의)</li>
										<li>단, 일부 휴게소,리조트 등 특수 매장 제외</li>
										<li>&nbsp;</li>
										<li><b>[</b><b>주문취소 안내]</b></li>
										<li>주문한 매장으로 전화해서 주문 취소 요청</li>
										<li>&nbsp;주문취소 후 30분~2시간 뒤 쿠폰 복구 됨</li>
										<li>- Twenty 고객센터: 080-111-2222</li>
									</ul>
								</div>

								<div id="useInfo3" class="info_box3">
									<h4 id="useinfo_title4" class="tit">환불 안내</h4>
									<ul>
										<li>30일 환불 가능 상품 (구매 시점으로부터 30일 이내)</li>
										<li>미사용 티켓 유효기간 종료 후 적립금 100% 환불</li>
									</ul>
								</div>
							</div>
							<!-- 상품설명 끝 -->
							<!-- 상품이미지 시작 -->
							<div id="md_area" style="width: 100%; text-align: center;">
								<img class="lazy"
									src="/twenty/resources/goods_images/${goodsModel.goods_image2}"
									data-original="http://img1.tmon.kr/deals/2016/10/27/426478990/summary_5bdd4.jpg"
									style="display: inline;">

								<div style="padding-top: 70px; padding-bottom: 30px;">
									<img
										src="/twenty/resources/goods_images/${goodsModel.goods_image3}"
										class="lazy"
										data-original="http://img1.tmon.kr//local/banner/instagram_hashtag_event_20160906.jpg"
										style="display: inline;">
								</div>
							</div>
							<!-- 상품이미지 끝 -->
						</div>

					</div>
				</div>
				<!-- 상품평 시작 -->
				<div class="tabs" style="margin-top: 4em;">
					<ul class="menu_drop">
						<li class="item3"><a href="#demo" data-toggle="collapse"
							data-target="#demo"><img
								src="./resources/web/images/arrow.png" alt="">Reviews</a></li>
					</ul>
				</div>
				<!-- @@@@ 상품평 입력 @@@@ -->
				<div id="demo" class="collapse in" style="padding-top: 15px;">
					<form name="f" class="commentForm" method="post">
						<input type="hidden" name="goods_num"
							value="${goodsModel.goods_num}" />
						<!-- 비회원 일 때 -->
						<c:if test="${session_id == null}">
							<div class="col-sm-9 form-group">
								<select class="form-control" name="goods_point">
									<option value="5">★★★★★&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;아주만족</option>
									<option value="4">★★★★&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;만족</option>
									<option value="3">★★★&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;보통</option>
									<option value="2">★★&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;미흡</option>
									<option value="1">★&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;불만족
									</option>
								</select>
							</div>
							<button type="button" class="btn1 btn-primary1"
								onclick="javascript:location.href='loginForm'">상품평 등록</button>
							<div class="form-group">
								<textarea class="form-control" rows="5"
									placeholder="로그인 후 상품평 등록이 가능합니다."></textarea>
							</div>
						</c:if>
						<!-- 회원 일 때 -->
						<c:if test="${session_id != null}">
							<div class="col-sm-9 form-group">
								<select class="form-control" name="goods_point">
									<option value="5">★★★★★&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;아주만족</option>
									<option value="4">★★★★&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;만족</option>
									<option value="3">★★★&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;보통</option>
									<option value="2">★★&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;미흡</option>
									<option value="1">★&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;불만족
									</option>
								</select>
							</div>
							<button type="button" class="btn1 btn-primary1"
								onclick="onComment()">상품평 등록</button>
							<div class="form-group">
								<textarea class="form-control" rows="5" name="comments"
									placeholder="상품평을 입력해주세요." required="required"></textarea>
							</div>
						</c:if>
					</form>
				</div>
				<!-- @@@@@ 구매만족도 @@@@@@ -->
				<div style="margin-top: 20px;">
					<label><font size="4">구매만족도 </font></label>
					<div class="panel panel-default" style="margin-top: 10px;">
						<div class="panel-heading">
							<div class="row">
								<div class="col-xs-12 col-md-5 text-center">
									<p style="padding-left: 30%; padding-top: 15%;">
										<font size="10" style="font-weight: bold;"><fmt:formatNumber
												value="${goodsModel.goods_avg_point}" type="pattern"
												pattern="0.0" /></font><font color="#727272" size="5"> / 5</font><br>
										${goodsCommentList.size()}명 평가
									</p>
								</div>
								<div class="col-xs-12 col-md-7 text-center">
									<div class="col-xs-12 col-md-3 text-right">★5</div>
									<div class="progress">
										<div
											class="progress-bar progress-bar-success progress-bar-striped active"
											role="progressbar" aria-valuenow="40" aria-valuemin="0"
											aria-valuemax="100"
											style="width: <fmt:formatNumber
												value="${countNum5/goodsCommentList.size()*100}" type="pattern"
												pattern="0" />%">
											<fmt:formatNumber
												value="${countNum5/goodsCommentList.size()*100}"
												type="pattern" pattern="0" />
											%
										</div>
									</div>
									<div class="col-xs-12 col-md-3 text-right">★4</div>
									<div class="progress">
										<div
											class="progress-bar progress-bar-success progress-bar-striped active"
											role="progressbar" aria-valuenow="40" aria-valuemin="0"
											aria-valuemax="100"
											style="width: <fmt:formatNumber
												value="${countNum4/goodsCommentList.size()*100}" type="pattern"
												pattern="0" />%">
											<fmt:formatNumber
												value="${countNum4/goodsCommentList.size()*100}"
												type="pattern" pattern="0" />
											%
										</div>
									</div>
									<div class="col-xs-12 col-md-3 text-right">★3</div>
									<div class="progress">
										<div
											class="progress-bar progress-bar-info progress-bar-striped active"
											role="progressbar" aria-valuenow="50" aria-valuemin="0"
											aria-valuemax="100"
											style="width: <fmt:formatNumber
												value="${countNum3/goodsCommentList.size()*100}" type="pattern"
												pattern="0" />%">
											<fmt:formatNumber
												value="${countNum3/goodsCommentList.size()*100}"
												type="pattern" pattern="0" />
											%
										</div>
									</div>
									<div class="col-xs-12 col-md-3 text-right">★2</div>
									<div class="progress">
										<div
											class="progress-bar progress-bar-warning progress-bar-striped active"
											role="progressbar" aria-valuenow="60" aria-valuemin="0"
											aria-valuemax="100"
											style="width:<fmt:formatNumber
												value="${countNum2/goodsCommentList.size()*100}" type="pattern"
												pattern="0" />%">
											<fmt:formatNumber
												value="${countNum2/goodsCommentList.size()*100}"
												type="pattern" pattern="0" />
											%
										</div>
									</div>
									<div class="col-xs-12 col-md-3 text-right">★1</div>
									<div class="progress">
										<div
											class="progress-bar progress-bar-danger progress-bar-striped active"
											role="progressbar" aria-valuenow="70" aria-valuemin="0"
											aria-valuemax="100"
											style="width: <fmt:formatNumber
												value="${countNum1/goodsCommentList.size()*100}" type="pattern"
												pattern="0" />%">
											<fmt:formatNumber
												value="${countNum1/goodsCommentList.size()*100}"
												type="pattern" pattern="0" />
											%
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- @@@@ 상품평 리스트 @@@@ -->
				<div style="margin-top: 20px;">
					<label><font size="4" style="font-weight: 600;">전체
							리뷰 </font><font color="#727272" size="3">${goodsCommentList.size()}건</font></label>
				</div>
				<div style="margin: 0 10 0 30px;">
					<c:if test="${fn:length(goodsCommentList) le 0}">
					등록된 상품평이 없습니다
				</c:if>
					<c:forEach var="goodsCommentList" items="${goodsCommentList}"
						varStatus="stat">
						<div class="reply_view">
							<div class="panel panel-default">
								<div class="panel-body">
									<!-- 상품평 별점 -->
									<div class="so">
										<p>
											<c:if test='${goodsCommentList.goods_point == 1}'>
      &nbsp;<img src="/twenty/resources/images/goods/star_on2.gif"
													border="0" />
												<img src="/twenty/resources/images/goods/star_off2.gif"
													border="0" />
												<img src="/twenty/resources/images/goods/star_off2.gif"
													border="0" />
												<img src="/twenty/resources/images/goods/star_off2.gif"
													border="0" />
												<img src="/twenty/resources/images/goods/star_off2.gif"
													border="0" />&nbsp;<strong>불만족</strong>
											</c:if>
											<c:if test='${goodsCommentList.goods_point == 2}'>
     &nbsp; <img src="/twenty/resources/images/goods/star_on2.gif"
													border="0">
												<img src="/twenty/resources/images/goods/star_on2.gif"
													border="0">
												<img src="/twenty/resources/images/goods/star_off2.gif"
													border="0">
												<img src="/twenty/resources/images/goods/star_off2.gif"
													border="0">
												<img src="/twenty/resources/images/goods/star_off2.gif"
													border="0">&nbsp;<strong>미흡</strong>
											</c:if>
											<c:if test='${goodsCommentList.goods_point == 3}'>
     &nbsp; <img src="/twenty/resources/images/goods/star_on2.gif"
													border="0">
												<img src="/twenty/resources/images/goods/star_on2.gif"
													border="0">
												<img src="/twenty/resources/images/goods/star_on2.gif"
													border="0">
												<img src="/twenty/resources/images/goods/star_off2.gif"
													border="0">
												<img src="/twenty/resources/images/goods/star_off2.gif"
													border="0">&nbsp;<strong>보통</strong>
											</c:if>
											<c:if test='${goodsCommentList.goods_point == 4}'>
     &nbsp; <img src="/twenty/resources/images/goods/star_on2.gif"
													border="0">
												<img src="/twenty/resources/images/goods/star_on2.gif"
													border="0">
												<img src="/twenty/resources/images/goods/star_on2.gif"
													border="0">
												<img src="/twenty/resources/images/goods/star_on2.gif"
													border="0">
												<img src="/twenty/resources/images/goods/star_off2.gif"
													border="0">&nbsp;<strong>만족</strong>
											</c:if>
											<c:if test='${goodsCommentList.goods_point == 5}'>
       &nbsp; <img src="/twenty/resources/images/goods/star_on2.gif"
													border="0">
												<img src="/twenty/resources/images/goods/star_on2.gif"
													border="0">
												<img src="/twenty/resources/images/goods/star_on2.gif"
													border="0">
												<img src="/twenty/resources/images/goods/star_on2.gif"
													border="0">
												<img src="/twenty/resources/images/goods/star_on2.gif"
													border="0">&nbsp;<strong>아주만족</strong>
											</c:if>
										</p>
										<!-- 상품평 작성자/등록일 -->
										<p id="regdate">
											작성자 : ${goodsCommentList.id}님 | 등록일 :
											<fmt:formatDate value="${goodsCommentList.comment_date}"
												pattern="yy.MM.dd"></fmt:formatDate>
										</p>
									</div>
								</div>
								<!-- 상품평 내용 + 해당 아이디일 때 삭제 표시 -->
								<div style="padding: 20px;">
									<p>${goodsCommentList.comments}</p>
								</div>
								<div align="right">
									<c:if test="${session_id == goodsCommentList.id}">
										<a
											href="goodsCommentDelete?comment_num=${goodsCommentList.comment_num}&goods_num=${goodsModel.goods_num}"
											class="btn btnC_01 btnP_02"> <span
											class="btn btnC_05 reply_btn">삭제</span>
										</a>
									</c:if>
								</div>
							</div>
						</div>
					</c:forEach>
				</div>
				<!-- 상품평 끝 -->
			</div>
			<div class="aside xx_heatmap_region"
				style="display: inline; float: right;">
				<div>
					<!-- 뜨는 상품 -->
					<div class="lstarea popular">
						<h5 style="color: #058aff; font-weight: 600; margin-left: 10px;">
							<span>추천 상품</span>
						</h5>
						<ul class="aside_lst" id="aSideArea">
							<c:forEach var="goodsViewBest" items="${goodsViewBest}"
								varStatus="stat">
								<li><a
									href="goodsView?goods_num=${goodsViewBest.goods_num}"> <img
										src="/twenty/resources/goods_images/${goodsViewBest.goods_image1}"
										onerror="this.src='/pet/resources/images/noimg_130.gif'"
										data-original="http://img2.tmon.kr/deals/2016/10/28/428990446/428990446_catlist_3col_v2_d597d_1477646768production_q100_142x145_1.jpg"
										width="142" height="145"> <span class="dsc">${goodsViewBest.goods_name}</span>
										<span class="cnt"><strong>${goodsViewBest.goods_order_sum}</strong>개
											구매</span> <em class="mask"></em>
								</a></li>
							</c:forEach>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>