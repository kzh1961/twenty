<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="product">
		<div class="container">
			<div class="product-top">
				<div class="product-one">
					<c:forEach var="goodsList" items="${goodsList}" varStatus="stat">
						<div class="col-md-4 product-left" style="margin-bottom: 20px;">
							<a href="goodsView?goods_num=${goodsList.goods_num}">
								<div class="product-main simpleCart_shelfItem">
									<img class="img-responsive zoom-img"
										src="/twenty/resources/goods_images/${goodsList.goods_image1}"
										width="300" height="300" alt=""
										onerror="this.src='/pet/resources/images/noimg_130.gif'" />
										
									<%-- <dl class="price">
										<dt class="name">
											<span
												style="font-size: 16px; color: #1a1a1a; font-weight: bold;">${goodsList.goods_name}</span>
										</dt>
									</dl>

									<p>상품부제목</p>
									<h4>
										<a class="item_add" href="#"><i></i></a> <span
											class=" item_price"><font style="color: grey;"><del>
													<fmt:formatNumber value="${goodsList.goods_price * 1.25}"
														type="number" />
													원
												</del></font>&nbsp;&nbsp;&nbsp;<b><fmt:formatNumber
													value="${goodsList.goods_price}" type="number" />원</b></span>

									</h4>
									<p
										style="text-align: left; font-size: 11px; background: #5DB7F9; color: white; width: 54px; margin-left: 250px; padding: 2px;">&nbsp;무료배송</p>
 --%>
 <div class="product-bottom">
									<h3>${goodsList.goods_subtitle}</h3>
									<h3>${goodsList.goods_name}</h3>
									<p>판매량 : ${goodsList.goods_order_sum}</p>
									<h4>
										<a class="item_add" href="#"><i></i></a> <span
											class=" item_price"><font style="color: grey;"><del>
													<fmt:formatNumber
														value="${goodsList.goods_price * 1.25}"
														type="number" />
													원
												</del></font>&nbsp;&nbsp;&nbsp; <b> <fmt:formatNumber
													value="${goodsList.goods_price}" type="number" />원
										</b></span>



									</h4>

									<p
										style="text-align: left; font-size: 11px; background: #5DB7F9; color: white; width: 54px; margin-left: 250px; padding: 2px;">&nbsp;무료배송</p>
								</div>
									<div class="srch">
										<span>-20%</span>
									</div>
								</div>
							</a>
						</div>
					</c:forEach>
				</div>
				<!--  등록된 상품이 없을때 -->
				<c:if test="${fn:length(goodsList) le 0}">
	등록된 상품이 없습니다
</c:if>
			</div>
		</div>
	</div>
</body>
</html>