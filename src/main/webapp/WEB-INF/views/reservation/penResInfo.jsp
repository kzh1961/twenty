<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">

<title>항공 주문 페이지</title>
<script language="JavaScript">
	function checkIt() {
		var orderForm = eval("document.orderForm");
		document.orderForm.action = "/twenty/res/reservation";
		document.orderForm.submit();
	}
</script>
</head>
<body>
	<div style="width: 800px; margin: auto;">
		<form name="orderForm" method="post">
			<input type="hidden" name="id" value="${session_id }"/>
			<input type="hidden" name="people" value="${people }"/>
			<input type="hidden" name="pen_name" value="${room.pen_name }"/>
			<input type="hidden" name="price" value="${price }"/>
			<input type="hidden" name="st_day" value="${st_day }"/>
			<input type="hidden" name="end_day" value="${end_day }"/>
			<input type="hidden" name="room_num" value="${room.room_num }"/>
			<table width=800px border="0" cellpadding="1" cellspacing="1">
				<tr height=23px>
					<td width="800px" align="center" bgcolor="#73B6E1" colspan="2"><font
						color="white" size="4">펜션 정보</font></td>
				</tr>
				<tr height=23px>
					<td width="100px" align="right" bgcolor="#73B6E1"><font
						color="white" size="2">펜션이름</font></td>
					<td width="700px" bgcolor="#FFFFFF" align="left">&nbsp;&nbsp;${room.pen_name }</td>
				</tr>

				<tr>
					<td colspan=6></td>
				</tr>
				<tr height=23px>
					<td width="100px" align="right" bgcolor="#73B6E1"><font
						color="white" size="2">방번호</font></td>
					<td width="700px" bgcolor="#FFFFFF" align="left">&nbsp;&nbsp;${room.room_num }</td>

				</tr>

				<tr>
					<td colspan=6></td>
				</tr>
				<tr height=23px>
					<td width="800px" align="center" bgcolor="#73B6E1" colspan="2"><font
						color="white" size="4">상품 정보</font></td>
				</tr>

				<tr>
					<td colspan=6>
				</tr>
				<tr height=23px>
					<td width="100px" align="right" bgcolor="#73B6E1"><font
						color="white" size="2">체크인 날짜</font></td>
					<td width="700px" bgcolor="#FFFFFF" align="left">&nbsp;&nbsp;${st_day }</td>
				</tr>

				<tr>
					<td colspan=6></td>
				</tr>

				<tr height=23px>
					<td width="100px" align="right" bgcolor="#73B6E1"><font
						color="white" size="2">체크아웃 날짜</font></td>
					<td width="700px" bgcolor="#FFFFFF" align="left">&nbsp;&nbsp;${end_day }</td>
				</tr>

				<tr>
					<td colspan=6></td>
				</tr>

				<tr height=23px>
					<td width="100px" align="right" bgcolor="#73B6E1"><font
						color="white" size="2">총 인원</font></td>
					<td width="700px" bgcolor="#FFFFFF" align="left">&nbsp;&nbsp;${people }명
					</td>
				</tr>

				<tr>
					<td colspan=6></td>
				</tr>

				<tr height=23px>
					<td width="100px" align="right" bgcolor="#73B6E1"><font
						color="white" size="2">요금 합계</font></td>
					<td width="700px" bgcolor="#FFFFFF" align="left">&nbsp;&nbsp;${price }원
					</td>
				</tr>
				<tr>
					<td colspan=6></td>
				</tr>
				<tr>
					<td colspan=6></td>
				</tr>

				<tr height=23px>
					<td width="800px" align="center" bgcolor="#73B6E1" colspan="2"><font
						color="white" size="4">예약 정보</font></td>
				</tr>
				<tr>
					<td colspan=6></td>
				</tr>

				<tr height=23>
					<td width="100px" align="right" bgcolor="#73B6E1"><font
						color="white" size="2">예약자 성명</font></td>
					<td width="700px" bgcolor="#FFFFFF" align="left">&nbsp;&nbsp;
					<input type="text" name="name" size="15" maxlength="20">
					</td>
				</tr>

				<tr>
					<td colspan=6>
				</tr>

				<tr height=23px>
					<td width="100px" align="right" bgcolor="#73B6E1"><font
						color="white" size="2">예약자 휴대폰</font></td>
					<td width="700px" bgcolor="#FFFFFF" align="left">&nbsp;&nbsp;
					<input type="text" name="phone" size="15" maxlength="20">
					</td>
				</tr>

				<tr>
					<td colspan=6></td>
				</tr>

				<tr height=23px>
					<td width="100px" align="right" bgcolor="#73B6E1"><font
						color="white" size="2">내용</font></td>
					<td width="700px" bgcolor="#FFFFFF" align="left">&nbsp;&nbsp;
					<input type="text" name="content" size="80" maxlength="20">
					</td>
				</tr>
				<tr>
					<td colspan=6></td>
				</tr>
			</table>

			<table width=80% border=0 cellpadding="0" cellspacing="1">
				<tr>
					<td height=10></td>
				</tr>
				<tr>
					<td height=10></td>
				</tr>


				<tr>
					<td colspan=6></td>
				</tr>

				<tr>
					<td colspan=6></td>
				</tr>


				<tr>
					<td colspan=6></td>
				</tr>
				<tr>
					<td colspan=6></td>
				</tr>

				<tr>
					<td align=center colspan=6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a onclick="checkIt()"><img style="cursor:pointer" src="/twenty/resources/pension_images/btn_buy.png"></a> &nbsp; <a
						href="${contextPath }/twenty/pensionView?pen_name=${room.pen_name }"/><img
							src="/twenty/resources/pension_images/btn_list.png"
							style="width: 84px; height: 25px; border: 0px;" /></a></td>
				</tr>

			</table>
		</form>
	</div>
</body>
</html>