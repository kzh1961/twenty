<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.HashMap"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<title>상품!</title>

<!-- 여기서부터 스크립트 -->
<link href="/web/css/style.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<link href="/style.css" rel="stylesheet" type="text/css">
<script>
	$(function() {

		$.datepicker.regional['ko'] = {
			closeText : '닫기',
			prevText : '이전달',
			nextText : '다음달',
			currentText : '오늘',	
			monthNames : [ '1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월',
					'9월', '10월', '11월', '12월' ],
			monthNamesShort : [ '1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월',
					'9월', '10월', '11월', '12월' ],
			dayNames : [ '일', '월', '화', '수', '목', '금', '토' ],
			dayNamesShort : [ '일', '월', '화', '수', '목', '금', '토' ],
			dayNamesMin : [ '일', '월', '화', '수', '목', '금', '토' ],
			weekHeader : 'Wk',
			dateFormat : 'yy-mm-dd',
			firstDay : 0,
			isRTL : false,
			duration : 200,
			showAnim : 'show',
			showMonthAfterYear : true,
			yearSuffix : '년',
			beforeShowDay : disableAllTheseDays
		};
		$.datepicker.setDefaults($.datepicker.regional['ko']);

		$("#datepicker").datepicker(
				{
					dateFormat : 'yy-mm-dd',
					changeMonth : true,
					changeYear : true,
					minDate : 0, //오늘날짜 이전 비활성화
					onSelect : function(selectedDate) {

						var option = this.id == "datepicker" ? "minDate"
								: "maxDate", instance = $(this).data(
								"datepicker"), date = $.datepicker.parseDate(
								instance.settings.dateFormat
										|| $.datepicker._defaults.dateFormat,
								selectedDate, instance.settings);
						jQuery("#datepicker, #datepicker1").not(this)
								.datepicker("option", option, date);
						$('input[type=text][id=datepicker]').val(
								$("#datepicker").val())

						var click = $("#datepicker").val();
						var arr = click.split("-");
						var clickDate = new Date(arr[0], Number(arr[1]) - 1,
								arr[2]);

						for (var i = 0; i < disabledDays.length; i++) {
							var arr2 = disabledDays[i].split("-");
							var disDate = new Date(arr2[0],
									Number(arr2[1]) - 1, arr2[2]);

							if (clickDate.getTime() < disDate.getTime()) {
								$("#datepicker1").datepicker("option",
										"maxDate", disabledDays[i]);
								break;
							} else {
								$("#datepicker1").datepicker("option",
										"maxDate", "");

							}
						}
					}

				});
	});

	$(function() {

		$.datepicker.regional['ko'] = {
			closeText : '닫기',
			prevText : '이전달',
			nextText : '다음달',
			currentText : '오늘',
			monthNames : [ '1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월',
					'9월', '10월', '11월', '12월' ],
			monthNamesShort : [ '1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월',
					'9월', '10월', '11월', '12월' ],
			dayNames : [ '일', '월', '화', '수', '목', '금', '토' ],
			dayNamesShort : [ '일', '월', '화', '수', '목', '금', '토' ],
			dayNamesMin : [ '일', '월', '화', '수', '목', '금', '토' ],
			weekHeader : 'Wk',
			dateFormat : 'yy-mm-dd',
			firstDay : 0,
			isRTL : false,
			duration : 200,
			showAnim : 'show',
			showMonthAfterYear : true,
			yearSuffix : '년'
		};
		$.datepicker.setDefaults($.datepicker.regional['ko']);

		$("#datepicker1").datepicker(
				{
					dateFormat : 'yy-mm-dd',
					changeMonth : true,
					changeYear : true,
					minDate : 0, //오늘날짜 이전 비활성화

					onSelect : function(selectedDate) {

						var option = this.id == "datepicker" ? "minDate"
								: "maxDate", instance = $(this).data(
								"datepicker"), date = $.datepicker.parseDate(
								instance.settings.dateFormat
										|| $.datepicker._defaults.dateFormat,
								selectedDate, instance.settings);
						jQuery("#datepicker, #datepicker1").not(this)
								.datepicker("option", option, date);
						$('input[type=text][id=datepicker1]').val(
								$("#datepicker1").val());

					}
				});
	});

	function disableAllTheseDays(date) {
		var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
		for (i = 0; i < disabledDays.length; i++) {

			if ($.inArray(y + '-' + (m + 1) + '-' + d, disabledDays) != -1) {
				return [ false ];
			}
		}
		return [ true ];
	}

	var disabledDays = new Array();
	var i = 0;
	
	<c:forEach var="expiryDate" items="${expiryDate}">
	disabledDays[i] = "${expiryDate}";
	i++;
	</c:forEach>


</script>
<style type="text/css">
.f_indigo {
	font-weight: 600;
}

.indigo {
	color: #FC5522;
	font-weight: 700;
}
</style>
<!-- 스크립트 끝 -->
<script type="text/javascript">
	function check() {
		var frm = document.pension;

		if (frm.st_day.value == "") {
			alert("시작일을 선택해주세요");
			return false;
		}
		if (frm.end_day.value == "") {
			alert("도착일을 선택해주세요");
			return false;
		}
	}
</script>
</head>
<body>
<div class="container">

	<form name="pension" action="<%=request.getContextPath() %>/res/penResInfo" method="post" onsubmit="return check()">

		 <input type="hidden" name="room_num" value="${room.room_num }"/>
		 <input type="hidden" name="id" value="${session.id}"/>
		 <input type="hidden" name="pen_name" value="${room.pen_name }"/>
		 <input type="hidden" name="price1" value="${room.price1 }"/>
		 <input type="hidden" name="price2" value="${room.price2 }"/>
		 <input type="hidden" name="room_image1" value="${room.room_image1 }"/>
		 <input type="hidden" name="room_image2" value="${room.room_image2 }"/>
		 <input type="hidden" name="content" value="${room.content }"/>
		<table width="1200" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="950" align="right" height="450" valign="top" 
					bgcolor="white">
					<table width="950" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="950" colspan="3">
								<table width="950" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="150" align="right" class="orange">
										<td width="20">&nbsp;</td>
										<td width="580"></td>
									</tr>
									<tr>
										<td height="40" colspan="3">&nbsp;</td>
									</tr>
								</table>
							</td>
						<tr>
							<td width="100" valign="middle" align="left">
								<table width="30" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td align="center">
										<img src="/twenty/resources/pension_images/next_on_left.png" onclick="javascript:history.back()" style="cursor: pointer" ;/>
									
										</td>
									</tr>
								</table>
							</td>

							<td width="750" height="350" valign="top" align="left">
								<table width="750" border="0" cellspacing="0" cellpadding="0"
									border="1">
									<tr>
										<td width="325" align="right">
											<div id="datepicker">
												&nbsp;&nbsp;체크인날짜&nbsp;&nbsp;<input
													type="text" id="datepicker" name="st_day"
													style="border: 0px solid #eee; margin: 10px;" size="7">
													<font class="indigo" style="margin-left:5px">인원</font>
											<select name="people">
							
												<c:forEach var="i" begin="1"
													end="${room.max_people }" step="1">
													<option value="${i }">${i }</option>
												</c:forEach>
										</select>
											</div>
										</td>
										<td width="325" align="right">
											<div id="datepicker1" >
												체크아웃날짜&nbsp;&nbsp;<input
													type="text" id="datepicker1" name="end_day"
													style="border: 0px solid #eee; margin: 10px;" size="7">
											</div>
										</td>
									</tr>
								</table>
							</td>
							<td width="20" valign="middle">
								<table width="100" border="0" cellspacing="0" cellpadding="0" height="100">
									<tr>
										<td align="center">
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" value="" style="width: 100px; height: 80px;margin-left:30px; border: 0; background: url('/twenty/resources/pension_images/next_on_right.png') no-repeat; cursor: pointer; margin: 8px 0 0 6px;">
										</td>
									</tr>
								</table>
							</td>

						</tr>
					</table>

				

			

				<!-- 추가 -->
				<td width="150" bgcolor="white" height="450" valign="top">
					<table width="250" cellspacing="10" cellpadding="0" bgcolor="#F2F2F2">
						<tr>
							<td height="20" valign="bottom" class="f_indigo"><!-- <img
								src="./pension/image/css3.png"> -->&nbsp;펜션이름</td>
						</tr>
						<tr>
							<td class="indigo">${room.pen_name }</td>
						</tr>
						<tr>
							<td height="40" valign="bottom" class="f_indigo"><!-- <img
								src="./pension/image/css3.png"> -->&nbsp;주중금액</td>
						</tr>
						<tr>
							<td class="indigo">${room.price1 }원</td>
						</tr>
						<tr>
							<td height="40" valign="bottom" class="f_indigo"><img
								src="/twenty/resources/pension_images/css3.png">&nbsp;주말금액</td>
						</tr>
						<tr>
							<td class="indigo">${room.price2 }원</td>
						</tr>
						<tr>
							<td height="40" valign="bottom" class="f_indigo"><img
								src="/twenty/resources/pension_images/css3.png">&nbsp;이미지</td>
						</tr>
						<tr>
		
								<td><img
									src="/twenty/resources/room_images/${room.room_image1 }"
									width="197" border="0"></td>
						</tr>
						<tr>
							<td height="20" valign="middle" class="f_indigo"><img
								src="/twenty/resources/pension_images/css3.png">&nbsp;체크아웃 날짜</td>
						</tr>
						<tr>
							<td class="f_indigo">&nbsp;&nbsp;&nbsp;<input type="text"
								id="datepicker" name="depart_date" class="indigo"
								style="border: 1px solid #eee; border-color: white;" size="20"></td>
						</tr>
						<tr>
							<td height="20" valign="middle" class="f_indigo"><img
								src="/twenty/resources/pension_images/css3.png">&nbsp;체크인 날짜</td>
						</tr>
						<tr>
							<td class="f_indigo">&nbsp;&nbsp;&nbsp;<input type="text"
								id="datepicker1" name="arrival_date" class="indigo"
								style="border: 1px solid #eee; border-color: white" size="20"></td>
						</tr>
					</table> 
	</table>
	</form>
	<!-- 추가끝 -->
	<td width="20" bgcolor="white">&nbsp;</td>
</div>
</body>
</html>