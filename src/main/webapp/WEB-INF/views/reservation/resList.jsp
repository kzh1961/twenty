<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style type="text/css">
.paging {
	text-align: center;
	height: 32px;
	margin-top: 5px;
	margin-bottom: 15px;
}

.paging a, .paging strong {
	display: inline-block;
	width: 36px;
	height: 32px;
	line-height: 28px;
	font-size: 14px;
	border: 1px solid #e0e0e0;
	margin-left: 5px;
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius: 3px;
	-webkit-box-shadow: 1px 1px 1px 0px rgba(235, 235, 235, 1);
	-moz-box-shadow: 1px 1px 1px 0px rgba(235, 235, 235, 1);
	box-shadow: 1px 1px 1px 0px rgba(235, 235, 235, 1);
}

.paging a:first-child {
	margin-left: 0;
}

.paging strong {
	color: #fff;
	background: skyblue;
	border: 1px solid skyblue;
}

.paging .page_arw {
	font-size: 11px;
	line-height: 30px;
}
</style>
<script>
function confirmCheck(res_num){
	var con = confirm("예약을 취소하시겠습니까?");
	if(con==true){
		location.href="/twenty/res/resDelete?res_num="+res_num;
	}
	false;
}
</script>
</head>
<body>
	<!--start-breadcrumbs-->
	<div class="breadcrumbs">
		<div class="container">
			<div class="breadcrumbs-main">
				<ol class="breadcrumb">
					<li><a href="./index.jsp">Home</a></li>
					<li>마이페이지</li>
					<li class="active">예약내역</li>
				</ol>
			</div>
		</div>
	</div>
	<!--end-breadcrumbs-->
	<div class="container" style="margin-bottom: 60px;">
		<div class="register-top heading">
			<br> <br>
			<h2>예약내역</h2>
			<br> <br>
		</div>
		<div>
			<table class="table table-striped">
				<thead>
					<tr>
						<th style="width: 7%; text-align: center;">이름</th>
						<th style="width: 8%; text-align: center;">인원수</th>
						<th style="width: 14%; text-align: center;">체크인날짜</th>
						<th style="width: 14%; text-align: center;">체크아웃날짜</th>
						<th style="width: 8%; text-align: center;">연락처</th>
						<th style="width: 14%; text-align: center;">내용</th>
						<th style="width: 12%; text-align: center;">펜션명</th>
						<th style="width: 6%; text-align: center;">객실</th>
						<th style="width: 10%; text-align: center;">결제금액</th>
						<th style="width: 7%; text-align: center;">취소</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="resList" items="${resList}">
						<tr>
							<td style="width: 7%; text-align: center;">${resList.name}</td>
							<td style="width: 8%; text-align: center;">${resList.people}</td>
							<td style="width: 14%; text-align: center;">${resList.st_day}</td>
							<td style="width: 14%; text-align: center;">${resList.end_day}</td>
							<td style="width: 8%; text-align: center;">${resList.phone}</td>
							<td style="width: 14%; text-align: center;">${resList.content}</td>
							<td style="width: 12%; text-align: center;">${resList.pen_name}</td>
							<td style="width: 6%; text-align: center;">${resList.room_num}</td>
							<td style="width: 10%; text-align: center;">${resList.price}</td>
							<td style="width: 7%; text-align: center;"><img src="/twenty/resources/images/close.png" style="cursor:pointer" onclick="confirmCheck('${resList.res_num}')" onfocus=""></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		<div>
			<c:if test="${fn:length(resList) le 0}">
				<br />
				<center>예약 내역이 없습니다.</center>
				<br />
			</c:if>
		</div>
	</div>
</body>
</html>