<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript">
	function list() {
		if (confirm("목록으로 가시겠습니까?") == true) {
			location.href = 'adminNoticeList';
		} else {
			return;
		}
	}
</script>
</head>
<body>
	<!--start-breadcrumbs-->
	<div class="breadcrumbs">
		<div class="container">
			<div class="breadcrumbs-main">
				<ol class="breadcrumb">
					<li><a href="./index.jsp">Home</a></li>
					<li>고객센터</li>
					<li class="active">공지사항</li>
				</ol>
			</div>
		</div>
	</div>
	<!--end-breadcrumbs-->
	<div class="container">
		<div class="register-top heading">
			<br> <br>
			<h2>공지사항</h2>
			<br> <br>
		</div>
		<form:form commandName="noticeModel" action="./noticeModify"
			method="post">
			<form:input type="hidden" path="notice_num"
				value="${noticeModel.notice_num}" />
			<div class="col-sm-offset-1 col-sm-10 form-group">
				<label for="subject">제목:</label>
				<form:input class="form-control" type="text" path="subject"
					value="${noticeModel.subject}" required="requiered" />
			</div>
			<div class="col-sm-offset-1 col-sm-10 form-group">
				<label for="content">내용:</label>
				<form:textarea class="form-control" rows="20" path="content"
					value="${noticeModel.content}" required="requiered"></form:textarea>
			</div>
			<br>
			<div class="col-sm-offset-1 col-sm-10 form-group"
				style="text-align: center">
				<br>
				<button type="submit" class="btn btn-info">수정하기</button>
				<button type="button" class="btn btn-default" onclick="list();">취소</button>
			</div>
		</form:form>
	</div>
	<br>
	<br>
	<br>
	<br>
	<br>
</body>
</html>