<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style type="text/css">
.paging {
	text-align: center;
	height: 32px;
	margin-top: 5px;
	margin-bottom: 15px;
}

.paging a, .paging strong {
	display: inline-block;
	width: 36px;
	height: 32px;
	line-height: 28px;
	font-size: 14px;
	border: 1px solid #e0e0e0;
	margin-left: 5px;
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius: 3px;
	-webkit-box-shadow: 1px 1px 1px 0px rgba(235, 235, 235, 1);
	-moz-box-shadow: 1px 1px 1px 0px rgba(235, 235, 235, 1);
	box-shadow: 1px 1px 1px 0px rgba(235, 235, 235, 1);
}

.paging a:first-child {
	margin-left: 0;
}

.paging strong {
	color: #fff;
	background: skyblue;
	border: 1px solid skyblue;
}

.paging .page_arw {
	font-size: 11px;
	line-height: 30px;
}
</style>
</head>
<body>
	<!--start-breadcrumbs-->
	<div class="breadcrumbs">
		<div class="container">
			<div class="breadcrumbs-main">
				<ol class="breadcrumb">
					<li><a href="./index.jsp">Home</a></li>
					<li>고객센터</li>
					<li class="active">공지사항</li>
				</ol>
			</div>
		</div>
	</div>
	<!--end-breadcrumbs-->
	<div class="container">
		<div class="register-top heading">
			<br> <br>
			<h2>공지사항</h2>
			<br> <br>
		</div>
		<div>
			<table class="table table-striped">
				<thead>
					<tr>
						<th style="width: 10%; text-align: center;">번호</th>
						<th style="width: 50%; text-align: center;">제목</th>
						<th style="width: 20%; text-align: center;">등록일</th>
						<th style="width: 10%; text-align: center;">조회</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="list" items="${noticeList}">
						<c:url var="viewURL" value="noticeView">
							<c:param name="notice_num" value="${list.notice_num}" />
							<c:param name="currentPage" value="${currentPage}" />
						</c:url>
						<tr>
							<td style="width: 10%; text-align: center;">${list.notice_num}</td>
							<td style="width: 50%; text-align: left;"><a
								href="${viewURL}">${list.subject}</a></td>
							<td style="width: 20%; text-align: center;"><fmt:formatDate
									value="${list.regdate}" pattern="YYYY-MM-dd" /></td>
							<td style="width: 10%; text-align: center;">${list.readcount}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		<div>
			<c:if test="${fn:length(noticeList) le 0}">
				<br />
				<center>등록된 게시물이 없습니다</center>
				<br />
			</c:if>
		</div>
		<form>
			<c:if test="${session_id == 'admin' }">
				<a href="./noticeWriteForm" class="btn btn-info" role="button">글쓰기</a>
			</c:if>
			<div class="col-sm-offset-5 paging" style="display: inline;">${pagingHtml}</div>
			<button type="submit" class="btn btn-info" style="float: right;">
				<span class="glyphicon glyphicon-search"></span> 검색
			</button>
			<div class="col-xs-2" style="float: right;">
				<input type="text" class="form-control" name="isSearch"
					required="required"> <br>
			</div>
			<div style="float: right;">
				<label class="radio-inline"><input type="radio"
					name="searchNum" value="1" required="required">제목</label> <label
					class="radio-inline"><input type="radio" name="searchNum"
					value="2" required="required">내용</label>
			</div>
		</form>
	</div>
	<br>
	<br>
	<br>
	<br>
	<br>
</body>
</html>