<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript">
	function onDelete() {
		if (confirm("삭제하겠습니까?") == true) {
			location.href = 'noticeDelete?notice_num=${noticeModel.notice_num}';
		} else {
			return;
		}
	}
	function onModify() {
		location.href = 'noticeModifyForm?notice_num=${noticeModel.notice_num}';
	};
</script>
<style>
p {
	display: inline-block;
}

#regdate {
	float: right;
}
</style>
</head>
<body>
	<!--start-breadcrumbs-->
	<div class="breadcrumbs">
		<div class="container">
			<div class="breadcrumbs-main">
				<ol class="breadcrumb">
					<li><a href="./index.jsp">Home</a></li>
					<li>고객센터</li>
					<li class="active">공지사항</li>
				</ol>
			</div>
		</div>
	</div>
	<!--end-breadcrumbs-->
	<br>
	<div class="container">
		<div class="register-top heading">
			<br> <br>
			<h2>공지사항</h2>
		</div>
		<ul class="pager">
			<li class="next"><a href="./noticeList">목록</a></li>
		</ul>
		<div class="panel panel-default">
			<div class="panel-heading">
				<p>${noticeModel.subject }</p>
				<p id="regdate">
					<fmt:formatDate value="${noticeModel.regdate }"
						pattern="yyyy-MM-dd" />
				</p>
			</div>
			<div class="panel-body" style="min-height: 200px">
				<br>${noticeModel.content }<br> <br>
			</div>
		</div>
		<c:if test="${session_id == 'admin' }">
			<div style="text-align: center">
				<button type="button" class="btn btn-info" onclick="onModify()">수정</button>
				<button type="button" class="btn btn-info" onclick="onDelete()">삭제</button>
			</div>
		</c:if>
	</div>
	<br>
	<br>
	<br>
	<br>
	<br>
</body>
</html>

