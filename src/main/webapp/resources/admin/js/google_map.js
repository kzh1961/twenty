/*이 프로젝트에선 안쓰는 것임*/
var map_x;
var map_y;
var contents = "";
var map;

function setLocation(x, y) {
	map_x = x;
	map_y = y;
}

function initialize() {
	var cairo = {lat: map_x, lng: map_y};
	var mapOptions = {
		zoom : 16,
		scaleControl: true,
		/* center: new google.maps.LatLng(35.87110100714382, 128.60169690333006) */
		center : cairo
	};
	map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
	
	var infowindow = new google.maps.InfoWindow;
	infowindow.setContent('<b>' + contents + '</b>');
	
	var markers = []; // 요렇게 일단 다중 마커를 담을 변수를 만들어요.

	for (var i = 0; i < 1000; ++i) {
	//반복문으로 외부 데이터를 기반으로 마커를 생성해줍니다.
	var latLng = new google.maps.LatLng(data.photos[i].latitude,
	     data.photos[i].longitude);
	var marker = new google.maps.Marker({map: map,
	            position: latLng,
	            draggable: true,
	            icon: markerImage
	          });
	marker.addListener('click', function() {
		  infowindow.open(map, marker);
		});
	     markers.push(marker); //마커들을 markers 배열안에 넣습니다.
	}
}

function geocode(address1, address2, content) {
	var address = "";
	address = address1 + " " + address2;
	contents = content;
	var geocoder = new google.maps.Geocoder();
	geocoder.geocode({
		'address' : address,
		'partialmatch' : true
	}, geocodeResult);
}

function geocodeResult(results, status) {
	//if( status == google.maps.GeocoderStatus.OK ) {
	if (status == 'OK' && results.length > 0) {
		//map.fitBounds(results[0].geometry.viewport);
		setLocation(results[0].geometry.location.lat(), results[0].geometry.location.lng());
		initialize();
	} else {
		alert("Geocode was not successful for the following reason: "
				+ status);
	}
}
/* google.maps.event.addDomListener(window, 'load', geocode());  */