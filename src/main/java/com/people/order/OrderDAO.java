package com.people.order;

import java.util.List;

import com.people.order.OrderModel;

public interface OrderDAO {

	public Object OrderInsert(OrderModel orderModel);

	public List<OrderModel> OrderList(OrderModel orderModel);

	public Object deleteOrder(OrderModel orderModel);

	public OrderModel OrdergetOne(int order_num);

	public Object OrderModify(OrderModel orderModel);

	public Object OrderTradeNum();
}
