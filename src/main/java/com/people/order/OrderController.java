package com.people.order;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.people.order.OrderModel;
import com.people.util.Paging;
import com.people.basket.BasketModel;
import com.people.basket.BasketService;
import com.people.goods.GoodsModel;
import com.people.goods.GoodsService;
import com.people.member.MemberModel;
import com.people.member.MemberService;

@Controller
public class OrderController {
	private int currentPage = 1;
	private int totalCount;
	private int blockCount = 10;
	private int blockPage = 5;
	private String pagingHtml;
	private Paging page;
	int check = 0;
	String me = "포인트가 적합하지 않습니다";
	
	@Resource(name = "orderService")
	private OrderService orderService;

	@Resource(name = "memberService")
	private MemberService memberService;

	@Resource(name = "goodsService")
	private GoodsService goodsService;
	
	@Resource(name = "basketService")
	private BasketService basketService;
	
	MemberModel memberModel = new MemberModel();
	
	BasketModel basketModel = new BasketModel();

	private List<BasketModel> basketList = new ArrayList<BasketModel>();
	
	private List<OrderModel> orderList = new ArrayList<OrderModel>();
	
	@RequestMapping(value = "/orderForm")
	public String orderForm(GoodsModel goodsModel, Model model, HttpSession session, HttpServletRequest request) {
		
		String id = request.getParameter("basket_member_id");
		int goods_num = goodsModel.getGoods_num();
		int amount = goodsModel.getGoods_amount();

		memberModel = memberService.getMember(id);
		goodsModel = goodsService.goodsView(goods_num);
		goodsModel.setGoods_amount(amount);

		model.addAttribute("memberModel", memberModel);
		model.addAttribute("goodsModel", goodsModel);
		
		if(check == 1) {
		
		model.addAttribute("me" , me);
		
		check = 0;
		}

		return "orderForm";

	}

	@RequestMapping(value = "/orderForm1")
	public String orderForm1(HttpSession session, MemberModel memberModel,
			GoodsModel goodsModel,OrderModel orderModel, Model model, HttpServletRequest request) {
	
		int usepoint = Integer.parseInt(request.getParameter("usepoint"));
		int goods_num = goodsModel.getGoods_num();
		int amount = goodsModel.getGoods_amount();
		String id = session.getAttribute("session_id").toString();
		
		memberModel = memberService.getMember(id);
			
		if( usepoint > 0 && (usepoint < 1000 || usepoint > memberModel.getPoint())) {
			
			check = 1;
			
			return "redirect:orderForm?goods_num=" + goods_num + "&goods_amount=" + amount + "&basket_member_id=" + id; 
		}

		orderModel.setOrder_goods_num(goods_num);
		orderModel.setOrder_goods_amount(amount);
		orderModel.setOrder_member_id(id);
		orderModel.setOrder_receive_zipcode(memberModel.getZipcode());
		orderModel.setOrder_receive_addr(memberModel.getAddress1() + " " + memberModel.getAddress2());
		orderModel.setOrder_goods_name(goodsModel.getGoods_name());

		if (orderModel.getOrder_memo() == "") {
			orderModel.setOrder_memo("없음");
		}

		goodsModel = goodsService.goodsView(goods_num);

		model.addAttribute("goodsModel", goodsModel);
		model.addAttribute("orderModel", orderModel);
		model.addAttribute("usepoint", usepoint);

		return "orderBuyForm";

	}
	
	@RequestMapping(value = "/orderBuyOk")
	public String orderBuyOk(Model model, OrderModel orderModel, HttpServletRequest request, HttpSession session) {
		
		GoodsModel goodsModel = new GoodsModel();
		
		orderModel.setOrder_trans_num("준비중");
		orderModel.setOrder_status("상품준비");
		
		goodsModel = goodsService.goodsView(orderModel.getOrder_goods_num());
    	int amount = goodsModel.getGoods_amount() - orderModel.getOrder_goods_amount();
    	goodsModel.setGoods_amount(amount);
    	
    	goodsService.amountDown(goodsModel);
    	orderService.OrderInsert(orderModel);
    	memberModel.setId(orderModel.getOrder_member_id());
    	int point = (memberModel.getPoint() - orderModel.getUsepoint()) + ((goodsModel.getGoods_price()*orderModel.getOrder_goods_amount()-orderModel.getUsepoint())/20);
		System.out.println("포인트" + memberModel.getPoint());
		System.out.println("사용포인트" + orderModel.getUsepoint());
		System.out.println(point);
    	memberModel.setPoint(point);
    	memberService.pointUpdate(memberModel);
    	System.out.println(orderModel.getOrder_date());
    	model.addAttribute("orderModel", orderModel);
    
    	return "orderOK";
		
	}
	
	@RequestMapping(value="/basketOrderForm")
	public String basketOrderForm(Model model, HttpSession session){
		
		String id = (String)session.getAttribute("session_id");
		
		basketModel.setBasket_id(id);
		basketList = basketService.BasketList(basketModel);
		memberModel = memberService.getMember(id);
		
		model.addAttribute("basketList", basketList);
		model.addAttribute("memberModel", memberModel);
		
		if(check == 1) {
			
		model.addAttribute("me", me);
			
		check = 0;
		}
		
		return "basketOrderForm";
	}
	
	@RequestMapping(value="/basketOrderBuyForm")
	public String basketOrderBuyForm(Model model, OrderModel orderModel, HttpServletRequest request, HttpSession session){
		
		int usepoint = Integer.parseInt(request.getParameter("usepoint"));
		orderModel.setOrder_receive_addr(memberModel.getAddress1()+" "+memberModel.getAddress2());
		orderModel.setOrder_member_id(memberModel.getId());
		
		memberModel = memberService.getMember(session.getAttribute("session_id").toString());
		
		if( usepoint > 0 && (usepoint < 1000 || usepoint > memberModel.getPoint()) ) {
			
			check = 1;
			
			return "redirect:basketOrderForm?id="+ memberModel.getId();
		}
		
		model.addAttribute("basketList", basketList);
		model.addAttribute("orderModel", orderModel);
		model.addAttribute("usepoint", usepoint);
		return "basketOrderBuyForm";
	}
	
	@RequestMapping(value="/basketOrderOk")
	public String basketOrderOk(Model model, OrderModel orderModel, HttpSession session){
		
		int amount;
		
		memberModel = memberService.getMember(session.getAttribute("session_id").toString());
		
		GoodsModel goodsModel = new GoodsModel();
		for(int i=0;i<basketList.size();i++){
			basketModel = basketList.get(i);
			goodsModel = goodsService.goodsView(basketModel.getBasket_goods_num());
			amount = goodsModel.getGoods_amount()-basketModel.getBasket_goods_amount();
			if(amount < 0){
				System.out.println(basketModel.getBasket_goods_name()+" 상품의 수량이 부족합니다.");
				return "basketOrderBuyForm";
			}
			goodsModel.setGoods_amount(amount);
			goodsService.amountDown(goodsModel);
			
			orderModel.setOrder_goods_name(basketModel.getBasket_goods_name());
			orderModel.setOrder_goods_amount(basketModel.getBasket_goods_amount());
			orderModel.setOrder_goods_image(basketModel.getBasket_goods_image1());
			orderModel.setOrder_goods_num(basketModel.getBasket_goods_num());
			orderModel.setOrder_goods_price(basketModel.getBasket_goods_price());
			orderModel.setOrder_sum_money(basketModel.getBasket_goods_price() * basketModel.getBasket_goods_amount());
			orderModel.setOrder_member_id(basketModel.getBasket_id());
			orderModel.setOrder_status("상품준비중");
			orderModel.setOrder_trans_num("준비중");
			orderService.OrderInsert(orderModel);
			
			memberModel.setPoint((memberModel.getPoint()-orderModel.getUsepoint()) + (( basketModel.getBasket_goods_price() * basketModel.getBasket_goods_amount() - orderModel.getUsepoint() ) /20 ));
			memberModel.setId(basketModel.getBasket_id());
			memberService.pointUpdate(memberModel);
		}
		basketService.deleteAllBasket(basketModel);
		model.addAttribute("orderModel", orderModel);
		
		return "basketOrderOk";
	}
	
	@RequestMapping(value="orderList")
	public String orderList(HttpSession session, Model model,HttpServletRequest request){
		if (request.getParameter("currentPage") == null || request.getParameter("currentPage").trim().isEmpty()
				|| request.getParameter("currentPage").equals("0")) {
			currentPage = 1;
		} else {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		OrderModel orderModel = new OrderModel();
		orderModel.setOrder_member_id(session.getAttribute("session_id").toString());
		orderList = orderService.OrderList(orderModel);
		totalCount = orderList.size();
		
		page = new Paging(currentPage, totalCount, blockCount, blockPage, "orderList");
		pagingHtml = page.getPagingHtml().toString();
		
		int lastCount = totalCount;
		
		if (page.getEndCount() < totalCount)
			lastCount = page.getEndCount() + 1;
		
		orderList = orderList.subList(page.getStartCount(), lastCount);
		
		model.addAttribute("orderList",orderList);
		model.addAttribute("pagingHtml", pagingHtml);
		model.addAttribute("totalCount", totalCount);
		model.addAttribute("currentPage", currentPage);
		return "orderList";
	}
}
