package com.people.room;

public class RoomModel {

	private int room_num;
	private String pen_name;
	private String price1;
	private String price2;
	private String max_people;
	private String room_image1;
	private String room_image2;
	private String content;

	public int getRoom_num() {
		return room_num;
	}

	public void setRoom_num(int room_num) {
		this.room_num = room_num;
	}

	public String getPen_name() {
		return pen_name;
	}

	public void setPen_name(String pen_name) {
		this.pen_name = pen_name;
	}

	public String getPrice1() {
		return price1;
	}

	public void setPrice1(String price1) {
		this.price1 = price1;
	}

	public String getPrice2() {
		return price2;
	}

	public void setPrice2(String price2) {
		this.price2 = price2;
	}

	public String getMax_people() {
		return max_people;
	}

	public void setMax_people(String max_people) {
		this.max_people = max_people;
	}

	public String getRoom_image1() {
		return room_image1;
	}

	public void setRoom_image1(String room_image1) {
		this.room_image1 = room_image1;
	}

	public String getRoom_image2() {
		return room_image2;
	}

	public void setRoom_image2(String room_image2) {
		this.room_image2 = room_image2;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
