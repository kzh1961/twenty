package com.people.room;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Service;

@Service
public class RoomService implements RoomDAO {

	@Resource(name = "sqlSessionTemplate")
	private SqlSessionTemplate sqlSessionTemplate;
	
	/*諛⑸━�ㅽ��*/
	@Override
	public List<RoomModel> roomList(String pen_name) {
		return sqlSessionTemplate.selectList("room.roomList", pen_name);

	}
	
	/*諛⑹���몃낫湲�*/
	@Override
	public RoomModel roomView(RoomModel roomModel) {
		return sqlSessionTemplate.selectOne("room.roomView", roomModel);
	}
	
	/*愿�由ъ��諛⑹�媛�*/
	@Override
	public Object adminRoomWrite(RoomModel roomModel) {
		return sqlSessionTemplate.insert("room.adminRoomWrite", roomModel);
	}
	
	/*愿�由ъ��諛⑸━�ㅽ��*/
	@Override 
	public List<RoomModel> adminRoomList() {
		return sqlSessionTemplate.selectList("room.adminRoomList");
	}
	
	/*愿�由ъ��諛⑹����*/
	@Override
	public Object adminRoomDelete(RoomModel roomModel) {
		return sqlSessionTemplate.delete("room.adminRoomDelete", roomModel);
	}
	
	@Override
	public String minprice(String pen_name) {
		return sqlSessionTemplate.selectOne("room.minprice", pen_name);
	}

	@Override
	public Object roomAllDelete(String pen_name) {
		return sqlSessionTemplate.delete("room.roomAllDelete", pen_name);
	}

	@Override
	public List<Map<String, Object>> imageDelete(String pen_name) {
		return sqlSessionTemplate.selectList("room.imageDelete", pen_name);
	}

	@Override
	public Object roomModify(RoomModel roomModel) {
		return sqlSessionTemplate.update("room.roomModify", roomModel);
	}

	
	
}
