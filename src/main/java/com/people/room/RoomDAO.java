package com.people.room;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface RoomDAO {
	
	/*펜션상세방리스트*/
	public List<RoomModel> roomList(String pen_name);
	
	/*방상세보기*/
	public RoomModel roomView(RoomModel roomModel);
	
	/*관리자방등록*/
	public Object adminRoomWrite(RoomModel roomModel);
	
	/*관리자방리스트*/
	public List<RoomModel> adminRoomList();
	
	/*관리자방삭제*/
	public Object adminRoomDelete(RoomModel roomModel);

	public String minprice(String pen_name);

	/*펜션 삭제시 룸 전부삭제*/
	public Object roomAllDelete(String pen_name);

	public List<Map<String, Object>> imageDelete(String pen_name);
	
	public Object roomModify(RoomModel roomModel);	
}
