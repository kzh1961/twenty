package com.people.main;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.people.goods.GoodsModel;
import com.people.goods.GoodsService;
import com.people.pension.PensionModel;
import com.people.pension.PensionService;

@Controller
public class MainController {

	private String search;
	@Resource(name = "pensionService")
	private PensionService pensionService;

	@Resource(name = "goodsService")
	private GoodsService goodsService;

	@RequestMapping(value = "/main")
	public String mainForm(Model model) {

		// goods
		List<GoodsModel> goodsSelectList1 = goodsService.goodsSelectList("1");
		List<GoodsModel> goodsSelectList2 = goodsService.goodsSelectList("2");
		List<GoodsModel> goodsSelectList3 = goodsService.goodsSelectList("3");
		List<GoodsModel> goodsSelectList4 = goodsService.goodsSelectList("4");
		List<GoodsModel> goodsSelectList5 = goodsService.goodsSelectList("5");
		List<GoodsModel> goodsSelectList6 = goodsService.goodsSelectList("6");
		List<GoodsModel> goodsSelectList7 = goodsService.goodsSelectList("7");
		List<GoodsModel> goodsSelectList8 = goodsService.goodsSelectList("8");
		List<GoodsModel> goodsSelectList9 = goodsService.goodsSelectList("9");
		List<GoodsModel> goodsSelectList10 = goodsService.goodsSelectList("10");
		List<GoodsModel> goodsSelectList11 = goodsService.goodsSelectList("11");
		List<GoodsModel> goodsSelectList12 = goodsService.goodsSelectList("12");
		List<GoodsModel> goodsSelectList13 = goodsService.goodsSelectList("13");
		List<GoodsModel> goodsSelectList14 = goodsService.goodsSelectList("14");
		List<GoodsModel> goodsSelectList15 = goodsService.goodsSelectList("15");
		List<GoodsModel> goodsSelectList16 = goodsService.goodsSelectList("16");
		List<GoodsModel> goodsSelectList17 = goodsService.goodsSelectList("17");
		List<GoodsModel> goodsSelectList18 = goodsService.goodsSelectList("18");
		List<GoodsModel> goodsSelectList19 = goodsService.goodsSelectList("19");
		List<GoodsModel> goodsSelectList20 = goodsService.goodsSelectList("20");

		model.addAttribute("goodsSelectList1", goodsSelectList1);
		model.addAttribute("goodsSelectList2", goodsSelectList2);
		model.addAttribute("goodsSelectList3", goodsSelectList3);
		model.addAttribute("goodsSelectList4", goodsSelectList4);
		model.addAttribute("goodsSelectList5", goodsSelectList5);
		model.addAttribute("goodsSelectList6", goodsSelectList6);
		model.addAttribute("goodsSelectList7", goodsSelectList7);
		model.addAttribute("goodsSelectList8", goodsSelectList8);
		model.addAttribute("goodsSelectList9", goodsSelectList9);
		model.addAttribute("goodsSelectList10", goodsSelectList10);
		model.addAttribute("goodsSelectList11", goodsSelectList11);
		model.addAttribute("goodsSelectList12", goodsSelectList12);
		model.addAttribute("goodsSelectList13", goodsSelectList13);
		model.addAttribute("goodsSelectList14", goodsSelectList14);
		model.addAttribute("goodsSelectList15", goodsSelectList15);
		model.addAttribute("goodsSelectList16", goodsSelectList16);
		model.addAttribute("goodsSelectList17", goodsSelectList17);
		model.addAttribute("goodsSelectList18", goodsSelectList18);
		model.addAttribute("goodsSelectList19", goodsSelectList19);
		model.addAttribute("goodsSelectList20", goodsSelectList20);
		return "mainForm";
	}

	@RequestMapping(value = "/search")
	public String search(Model model, HttpServletRequest request) {

		int searchNum = Integer.parseInt(request.getParameter("searchNum"));

		if (searchNum == 1) {
			search = request.getParameter("search");
			List<GoodsModel> goodsList = goodsService.goodsSearchList(search);

			model.addAttribute("goodsList", goodsList);

			return "goodsList";
		} else if (searchNum == 0) {
			search = request.getParameter("search");
			List<PensionModel> pensionList = pensionService.pensionList(search);
			model.addAttribute("pensionList", pensionList);
			return "pensionList";
		} else if (searchNum == 2) {
			search = request.getParameter("search");
			List<PensionModel> pensionList = pensionService.pensionList2(search);
			model.addAttribute("pensionList", pensionList);
		}
		return "pensionList";
	}

}