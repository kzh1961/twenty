package com.people.basket;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.people.goods.GoodsModel;
import com.people.goods.GoodsService;
@Controller
public class BasketController {
	@Resource(name="basketService")
	private BasketService basketService;
	
	@Resource(name="goodsService")
	private GoodsService goodsService;
	
	private BasketModel basketModel = new BasketModel();
	private List<BasketModel> basketList = new ArrayList<BasketModel>();
	
	@RequestMapping(value="/basketAdd")
	public String basketAdd(HttpServletRequest request, Model model,GoodsModel goodsModel){
		
		String id = request.getParameter("basket_id");
		basketModel.setBasket_goods_amount(goodsModel.getGoods_amount());;
		goodsModel = goodsService.goodsView(goodsModel.getGoods_num());
		
		if(goodsModel.getGoods_image1()==null)
			basketModel.setBasket_goods_image1(" ");
		else
			basketModel.setBasket_goods_image1(goodsModel.getGoods_image1());
		
		basketModel.setBasket_id(id);
		basketModel.setBasket_goods_price(goodsModel.getGoods_price());
		basketModel.setBasket_goods_num(goodsModel.getGoods_num());
		basketModel.setBasket_goods_name(goodsModel.getGoods_name());
		
		basketService.insertBasket(basketModel);
		
		return "redirect:/basketList";
	}
	
	@RequestMapping(value="/basketList")
	public String basketList(HttpSession session,Model model){
		basketModel.setBasket_id(session.getAttribute("session_id").toString());
		basketList = basketService.BasketList(basketModel);
		model.addAttribute("basketList", basketList);
		return "basketList";
	}
	
	@RequestMapping(value="/deleteBasket")
	public String deleteBasket(HttpSession session, HttpServletRequest request, Model model, BasketModel basketModel){	
		basketModel.setBasket_num(Integer.parseInt(request.getParameter("basket_num")));
		basketService.deleteBasket(basketModel);
		return "redirect:basketList";
	}
}
