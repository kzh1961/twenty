package com.people.basket;

import java.util.List;

import javax.annotation.Resource;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Service;
@Service
public class BasketService implements BasketDao{
	
	@Resource(name="sqlSessionTemplate")
	private SqlSessionTemplate sqlSessionTemplate;
	
	@Override
	public Object insertBasket(BasketModel basketModel) {
		
		return sqlSessionTemplate.insert("basket.insertBasket", basketModel);
	}

	@Override
	public List<BasketModel> BasketList(BasketModel basketModel) {
		
		return sqlSessionTemplate.selectList("basket.basketList",basketModel);
	}

	@Override
	public Object deleteBasket(BasketModel basketModel) {
		
		return sqlSessionTemplate.delete("basket.deleteBasket",basketModel);
	}

	@Override
	public Object deleteAllBasket(BasketModel basketModel) {

		return sqlSessionTemplate.delete("basket.deleteAllBasket",basketModel);
	}

		
	
}
