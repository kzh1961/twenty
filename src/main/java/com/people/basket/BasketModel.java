package com.people.basket;

import java.util.Date;

public class BasketModel {
	private int basket_num;
	private String basket_id;
	private int basket_goods_num;
	private String basket_goods_name;
	private int basket_goods_price;
	private int basket_goods_amount;
	private String basket_goods_image1;
	private Date basket_date;
	
	public int getBasket_num() {
		return basket_num;
	}
	public void setBasket_num(int basket_num) {
		this.basket_num = basket_num;
	}
	public String getBasket_id() {
		return basket_id;
	}
	public void setBasket_id(String basket_id) {
		this.basket_id = basket_id;
	}
	public int getBasket_goods_num() {
		return basket_goods_num;
	}
	public void setBasket_goods_num(int basket_goods_num) {
		this.basket_goods_num = basket_goods_num;
	}
	public String getBasket_goods_name() {
		return basket_goods_name;
	}
	public void setBasket_goods_name(String basket_goods_name) {
		this.basket_goods_name = basket_goods_name;
	}
	public int getBasket_goods_price() {
		return basket_goods_price;
	}
	public void setBasket_goods_price(int basket_goods_price) {
		this.basket_goods_price = basket_goods_price;
	}
	public int getBasket_goods_amount() {
		return basket_goods_amount;
	}
	public void setBasket_goods_amount(int basket_goods_amount) {
		this.basket_goods_amount = basket_goods_amount;
	}
	public String getBasket_goods_image1() {
		return basket_goods_image1;
	}
	public void setBasket_goods_image1(String basket_goods_image1) {
		this.basket_goods_image1 = basket_goods_image1;
	}
	public Date getBasket_date() {
		return basket_date;
	}
	public void setBasket_date(Date basket_date) {
		this.basket_date = basket_date;
	}
	
	
}
