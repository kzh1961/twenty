package com.people.admin;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.people.qna.QnAModel;
import com.people.qna.QnAService;
import com.people.reservation.ReservationModel;
import com.people.reservation.ReservationService;
import com.people.room.RoomModel;
import com.people.room.RoomService;
import com.people.goods.GoodsCommentModel;
import com.people.goods.GoodsModel;
import com.people.goods.GoodsService;
import com.people.member.MemberModel;
import com.people.member.MemberService;
import com.people.notice.NoticeModel;
import com.people.notice.NoticeService;
import com.people.order.OrderModel;
import com.people.order.OrderService;
import com.people.pension.PensionModel;
import com.people.pension.PensionService;
import com.people.util.Paging;
import com.people.validator.MemberValidator;
import com.people.validator.NoticeValidator;

//수호
@Controller
public class AdminController {

	@Resource
	private AdminService adminService;

	@Resource
	private GoodsService goodsService;

	@Resource
	private MemberService memberService;
	private String id;

	@Resource
	private NoticeService noticeService;
	
	@Resource
	private OrderService orderService;

	@Resource
	private QnAService qnaService;

	@Resource
	private PensionService pensionService;

	@Resource
	private RoomService roomService;

	@Resource
	private ReservationService reservationService;

	private String pen_name;

	// 상품 파일 업로드 경로
	// String uploadPath =
	// "E:\\app3\\d_pro\\src\\main\\webapp\\resources\\goods_upload\\";
	//
	//
	// String uploadPath = "/pet/src/main/webapp/resources/goods_upload";
	// String uploadPath =
	// "C:\\Java\\KH13\\0930Spring4\\pet\\d_pro\\src\\main\\webapp\\resources\\goods_images\\";
	
	/*String uploadPath = "C:\\twenty\\src\\main\\webapp\\resources\\goods_images\\";*/
	String uploadPath = "C:\\twenty\\src\\main\\webapp\\resources\\goods_images\\";
	String pensionuploadPath = "C:\\twenty\\src\\main\\webapp\\resources\\pension_images\\";
	String roomuploadPath = "C:\\twenty\\src\\main\\webapp\\resources\\room_images\\";

	private int searchNum;
	private String isSearch;

	private int currentPage = 1;
	private int totalCount;
	private int blockCount = 1000;
	private int blockPage = 5;
	private String pagingHtml;
	private Paging page;

	@RequestMapping(value = "adminForm")
	public String adminForm() {
		return "adminForm";
	}

	// 관리자전용 상품리스트
	@RequestMapping("adminGoodsList")
	public String adminGoodsList(Model model, HttpServletRequest request) throws Exception {

		if (request.getParameter("currentPage") == null || request.getParameter("currentPage").trim().isEmpty()
				|| request.getParameter("currentPage").equals("0")) {
			currentPage = 1;
		} else {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}

		List<GoodsModel> goodslist = adminService.goodsList();

		isSearch = request.getParameter("isSearch");
		if (isSearch != null) {
			searchNum = Integer.parseInt(request.getParameter("searchNum"));

			if (searchNum == 0)//
				goodslist = adminService.goodsSearch0(isSearch);
			else if (searchNum == 1)//
				goodslist = adminService.goodsSearch1(isSearch);
			else if (searchNum == 2)//
				goodslist = adminService.goodsSearch2(isSearch);
			else if (searchNum == 3)// goods_best
				goodslist = adminService.goodsSearch3(isSearch);

			totalCount = goodslist.size();
			page = new Paging(currentPage, totalCount, blockCount, blockPage, "adminGoodsList", searchNum, isSearch);
			pagingHtml = page.getPagingHtml().toString();

			int lastCount = totalCount;

			if (page.getEndCount() < totalCount)
				lastCount = page.getEndCount() + 1;

			goodslist = goodslist.subList(page.getStartCount(), lastCount);

			model.addAttribute("isSearch", isSearch);
			model.addAttribute("searchNum", searchNum);
			model.addAttribute("totalCount", totalCount);
			model.addAttribute("pagingHtml", pagingHtml);
			model.addAttribute("currentPage", currentPage);
			model.addAttribute("goodsList", goodslist);
			return "adminGoodsList";
		}

		totalCount = goodslist.size();

		page = new Paging(currentPage, totalCount, blockCount, blockPage, "adminGoodsList");
		pagingHtml = page.getPagingHtml().toString();

		int lastCount = totalCount;

		if (page.getEndCount() < totalCount)
			lastCount = page.getEndCount() + 1;

		goodslist = goodslist.subList(page.getStartCount(), lastCount);

		model.addAttribute("totalCount", totalCount);
		model.addAttribute("pagingHtml", pagingHtml);
		model.addAttribute("currentPage", currentPage);

		model.addAttribute("goodsList", goodslist);

		return "adminGoodsList";
	}

	// 관리자전용 상품등록 폼
	@RequestMapping("adminGoodsWriteForm")
	public String adminGoodsWriteForm(Model model) {

		model.addAttribute("goods", new GoodsModel());
		return "adminGoodsWriteForm";
	}

	// 관리자전용 상품등록
	@RequestMapping(value = "adminGoodsWrite")
	public String adminGoodsWrite(MultipartHttpServletRequest multipartHttpServletRequest, GoodsModel GoodsModel,
			HttpSession session) throws Exception {

		System.out.println("UPLOAD_PATH : " + uploadPath);

		// image1 로직
		MultipartFile multipartFile = multipartHttpServletRequest.getFile("file1");
		String filename = multipartFile.getOriginalFilename();
		if (filename != "") {
			GoodsModel.setGoods_image1(System.currentTimeMillis() + "_" + multipartFile.getOriginalFilename());
			String savimagename = System.currentTimeMillis() + "_" + multipartFile.getOriginalFilename();
			FileCopyUtils.copy(multipartFile.getInputStream(), new FileOutputStream(uploadPath + "/" + savimagename));
		} else {
			GoodsModel.setGoods_image1("NULL");
		}

		// image2 상세보기 중간
		MultipartFile multipartFile1 = multipartHttpServletRequest.getFile("file2");
		String filename1 = multipartFile1.getOriginalFilename();
		if (filename1 != "") {
			GoodsModel.setGoods_image2(System.currentTimeMillis() + "_content" + multipartFile1.getOriginalFilename());
			String savimagename1 = System.currentTimeMillis() + "_content" + multipartFile1.getOriginalFilename();
			FileCopyUtils.copy(multipartFile1.getInputStream(), new FileOutputStream(uploadPath + "/" + savimagename1));
		} else {
			GoodsModel.setGoods_image2("NULL");
		}

		// image3 상세보기 마지막
		MultipartFile multipartFile2 = multipartHttpServletRequest.getFile("file3");
		String filename2 = multipartFile2.getOriginalFilename();
		if (filename2 != "") {
			GoodsModel.setGoods_image3(System.currentTimeMillis() + "_delev" + multipartFile2.getOriginalFilename());
			String savimagename2 = System.currentTimeMillis() + "_delev" + multipartFile2.getOriginalFilename();
			FileCopyUtils.copy(multipartFile2.getInputStream(), new FileOutputStream(uploadPath + "/" + savimagename2));
		} else {
			GoodsModel.setGoods_image3("NULL");
		}

		GoodsModel.setGoods_seller(session.getAttribute("session_id").toString());

		adminService.insertGoods(GoodsModel);
		return "redirect:adminGoodsList";
	}

	// 관리자 상품 수정폼
	@RequestMapping("adminGoodsModifyForm")
	public String adminGoodsModifyForm(Model model, HttpServletRequest request) {
		
		int goods_num = Integer.parseInt(request.getParameter("goods_num"));

		GoodsModel goodsModel = adminService.goodsAdminView(goods_num);
		
		model.addAttribute("goodsModel", goodsModel);
		return "adminGoodsModifyForm";
	}


	
	
	
	
	// 관리자 상품 수정
	@RequestMapping("adminGoodsModify")
	public String adminGoodsModify(Model model, HttpServletRequest request, GoodsModel GoodsModel,
			MultipartHttpServletRequest multipartHttpServletRequest) {

		System.out.println("UPLOAD_PATH : " + uploadPath);

		String goods_old_img1 = request.getParameter("goods_old_img1");
		String goods_old_img2 = request.getParameter("goods_old_img2");
		String goods_old_img3 = request.getParameter("goods_old_img3");
		
		// image1
		if (multipartHttpServletRequest.getFile("file1") != null) {

			MultipartFile multipartFile = multipartHttpServletRequest.getFile("file1");
			String filename = multipartFile.getOriginalFilename();
			if (filename != "") {
				GoodsModel.setGoods_image1(System.currentTimeMillis() + "_" + multipartFile.getOriginalFilename());
				String savimagename = System.currentTimeMillis() + "_" + multipartFile.getOriginalFilename();
				try {
					FileCopyUtils.copy(multipartFile.getInputStream(),
							new FileOutputStream(uploadPath + "/" + savimagename));
					File goodsOldFile1 = new File(uploadPath + goods_old_img1);
					goodsOldFile1.delete();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} else {
			GoodsModel.setGoods_image1(multipartHttpServletRequest.getParameter("goods_image1"));
		}

		// image2
		if (multipartHttpServletRequest.getFile("file2") != null) {

			MultipartFile multipartFile1 = multipartHttpServletRequest.getFile("file2");
			String filename1 = multipartFile1.getOriginalFilename();
			if (filename1 != "") {
				GoodsModel.setGoods_image2(
						System.currentTimeMillis() + "_content" + multipartFile1.getOriginalFilename());
				String savimagename1 = System.currentTimeMillis() + "_content" + multipartFile1.getOriginalFilename();
				try {
					FileCopyUtils.copy(multipartFile1.getInputStream(),
							new FileOutputStream(uploadPath + "/" + savimagename1));
					File goodsOldFile2 = new File(uploadPath + goods_old_img2);
					goodsOldFile2.delete();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} else {
			GoodsModel.setGoods_image2(multipartHttpServletRequest.getParameter("goods_image2"));
		}

		// image3
		if (multipartHttpServletRequest.getFile("file3") != null) {

			MultipartFile multipartFile2 = multipartHttpServletRequest.getFile("file3");
			String filename2 = multipartFile2.getOriginalFilename();
			if (filename2 != "") {
				GoodsModel
						.setGoods_image3(System.currentTimeMillis() + "_delev" + multipartFile2.getOriginalFilename());
				String savimagename2 = System.currentTimeMillis() + "_delev" + multipartFile2.getOriginalFilename();
				try {
					FileCopyUtils.copy(multipartFile2.getInputStream(),
							new FileOutputStream(uploadPath + "/" + savimagename2));
					File goodsOldFile3 = new File(uploadPath + goods_old_img3);
					goodsOldFile3.delete();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} else {
			GoodsModel.setGoods_image3(multipartHttpServletRequest.getParameter("goods_image3"));
		}

		adminService.goodsModify(GoodsModel);
		model.addAttribute("goodsModel", GoodsModel);
		System.out.println("GoodsModel : " + GoodsModel);
		return "redirect:adminGoodsList";
	}

	// 관리자 상품삭제
	@RequestMapping("adminGoodsDelete")
	public String adminGoodsDelete(Model model, HttpServletRequest request, GoodsModel GoodsModel) {
		int goods_num = Integer.parseInt(request.getParameter("goods_num"));
		GoodsModel = goodsService.goodsView(goods_num);

		// 이미지 1,2,3까지 같이 삭제되게?
		String filename = GoodsModel.getGoods_image1();
		String filename1 = GoodsModel.getGoods_image2();
		String filename2 = GoodsModel.getGoods_image3();
		System.out.println(filename);

		File f = new File(uploadPath + filename);
		System.out.println(f.isFile());
		if (f.exists()) {
			f.delete();
			System.out.println("goods_image1");
		} else {
			System.out.println("goods_image1");
		}

		f = new File(uploadPath + filename1);
		if (f.exists()) {
			f.delete();
			System.out.println("getGoods_image2");
		} else {
			System.out.println("getGoods_image2");
		}

		f = new File(uploadPath + filename2);
		if (f.exists()) {
			f.delete();
			System.out.println("getGoods_image3");
		} else {
			System.out.println("getGoods_image3");
		}

		adminService.goodsDelete(goods_num);
		return "redirect:adminGoodsList";
	}

	// 상품코멘트(GoodsComment) 전체리스트
	@RequestMapping("adminCommentList")
	public String adminCommentList(Model model) throws Exception {

		List<GoodsCommentModel> adminCommentList = adminService.adminCommentList();
		model.addAttribute("adminCommentList", adminCommentList);
		return "adminCommentList";
	}
	
	// 상품코멘트(GoodsComment) 삭제
	@RequestMapping("adminCommentDelete")
	public String adminCommentDelete(HttpServletRequest request) {

		int comment_num = Integer.parseInt(request.getParameter("comment_num"));
		
		goodsService.goodsCommentDelete(comment_num);

		return "redirect:adminCommentList";
	}

	// 일반회원(Member) 리스트
	@RequestMapping("adminMemberList")
	public String adminMemberList(Model model, HttpServletRequest request) throws Exception {

		List<MemberModel> memberlist = adminService.memberList();
		model.addAttribute("memberlist", memberlist);
		return "adminMemberList";
	}

	// 관리자 회원 삭제
	@RequestMapping("adminMemberDelete")
	public String adminMemberDelete(Model model, HttpServletRequest request) {
		String id = request.getParameter("id");
		adminService.memberDelete(id);

		return "redirect:adminMemberList";
	}

	// 관리자 회원수정 폼
	@RequestMapping("adminMemberModifyForm")
	public String adminMemberModifyForm(Model model, MemberModel member) {

		member = memberService.getMember(member.getId());

		model.addAttribute("member", member);

		return "adminMemberModifyForm";
	}

	// 관리자 회원 수정
	@RequestMapping("adminMemberModify")
	public String adminMemberModify(Model model, MemberModel member, HttpServletRequest request) {

		String id = request.getParameter("id");
		member.setId(id);
		adminService.adminmemberModify(member);

		return "redirect:adminMemberList";
	}

	// 관리자 회원 분포도
	@RequestMapping("adminMemberMap")
	public String adminMemberMap(Model model) {

		List<MemberModel> memberaddress = adminService.addressList();

		model.addAttribute("memberaddress", memberaddress);
		System.out.println("회원주소리스트 = " + memberaddress);

		return "adminMemberMap";
	}

	// 판매자회원(Seller) 리스트
	@RequestMapping("adminSellerList")
	public String adminSellerList(Model model, HttpServletRequest request) throws Exception {

		List<MemberModel> memberlist = adminService.sellerList();
		model.addAttribute("memberlist", memberlist);
		return "adminSellerList";
	}

	// 주문리스트
	@RequestMapping("adminOrderList")
	public String OrderList(Model model) throws Exception {

		List<OrderModel> adminOrderList = adminService.orderAllList();

		model.addAttribute("adminOrderList", adminOrderList);

		return "adminOrderList";
	}

	 //주문 수정하기 폼
	 @RequestMapping("adminOrderModifyForm")
	 public String orderModifyForm(Model model, HttpServletRequest request, OrderModel orderModel){
	
	 orderModel = orderService.OrdergetOne(orderModel.getOrder_num());
	
	 model.addAttribute("orderModel", orderModel);
	
	 return "adminOrderModifyForm";
	 }
	 
	 // 주문수정
	 @RequestMapping("adminOrderModify")
	 public String adminOrderModify(Model model, OrderModel orderModel, HttpServletRequest request) {
		 
		 int order_num = Integer.parseInt(request.getParameter("order_num"));
		 orderModel.setOrder_num(order_num);
		 
		 orderService.OrderModify(orderModel);
		 
	 return "redirect:adminOrderList";
	 }

	 // 주문삭제
	 @RequestMapping("adminOrderDelete")
	 public String adminOrderDelete(Model model, OrderModel orderModel, HttpServletRequest request) {
		 
		 int order_num = Integer.parseInt(request.getParameter("order_num"));
		 adminService.adminOrderDelete(order_num);
		 
	 return "redirect:adminOrderList";
	 }

	// 관리자 공지사항 리스트(완료)
	@RequestMapping("adminNoticeList")
	public String adminNoticeList(Model model, HttpServletRequest request) {

		if (request.getParameter("currentPage") == null || request.getParameter("currentPage").trim().isEmpty()
				|| request.getParameter("currentPage").equals("0")) {
			currentPage = 1;
		} else {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}

		List<NoticeModel> noticeList;

		isSearch = request.getParameter("isSearch");
		if (isSearch != null) {
			searchNum = Integer.parseInt(request.getParameter("searchNum"));

			if (searchNum == 0)
				noticeList = noticeService.noticeSearch0(isSearch);
			else
				noticeList = noticeService.noticeSearch1(isSearch);

			totalCount = noticeList.size();
			page = new Paging(currentPage, totalCount, blockCount, blockPage, "adminNoticeList", searchNum, isSearch);
			pagingHtml = page.getPagingHtml().toString();

			int lastCount = totalCount;

			if (page.getEndCount() < totalCount)
				lastCount = page.getEndCount() + 1;

			noticeList = noticeList.subList(page.getStartCount(), lastCount);

			model.addAttribute("isSearch", isSearch);
			model.addAttribute("searchNum", searchNum);
			model.addAttribute("totalCount", totalCount);
			model.addAttribute("pagingHtml", pagingHtml);
			model.addAttribute("currentPage", currentPage);
			model.addAttribute("noticeList", noticeList);
			return "adminNoticeList";
		}

		noticeList = noticeService.noticeList();

		totalCount = noticeList.size();

		page = new Paging(currentPage, totalCount, blockCount, blockPage, "adminnoticeList");
		pagingHtml = page.getPagingHtml().toString();

		int lastCount = totalCount;

		if (page.getEndCount() < totalCount)
			lastCount = page.getEndCount() + 1;

		noticeList = noticeList.subList(page.getStartCount(), lastCount);

		model.addAttribute("totalCount", totalCount);
		model.addAttribute("pagingHtml", pagingHtml);
		model.addAttribute("currentPage", currentPage);
		model.addAttribute("noticeList", noticeList);
		return "adminNoticeList";
	}

	// 관리자 공지사항 글쓰기폼(완료)
	@RequestMapping(value = "adminNoticeWriteForm")
	public String adminNoticeWriteForm(Model model, HttpServletRequest request) {
		model.addAttribute("noticeModel", new NoticeModel());
		return "adminNoticeWriteForm";
	}

	// 관리자 공지사항 글쓰기(완료)
	@RequestMapping(value = "adminNoticeWrite")
	public String adminNoticeWrite(@ModelAttribute("noticeModel") NoticeModel noticeModel, Model model,
			BindingResult result, HttpServletRequest request, HttpSession session) {

		if (result.hasErrors()) {
			return "redirect:adminNoticeWriteForm";
		}
		String content = noticeModel.getContent().replaceAll("\r\n", "<br />");
		noticeModel.setContent(content);
		noticeService.noticeWrite(noticeModel);
		model.addAttribute("noticeModel", noticeModel);

		return "redirect:adminNoticeList";

	}

	// 관리자 공지사항 수정하기 폼(완료)
	@RequestMapping("adminNoticeModifyForm")
	public String adminNoticeModifyForm(@ModelAttribute("noticeModel") NoticeModel noticeModel, Model model,
			BindingResult result, HttpServletRequest request) {

		noticeModel = noticeService.noticeView(noticeModel.getNotice_num());
		String content = noticeModel.getContent().replaceAll("<br />", "\r\n");
		noticeModel.setContent(content);
		model.addAttribute("noticeModel", noticeModel);

		return "adminNoticeModifyForm";
	}

	// 관리자 공지사항 수정(완료)
	@RequestMapping("adminNoticeModify")
	public String adminNoticeModify(@ModelAttribute("noticeModel") NoticeModel noticeModel, Model model,
			HttpServletRequest request) {

		int notice_num = Integer.parseInt(request.getParameter("notice_num"));
		noticeModel.setNotice_num(notice_num);
		String content = noticeModel.getContent().replaceAll("\r\n", "<br />");
		noticeModel.setContent(content);
		noticeService.noticeModify(noticeModel);

		return "redirect:adminNoticeList";

	}

	// 관리자 공지사항 삭제(완료)
	@RequestMapping("adminNoticeDelete")
	public String adminNoticeDelete(HttpServletRequest request) {

		int notice_num = Integer.parseInt(request.getParameter("notice_num"));
		noticeService.noticeDelete(notice_num);

		return "redirect:adminNoticeList";
	}

	// 관리자 Qna 리스트(완료)
	@RequestMapping(value = "adminQnAList")
	public String qnaList(Model model) {

		List<QnAModel> adminQnAList = qnaService.qnaListAdmin();

		model.addAttribute("adminQnAList", adminQnAList);

		return "adminQnAList";
	}

	// 관리자 QnA 삭제
	@RequestMapping("adminQnADelete")
	public String adminQnADelete(HttpServletRequest request) {

		int qna_num = Integer.parseInt(request.getParameter("qna_num"));

		qnaService.qnaDelete(qna_num);

		return "redirect:adminQnAList";
	}

	/* 관리자펜션리스트 */
	@RequestMapping(value = "adminPensionList")
	public String adminPesionList(Model model, HttpServletRequest request) {
		String search = request.getParameter("search");
		List<PensionModel> adminPensionList = pensionService.adminPensionList(search);
		model.addAttribute("adminPensionList", adminPensionList);
		return "adminPensionList";
	}

	/* 관리자펜션등록폼 */
	@RequestMapping(value = "adminPensionWriteForm")
	public String adminPensionWriteForm() {
		return "adminPensionWriteForm";
	}

	/* 관리자펜션등록 */
	@RequestMapping(value = "adminPensionWrite")
	public String adminPensionWrite(MultipartHttpServletRequest multipartHttpServletRequest, PensionModel pensionModel)
			throws FileNotFoundException, IOException {

		MultipartFile multipartFile = multipartHttpServletRequest.getFile("pen_img1");
		String filename = multipartFile.getOriginalFilename();
		if (filename != "") {
			pensionModel.setPen_image1(System.currentTimeMillis() + "_" + multipartFile.getOriginalFilename());
			String savimagename = System.currentTimeMillis() + "_" + multipartFile.getOriginalFilename();
			FileCopyUtils.copy(multipartFile.getInputStream(),
					new FileOutputStream(pensionuploadPath + "/" + savimagename));
		} else {
			pensionModel.setPen_image1("NULL");
		}

		MultipartFile multipartFile1 = multipartHttpServletRequest.getFile("pen_img2");
		String filename1 = multipartFile1.getOriginalFilename();
		if (filename1 != "") {
			pensionModel.setPen_image2(System.currentTimeMillis() + "_content" + multipartFile1.getOriginalFilename());
			String savimagename1 = System.currentTimeMillis() + "_content" + multipartFile1.getOriginalFilename();
			FileCopyUtils.copy(multipartFile1.getInputStream(),
					new FileOutputStream(pensionuploadPath + "/" + savimagename1));
		} else {
			pensionModel.setPen_image2("NULL");
		}

		pensionService.adminPensionWrite(pensionModel);

		return "redirect:adminPensionList";
	}

	/* 펜션 수정 */
	@RequestMapping(value = "adminPensionModifyForm")
	public String adminPensionModifyForm(HttpServletRequest request, Model model) {
		String pen_name = request.getParameter("pen_name");
		PensionModel pensionModel = pensionService.pensionView(pen_name);
		model.addAttribute("pension", pensionModel);
		return "adminPensionModifyForm";
	}

	@RequestMapping(value = "adminPensionModify")
	public String adminPensionModifyForm(HttpServletRequest request,
			MultipartHttpServletRequest multipartHttpServletRequest, Model model, PensionModel pensionModel) {
		String old_img1 = request.getParameter("old_img1");
		String old_img2 = request.getParameter("old_img2");
		if (multipartHttpServletRequest.getFile("pen_img1") != null) {
			// 메인 상품이미지
			MultipartFile multipartFile = multipartHttpServletRequest.getFile("pen_img1");
			String filename = multipartFile.getOriginalFilename();
			if (filename != "") {
				pensionModel.setPen_image1(System.currentTimeMillis() + "_" + multipartFile.getOriginalFilename());
				String savimagename = System.currentTimeMillis() + "_" + multipartFile.getOriginalFilename();
				try {
					FileCopyUtils.copy(multipartFile.getInputStream(),
							new FileOutputStream(pensionuploadPath + "/" + savimagename));
					File oldFile1 = new File(pensionuploadPath + old_img1);
					oldFile1.delete();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} else {
			pensionModel.setPen_image1(multipartHttpServletRequest.getParameter("pen_img1"));
		}
		if (multipartHttpServletRequest.getFile("pen_img2") != null) {
			MultipartFile multipartFile1 = multipartHttpServletRequest.getFile("pen_img2");
			String filename1 = multipartFile1.getOriginalFilename();
			if (filename1 != "") {
				pensionModel
						.setPen_image2(System.currentTimeMillis() + "_content" + multipartFile1.getOriginalFilename());
				String savimagename1 = System.currentTimeMillis() + "_content" + multipartFile1.getOriginalFilename();
				try {
					FileCopyUtils.copy(multipartFile1.getInputStream(),
							new FileOutputStream(pensionuploadPath + "/" + savimagename1));
					File oldFile2 = new File(pensionuploadPath + old_img2);
					oldFile2.delete();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} else {
			pensionModel.setPen_image2(multipartHttpServletRequest.getParameter("pen_img2"));
		}
		pensionService.pensionModify(pensionModel);
		return "redirect:adminPensionList";
	}
	/*펜션 삭제*/
	@RequestMapping(value="adminPensionDelete")
	public String adminPensionDelete(Model model, HttpServletRequest request){
		pen_name = request.getParameter("pen_name");
		PensionModel pensionModel = pensionService.pensionView(pen_name);
		File file1 = new File(pensionuploadPath+pensionModel.getPen_image1());
		file1.delete();
		File file2 = new File(pensionuploadPath+pensionModel.getPen_image2());
		file2.delete();
		pensionService.adminPensionDelete(pen_name);
		List<Map<String, Object>> roomMap = roomService.imageDelete(pen_name);
		Iterator<Map<String, Object>> itr = roomMap.iterator();
			while(itr.hasNext()){
				 Map<String, Object> s = itr.next();
					File roomDelete1 = new File(roomuploadPath+s.get("ROOM_IMAGE1"));
					roomDelete1.delete();
					File roomDelete2 = new File(roomuploadPath+s.get("ROOM_IMAGE2"));
					roomDelete2.delete();			
			}
		roomService.roomAllDelete(pen_name);
		return "redirect:adminPensionList";
	}
	/* 관리자방등록폼 */
	@RequestMapping(value = "adminRoomWriteForm")
	public String adminRoomWriteForm(Model model, HttpServletRequest request) {

		String search = request.getParameter("search");
		List<PensionModel> adminPensionList = pensionService.adminPensionList(search);
		model.addAttribute("adminPensionList", adminPensionList);

		return "adminRoomWriteForm";
	}

	/* 관리자방등록폼1 */
	@RequestMapping(value = "adminRoomWriteForm1")
	public String adminRoomWriteForm1(HttpServletRequest request,Model model) {

		pen_name = request.getParameter("pen_name");
		model.addAttribute("pen_name", pen_name);
		return "adminRoomWriteForm1";
	}

	/* 관리자방등록 */
	@RequestMapping(value = "adminRoomWrite")
	public String adminRoomWrite(RoomModel roomModel, MultipartHttpServletRequest multipartHttpServletRequest)
			throws FileNotFoundException, IOException {
		MultipartFile multipartFile = multipartHttpServletRequest.getFile("room_img1");
		String filename = multipartFile.getOriginalFilename();
		if (filename != "") {
			roomModel.setRoom_image1(System.currentTimeMillis() + "_" + multipartFile.getOriginalFilename());
			String savimagename = System.currentTimeMillis() + "_" + multipartFile.getOriginalFilename();
			FileCopyUtils.copy(multipartFile.getInputStream(),
					new FileOutputStream(roomuploadPath + "/" + savimagename));
		} else {
			roomModel.setRoom_image1("NULL");
		}

		MultipartFile multipartFile1 = multipartHttpServletRequest.getFile("room_img2");
		String filename1 = multipartFile1.getOriginalFilename();
		if (filename1 != "") {
			roomModel.setRoom_image2(System.currentTimeMillis() + "_content" + multipartFile1.getOriginalFilename());
			String savimagename1 = System.currentTimeMillis() + "_content" + multipartFile1.getOriginalFilename();
			FileCopyUtils.copy(multipartFile1.getInputStream(),
					new FileOutputStream(roomuploadPath + "/" + savimagename1));
		} else {
			roomModel.setRoom_image2("NULL");
		}

		roomModel.setPen_name(pen_name);

		roomService.adminRoomWrite(roomModel);

		return "redirect:adminRoomList";
	}

	/* 관리자방리스트 */
	@RequestMapping(value = "adminRoomList")
	public String adminRoomList(Model model) {

		List<RoomModel> roomList = roomService.adminRoomList();

		model.addAttribute("adminRoomList", roomList);

		return "adminRoomList";
	}
	
	/*관리자 룸 수정*/
	@RequestMapping(value="adminRoomModifyForm")
	public String adminRoomModifyForm(Model model, RoomModel roomModel){
		roomModel = roomService.roomView(roomModel);
		model.addAttribute("room", roomModel);
		return "adminRoomModifyForm";
	}
	@RequestMapping(value="adminRoomModify")
	public String adminRoomModify(HttpServletRequest request,
			MultipartHttpServletRequest multipartHttpServletRequest, Model model, RoomModel roomModel) {
		String old_img1 = request.getParameter("old_img1");
		String old_img2 = request.getParameter("old_img2");
		if (multipartHttpServletRequest.getFile("room_img1") != null) {
			// 메인 상품이미지
			MultipartFile multipartFile = multipartHttpServletRequest.getFile("room_img1");
			String filename = multipartFile.getOriginalFilename();
			if (filename != "") {
				roomModel.setRoom_image1(System.currentTimeMillis() + "_" + multipartFile.getOriginalFilename());
				String savimagename = System.currentTimeMillis() + "_" + multipartFile.getOriginalFilename();
				try {
					FileCopyUtils.copy(multipartFile.getInputStream(),new FileOutputStream(roomuploadPath + savimagename));
					File oldFile1 = new File(roomuploadPath + old_img1);
					oldFile1.delete();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} else {
			roomModel.setRoom_image1(multipartHttpServletRequest.getParameter("room_img1"));
		}
		if (multipartHttpServletRequest.getFile("room_img2") != null) {
			MultipartFile multipartFile1 = multipartHttpServletRequest.getFile("room_img2");
			String filename1 = multipartFile1.getOriginalFilename();
			if (filename1 != "") {
				roomModel.setRoom_image2(System.currentTimeMillis() + "_content" + multipartFile1.getOriginalFilename());
				String savimagename1 = System.currentTimeMillis() + "_content" + multipartFile1.getOriginalFilename();
				try {
					FileCopyUtils.copy(multipartFile1.getInputStream(),
							new FileOutputStream(roomuploadPath + savimagename1));
					File oldFile2 = new File(roomuploadPath + old_img2);
					oldFile2.delete();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} else {
			roomModel.setRoom_image2(multipartHttpServletRequest.getParameter("room_img2"));
		}
		roomService.roomModify(roomModel);
		return "redirect:adminRoomList";
	}
	
	@RequestMapping(value="adminRoomDelete")
	public String adminRoomDelete(Model model, RoomModel roomModel){
		roomModel = roomService.roomView(roomModel);
		roomService.adminRoomDelete(roomModel);
		File file1 = new File(roomuploadPath+roomModel.getRoom_image1());
		file1.delete();
		File file2 = new File(roomuploadPath+roomModel.getRoom_image2());
		file2.delete();
		
		return "redirect:adminRoomList";
	}
	
	/* 관리자예약리스트 */
	@RequestMapping(value = "adminResList")
	public String adminReservationList(Model model) {

		List<ReservationModel> resList = reservationService.adminResList();

		model.addAttribute("adminResList", resList);

		return "adminResList";
	}
	
	/*관리자 예약수정폼*/
	@RequestMapping(value = "adminResModifyForm")
	public String adminResModifyForm(Model model,ReservationModel resModel){
		int res_num = resModel.getRes_num();
		resModel = reservationService.reservationView(res_num);
		model.addAttribute("reservation", resModel);
		return "adminResModifyForm";
	}
	
	@RequestMapping(value="adminResModify")
	public String adminResModify(Model model, ReservationModel resModel){
		reservationService.adminResModify(resModel);
		return "redirect:adminResList";
	}
	
	@RequestMapping(value="adminResDelete")
	public String adminResDelete(HttpServletRequest request, Model model){
		int res_num = Integer.parseInt(request.getParameter("res_num"));
		reservationService.adminResDelete(res_num);
		return "redirect:adminResList";
	}
	
	
	/* 판매자 추가 폼 */
	@RequestMapping(value = "adminSellerWriteForm")
	public String adminSellerWriteForm() {
		return "adminSellerWriteForm";
	}

	/* 판매자 추가 */
	@RequestMapping(value = "adminSellerWrite")
	public String adminSellerWrite(MemberModel memberModel) {

		memberService.adminSellerWrite(memberModel);

		return "redirect:adminMemberList";

	}
}
/*
 * // 회원정보수정
 * 
 * @RequestMapping(value = "adminMemberModifyForm") public String
 * adminMemberModifyForm(MemberModel member,Model model, HttpSession session) {
 * 
 * id = (String) session.getAttribute("session_id"); member =
 * memberService.getMember(id);
 * 
 * 
 * 
 * return "adminMemberModifyForm";
 * 
 * 
 * 
 * 
 * 
 * }
 * 
 * @RequestMapping(value = "adminMemberModify") public String
 * adminMemberModify(MemberModel member,HttpSession session,Model
 * model,HttpServletRequest request) throws UnsupportedEncodingException{
 * 
 * 
 * 
 * if("session_id" != null){ member.setId(id);
 * memberService.memberModify(member); model.addAttribute("member",member);
 * session.removeAttribute("session_name");
 * 
 * 
 * 
 * 
 * return "adminMemberList"; } else
 * 
 * return "adminMemberModifyForm";
 * 
 * } }
 */