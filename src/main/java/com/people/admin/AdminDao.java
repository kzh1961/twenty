package com.people.admin;

import java.util.List;

import com.people.goods.GoodsCommentModel;
import com.people.goods.GoodsModel;
import com.people.member.MemberModel;
import com.people.order.OrderModel;

public interface AdminDao {
	//관리자 상품리스트
	public List<GoodsModel> goodsList();
	
	//관리자 상품등록
	public Object insertGoods(GoodsModel GoodsModel);
	
	//관리자 서치 이건 필요없을듯
	List<GoodsModel> goodsSearch0(String search);
	List<GoodsModel> goodsSearch1(String search);
	List<GoodsModel> goodsSearch2(String search);
	List<GoodsModel> goodsSearch3(String search);
	
	//관리자 상품삭제
	public int goodsDelete(int goods_num);
	
	//관리자 상품수정
	public int goodsModify(GoodsModel GoodsModel);
	
	//
	public GoodsModel goodsAdminView(int goods_num);
	
	//상품코멘트(GoodsComment) 전체리스트
	public List<GoodsCommentModel> adminCommentList();
	
	//일반회원(Member) 리스트
	public List<MemberModel> memberList();
	
	//판매자회원(Seller) 리스트
	public List<MemberModel> sellerList();
	
	
	//관리자 회원검색 이것도 필요없는듯
	List<MemberModel> memberSearch0(String search);
	
	//관리자 회원 삭제
	public int memberDelete(String id);

	//관리자 회원 수정
	public Object adminmemberModify(MemberModel member);
	
	//관리자 회원 주소 구글맵
	public List<MemberModel> addressList();
	
	//관리자 회원 상품주문목록 전체리스트 가져오기
	public List<OrderModel> orderAllList();
	
	//관리자 주문삭제
	public int adminOrderDelete(int order_num);
}