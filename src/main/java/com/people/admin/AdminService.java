package com.people.admin;

import java.util.List;

import javax.annotation.Resource;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Service;

import com.people.goods.GoodsCommentModel;
import com.people.goods.GoodsModel;
import com.people.member.MemberModel;
import com.people.order.OrderModel;

//수호
@Service
public class AdminService implements AdminDao {
	@Resource(name = "sqlSessionTemplate")
	private SqlSessionTemplate sqlSessionTemplate;

	@Override
	public List<GoodsModel> goodsList() {
		return sqlSessionTemplate.selectList("goods.goodsList");
	}

	@Override
	public Object insertGoods(GoodsModel GoodsModel) {
		return sqlSessionTemplate.insert("goods.insertGoods", GoodsModel);

	}

	//
	@Override
	public List<GoodsModel> goodsSearch0(String search) {
		return sqlSessionTemplate.selectList("goods.goodsSearch0", "%" + search + "%");
	}

	//
	@Override
	public List<GoodsModel> goodsSearch1(String search) {
		return sqlSessionTemplate.selectList("goods.goodsSearch1", "%" + search + "%");
	}

	//
	@Override
	public List<GoodsModel> goodsSearch2(String search) {
		return sqlSessionTemplate.selectList("goods.goodsSearch2", "%" + search + "%");
	}

	@Override
	public List<GoodsModel> goodsSearch3(String search) {
		return sqlSessionTemplate.selectList("goods.goodsSearch3", "%" + search + "%");
	}

	//
	@Override
	public int goodsDelete(int goods_num) {
		return sqlSessionTemplate.delete("goods.goodsDelete", goods_num);
	}

	//
	@Override
	public int goodsModify(GoodsModel GoodsModel) {
		return sqlSessionTemplate.update("goods.goodsModify", GoodsModel);
	}

	//
	@Override
	public GoodsModel goodsAdminView(int goods_num) {
		return sqlSessionTemplate.selectOne("goods.goodsSelectOne", goods_num);
	}

	// 상품코멘트(GoodsComment) 전체리스트
	@Override
	public List<GoodsCommentModel> adminCommentList() {
		return sqlSessionTemplate.selectList("goods.adminCommentList");
	}

	// 일반회원(Member) 리스트
	@Override
	public List<MemberModel> memberList() {
		return sqlSessionTemplate.selectList("member.memberList");
	}

	// 판매자회원(Seller) 리스트
	@Override
	public List<MemberModel> sellerList() {
		return sqlSessionTemplate.selectList("member.sellerList");
	}

	@Override
	public List<MemberModel> addressList() {
		// TODO Auto-generated method stub
		return sqlSessionTemplate.selectList("member.addressList");
	}

	//
	@Override
	public List<MemberModel> memberSearch0(String search) {
		return sqlSessionTemplate.selectList("member.memberSearch0", "%" + search + "%");
	}

	//
	@Override
	public int memberDelete(String id) {
		return sqlSessionTemplate.delete("member.deleteMember", id);
	}

	//
	@Override
	public Object adminmemberModify(MemberModel member) {
		return sqlSessionTemplate.update("member.adminupdateMember", member);
	}

	@Override
	public List<OrderModel> orderAllList() {
		return sqlSessionTemplate.selectList("order.orderAllList");
	}

	// 관리자 주문삭제
	@Override
	public int adminOrderDelete(int order_num) {
		return sqlSessionTemplate.delete("order.adminOrderDelete", order_num);
	}
}