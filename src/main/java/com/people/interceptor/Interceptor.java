package com.people.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class Interceptor extends HandlerInterceptorAdapter {
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
		try {

			if (request.getSession().getAttribute("session_id") == null) {

				response.sendRedirect("/twenty/loginForm");
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return true;
	}
}