package com.people.seller;

import java.util.List;

import javax.annotation.Resource;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Service;

import com.people.goods.GoodsModel;

@Service
public class SellerService implements SellerDao {

	@Resource(name = "sqlSessionTemplate")
	private SqlSessionTemplate sqlSessionTemplate;

	@Override
	public List<GoodsModel> sellerGoodsList(String id) {
		return sqlSessionTemplate.selectList("goods.sellerGoodsList", id);
	}

	@Override
	public Object sellerGoodsWrite(GoodsModel goodsModel) {
		return sqlSessionTemplate.insert("goods.insertGoods", goodsModel);
	}

	@Override
	public int SellerGoodsDelete(int goods_num) {
		return sqlSessionTemplate.delete("goods.goodsDelete", goods_num);
	}
}