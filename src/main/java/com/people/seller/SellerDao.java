package com.people.seller;

import java.util.List;

import com.people.goods.GoodsModel;

public interface SellerDao {

	public List<GoodsModel> sellerGoodsList(String id);

	public Object sellerGoodsWrite(GoodsModel goodsModel);

	//판매자 상품삭제
	public int SellerGoodsDelete(int goods_num);
}