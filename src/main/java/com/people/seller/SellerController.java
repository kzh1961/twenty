package com.people.seller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.people.goods.GoodsModel;
import com.people.goods.GoodsService;
import com.people.admin.AdminService;

@Controller
public class SellerController {
	
	@Resource
	private GoodsService goodsService;
	
	@Resource
	private AdminService adminService;
	
	@Resource
	private SellerService sellerService;
	
	/*String uploadPath = "C:\\Users\\user2\\Desktop\\twenty\\src\\main\\webapp\\resources\\goods_images\\";*/
	String uploadPath = "C:\\Users\\user2\\Desktop\\twenty\\src\\main\\webapp\\resources\\goods_images\\";
	/*String uploadPath = "C:\\Users\\user2\\workspace\\twenty\\src\\main\\webapp\\resources\\goods_images\\";*/
	
	//판매자 관리자 폼
	@RequestMapping(value = "sellerForm")
	public String sellerForm() {
		return "sellerForm";
	}
	
	//판매자 상품리스트
	@RequestMapping(value = "sellerGoodsList")
	public String sellerGoodsList(Model model, HttpServletRequest request, HttpSession session) {
		
		String id = session.getAttribute("session_id").toString();
		List<GoodsModel> goodsList = sellerService.sellerGoodsList(id);
		
		model.addAttribute("goodsList", goodsList);
		
		return "sellerGoodsList";
	}
	
	//판매자 상품 등록 폼
	@RequestMapping(value = "sellerGoodsWriteForm")
	public String sellerGoodsWriteForm() {
		return "sellerGoodsWriteForm";
	}
	
	
	
	//판매자 상품 등록
	@RequestMapping(value = "sellerGoodsWrite")
	public String sellerGoodsWrite(GoodsModel goodsModel, MultipartHttpServletRequest multipartHttpServletRequest, HttpSession session) throws FileNotFoundException, IOException {
		
		// image1
		MultipartFile multipartFile = multipartHttpServletRequest.getFile("file1");
		String filename = multipartFile.getOriginalFilename();
		if (filename != "") {
			goodsModel.setGoods_image1(System.currentTimeMillis() + "_" + multipartFile.getOriginalFilename());
			String savimagename = System.currentTimeMillis() + "_" + multipartFile.getOriginalFilename();
			FileCopyUtils.copy(multipartFile.getInputStream(), new FileOutputStream(uploadPath + "/" + savimagename));
		} else {
			goodsModel.setGoods_image1("NULL");
		}

		// image2
		MultipartFile multipartFile1 = multipartHttpServletRequest.getFile("file2");
		String filename1 = multipartFile1.getOriginalFilename();
		if (filename1 != "") {
			goodsModel.setGoods_image2(System.currentTimeMillis() + "_content" + multipartFile1.getOriginalFilename());
			String savimagename1 = System.currentTimeMillis() + "_content" + multipartFile1.getOriginalFilename();
			FileCopyUtils.copy(multipartFile1.getInputStream(), new FileOutputStream(uploadPath + "/" + savimagename1));
		} else {
			goodsModel.setGoods_image2("NULL");
		}

		// image3
		MultipartFile multipartFile2 = multipartHttpServletRequest.getFile("file3");
		String filename2 = multipartFile2.getOriginalFilename();
		if (filename2 != "") {
			goodsModel.setGoods_image3(System.currentTimeMillis() + "_delev" + multipartFile2.getOriginalFilename());
			String savimagename2 = System.currentTimeMillis() + "_delev" + multipartFile2.getOriginalFilename();
			FileCopyUtils.copy(multipartFile2.getInputStream(), new FileOutputStream(uploadPath + "/" + savimagename2));
		} else {
			goodsModel.setGoods_image3("NULL");
		}
		
		goodsModel.setGoods_seller(session.getAttribute("session_id").toString());
		
		sellerService.sellerGoodsWrite(goodsModel);
		
		return "redirect:sellerGoodsList";
		
		
	}
	
	// 판매자 상품 수정폼
	@RequestMapping("sellerGoodsModifyForm")
	public String sellerGoodsModifyForm(Model model, HttpServletRequest request) {
		
		int goods_num = Integer.parseInt(request.getParameter("goods_num"));

		GoodsModel goodsModel = adminService.goodsAdminView(goods_num);
		
		model.addAttribute("goodsModel", goodsModel);
		return "sellerGoodsModifyForm";
	}
	
	// 판매자 상품 수정
	@RequestMapping("sellerGoodsModify")
	public String sellerGoodsModify(Model model, HttpServletRequest request, GoodsModel GoodsModel,
			MultipartHttpServletRequest multipartHttpServletRequest) {

		System.out.println("UPLOAD_PATH : " + uploadPath);

		String goods_old_img1 = request.getParameter("goods_old_img1");
		String goods_old_img2 = request.getParameter("goods_old_img2");
		String goods_old_img3 = request.getParameter("goods_old_img3");
		
		// image1
		if (multipartHttpServletRequest.getFile("file1") != null) {

			MultipartFile multipartFile = multipartHttpServletRequest.getFile("file1");
			String filename = multipartFile.getOriginalFilename();
			if (filename != "") {
				GoodsModel.setGoods_image1(System.currentTimeMillis() + "_" + multipartFile.getOriginalFilename());
				String savimagename = System.currentTimeMillis() + "_" + multipartFile.getOriginalFilename();
				try {
					FileCopyUtils.copy(multipartFile.getInputStream(),
							new FileOutputStream(uploadPath + "/" + savimagename));
					File goodsOldFile1 = new File(uploadPath + goods_old_img1);
					goodsOldFile1.delete();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} else {
			GoodsModel.setGoods_image1(multipartHttpServletRequest.getParameter("goods_image1"));
		}

		// image2
		if (multipartHttpServletRequest.getFile("file2") != null) {

			MultipartFile multipartFile1 = multipartHttpServletRequest.getFile("file2");
			String filename1 = multipartFile1.getOriginalFilename();
			if (filename1 != "") {
				GoodsModel.setGoods_image2(
						System.currentTimeMillis() + "_content" + multipartFile1.getOriginalFilename());
				String savimagename1 = System.currentTimeMillis() + "_content" + multipartFile1.getOriginalFilename();
				try {
					FileCopyUtils.copy(multipartFile1.getInputStream(),
							new FileOutputStream(uploadPath + "/" + savimagename1));
					File goodsOldFile2 = new File(uploadPath + goods_old_img2);
					goodsOldFile2.delete();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} else {
			GoodsModel.setGoods_image2(multipartHttpServletRequest.getParameter("goods_image2"));
		}

		// image3
		if (multipartHttpServletRequest.getFile("file3") != null) {

			MultipartFile multipartFile2 = multipartHttpServletRequest.getFile("file3");
			String filename2 = multipartFile2.getOriginalFilename();
			if (filename2 != "") {
				GoodsModel
						.setGoods_image3(System.currentTimeMillis() + "_delev" + multipartFile2.getOriginalFilename());
				String savimagename2 = System.currentTimeMillis() + "_delev" + multipartFile2.getOriginalFilename();
				try {
					FileCopyUtils.copy(multipartFile2.getInputStream(),
							new FileOutputStream(uploadPath + "/" + savimagename2));
					File goodsOldFile3 = new File(uploadPath + goods_old_img3);
					goodsOldFile3.delete();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} else {
			GoodsModel.setGoods_image3(multipartHttpServletRequest.getParameter("goods_image3"));
		}

		adminService.goodsModify(GoodsModel);
		model.addAttribute("goodsModel", GoodsModel);
		System.out.println("GoodsModel : " + GoodsModel);
		return "redirect:sellerGoodsList";
	}
	
	// 판매자 상품삭제
		@RequestMapping("sellerGoodsDelete")
		public String sellerGoodsDelete(Model model, HttpServletRequest request, GoodsModel GoodsModel) {
			
			int goods_num = Integer.parseInt(request.getParameter("goods_num"));
			GoodsModel = goodsService.goodsView(goods_num);

			// 이미지 1,2,3까지 같이 삭제되게?
			String filename = GoodsModel.getGoods_image1();
			String filename1 = GoodsModel.getGoods_image2();
			String filename2 = GoodsModel.getGoods_image3();
			System.out.println(filename);

			File f = new File(uploadPath + filename);
			System.out.println(f.isFile());
			if (f.exists()) {
				f.delete();
				System.out.println("goods_image1");
			} else {
				System.out.println("goods_image1");
			}

			f = new File(uploadPath + filename1);
			if (f.exists()) {
				f.delete();
				System.out.println("getGoods_image2");
			} else {
				System.out.println("getGoods_image2");
			}

			f = new File(uploadPath + filename2);
			if (f.exists()) {
				f.delete();
				System.out.println("getGoods_image3");
			} else {
				System.out.println("getGoods_image3");
			}

			sellerService.SellerGoodsDelete(goods_num);
			return "redirect:sellerGoodsList";
		}
}
