package com.people.member;

import java.util.List;

import javax.annotation.Resource;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Service;



@Service
public class MemberService implements MemberDao{

	@Resource(name="sqlSessionTemplate")
	private SqlSessionTemplate sqlSessionTemplate;
	
	//로그인
	@Override
    public MemberModel memberLogin(MemberModel mem) {
	    return sqlSessionTemplate.selectOne("member.login", mem);
    }
    //아이디 한개 꺼내오기        
	@Override
	public MemberModel getMember(String id) {
		return sqlSessionTemplate.selectOne("member.getMember", id);
	}
    //회원가입
	@Override
	public Object insertMember(MemberModel mem) {
		return sqlSessionTemplate.insert("member.insertMember", mem);
	}
	//이름&이메일로 아이디 찾기
	@Override
	public MemberModel idFindByName(MemberModel member) {
		return sqlSessionTemplate.selectOne("member.idfind", member);
    }
	//아이디&주민번호로 패스워드 찾기
	@Override
	public MemberModel pwFindById(MemberModel member) {
		return sqlSessionTemplate.selectOne("member.pwfind", member);
    }
    //회원정보수정
	@Override
	public Object memberModify(MemberModel member) {
		return sqlSessionTemplate.update("member.updateMember", member);
	}
	//회원탈퇴
	@Override
	public Object memberDelete(String id) {
		return sqlSessionTemplate.delete("member.deleteMember", id);
    }
	//포인트 갱신
	@Override
	public Object pointUpdate(MemberModel member) {
		
		return sqlSessionTemplate.update("member.pointUpdate",member);
	}
	//판매자 등록
	@Override
	public Object adminSellerWrite(MemberModel memberModel) {
		
		return sqlSessionTemplate.insert("member.adminSellerWrite", memberModel);
		
	}

}
