package com.people.member;

import java.util.List;

public interface MemberDao {

	public MemberModel memberLogin(MemberModel mem);

	public MemberModel getMember(String id);

	public Object insertMember(MemberModel mem);

	public MemberModel idFindByName(MemberModel member);

	public MemberModel pwFindById(MemberModel member);

	public Object memberModify(MemberModel member);

	public Object memberDelete(String id);
	
	public Object pointUpdate(MemberModel member);

	public Object adminSellerWrite(MemberModel memberModel);
	
}