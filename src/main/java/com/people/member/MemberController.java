package com.people.member;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Calendar;
import java.util.Date;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.tiles.jsp.taglib.GetAsStringTag;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.ui.Model;

import com.people.member.MemberService;
import com.people.member.MemberModel;

import com.people.validator.MemberValidator;

import oracle.sql.DATE;
	@Controller
	public class MemberController {
	   
	   @Resource(name="memberService")
	   private MemberService memberService;
	  
	   private String id;
	   //로그인 폼
	   @RequestMapping("/loginForm")
	   public String loginForm() {
	      return "loginForm";
	   }

	   //로그인동작 및 세션 생성
	   @RequestMapping("/login")
	   public String login(HttpServletRequest request, MemberModel mem) {
		   

	      MemberModel result = memberService.memberLogin(mem);
	     
	      if(result!=null) {
	         
	         HttpSession session = request.getSession();         
	         
	         session.setAttribute("session_mem", result);
	         session.setAttribute("session_id", result.getId());
	         session.setAttribute("session_name", result.getName());
	         session.setAttribute("session_admincheck", result.getAdmincheck());
	         session.setAttribute("TOKEN_SAVE_CHECK", "TRUE");
	                 
	   return "redirect:main";
	      }
	      //System.out.println("로그인 실패");         
	      return "member/loginError";
	         
	   }
	   @RequestMapping("/logout")
	   public String logout(HttpServletRequest request, MemberModel mem){
	      
	      HttpSession session = request.getSession(false);
	      
	         session.invalidate();
	         
	         return "redirect:main";
	     
	     
	   }
	   
	   
	   //유효성검사했을때 에러있을시 넘어가게끔 해줘야함
	   //회원가입
	      @ModelAttribute("member")
	      public MemberModel formBack() {
	         return new MemberModel();
	      }
	      
	      @RequestMapping("/memberForm")
	      public String memberForm(){
	    	  
	          return "member";
	    	  
	         /*return "member";*/
	      }
	      @RequestMapping("/memberJoin")
	      public String memberJoin(@ModelAttribute("member") MemberModel member,
	                              BindingResult result, Model model, HttpServletRequest request) throws UnsupportedEncodingException {
	    	  
	    	
	         //<form:form commandName="member"> 검사할 model을 영역에 올려준 이름과 동일하게한다.
	         
	         // Validation Binding
	         new MemberValidator().validate(member, result);
	         
	         // 에러가있으면 회원가입폼으로 넘어감
	         if(result.hasErrors()) {
	        	 
	        	
	        	 
	            return "member";
	         }
	         try {
	        	 
	        	 
	        	 memberService.insertMember(member);
	        	
	            
	            
	            
	            return  "loginForm";
	         } catch (DuplicateKeyException e) {
	            // db에서 id의 제약조건을 unique로 바꿨기 때문에 중복된 아이디로 가입하려하면 DuplicateKeyException이 뜨게되고
	            // 예외처리로 properties파일에 등록된 "invalid"의 내용이 나오게 만들고 회원가입폼으로 돌아가게했음.
	            // 아이디 중복검사
	            result.reject("invalid", null);
	           
	            return "member";
	         }

	      }
	      
	      //아이디 찾기
	      @RequestMapping(value = "/idFindForm")
	    	public String idFindForm() {
	    		return "idFindForm";
	    	}

	    	@RequestMapping(value = "/idFindOk")
	    	public String idFind(@ModelAttribute("member") MemberModel member, HttpServletRequest request,Model model) {

	    		int memberFindChk;

	    		member = memberService.idFindByName(member);
	    		if (member == null) {
	    			memberFindChk = 0;
	    			model.addAttribute("chk",memberFindChk);
	    			return "idFindError";

	    		} else {
	    			if (member.getName().equals(member.getName()) && member.getEmail().equals(member.getEmail())) {
	    				memberFindChk = 1;
	    			
	    				model.addAttribute("member", member);
	    				return "idFindOk";
	    			
	    			} else {
	    			
	    					memberFindChk = -1;
	    				model.addAttribute("chk", memberFindChk);
	    				
	    				return "idFindError";
	    			}
	    		}
	    	}

	    	// 비밀번호찾기
	    	@RequestMapping(value = "/pwFindForm")
	    	public String memberPwFindForm() {
	    	
	    		return "pwFindForm";
	    	}

	    	@RequestMapping(value = "/pwFindOk")
	    	public String pwFind(@ModelAttribute("member") MemberModel member, HttpServletRequest request, Model model) {

	    		int memberPwChk;


	    		member = memberService.pwFindById(member);
	    		
	    		if (member == null) {
	    			memberPwChk = 0;
	    			model.addAttribute("chk", memberPwChk);
	    			
	    			return "pwFindError";

	    		} else {
	    			
	    			if (member.getId().equals(member.getId()) && member.getJumin1().equals(member.getJumin1()) && member.getJumin2().equals(member.getJumin2())) {
	    				memberPwChk = 1;
	    				model.addAttribute("member",member);
	    				return "pwFindOk";
	    			} else {
	    				memberPwChk = -1;
	    				model.addAttribute("chk", memberPwChk);
	    				
	    				return "pwFindError";
	    			}
	    		}
	    	}

	    	// 회원정보수정
	    	@RequestMapping("/memberModify")
	    	public String memberModify(@ModelAttribute("member") MemberModel member, BindingResult result,Model model,
	    			HttpSession session) {
	    		
	    			id = (String) session.getAttribute("session_id");
	    		member = memberService.getMember(id);

	    			
	    			
	    			return "memberModify";
	    	

	    	  			
	    			
	    		
	    	}

	    	@RequestMapping("/memberModifySuccess")
	    	public String memberModifySuccess(@ModelAttribute("member") MemberModel member,HttpSession session,  BindingResult result,Model model,HttpServletRequest request)
	    			throws UnsupportedEncodingException{
	    		// Validation Binding
	    		new MemberValidator().validate(member, result);
	    	
	    	
	    		try {
	    			
			    	  member.setId(id);
		        	 memberService.memberModify(member);
		        	 model.addAttribute("member",member);
		        	 session.removeAttribute("session_name");
	    			
	    			
	    			
	    			
	    			return  "memberModifySuccess";
	    		} catch (DuplicateKeyException e) {
	    			// db에서 id의 제약조건을 unique로 바꿨기 때문에 중복된 아이디로 가입하려하면
	    			// DuplicateKeyException이 뜨게되고
	    			// 예외처리로 properties파일에 등록된 "invalid"의 내용이 나오게 만들고 회원가입폼으로 돌아가게했음.
	    			// 아이디 중복검사
	    			result.reject("invalid", null);
	    			
	    			return "memberModify";
	    		}

	    	}
	     
	       
	     
	     
	          @RequestMapping("/memberDeleteForm")
	       	public String memberDeleteForm() {
	       		
	       		return "deleteForm";
	       	}
	       	
	        //회원탈퇴  
	       	@RequestMapping("/memberDelete")
	       	public String memberDelete(@ModelAttribute("member") MemberModel member, BindingResult result, HttpSession session, HttpServletRequest request,Model model) {
	       		
	       		MemberModel memberModel; // 쿼리 결과 값을 저장할 객체
	       		
	       		String id;
	       		String password;
	       		password = request.getParameter("password");
	       		int deleteCheck;
	       		
	       		//해당 아이디의 정보를 가져온다
	       		id = session.getAttribute("session_id").toString();
	       		memberModel = (MemberModel) memberService.getMember(id);
	       		
	       		
	       		
	       		if(memberModel.getPassword().equals(password)) {
	       			//패스워드가 맞으면
	       			deleteCheck = 1;
	       			//삭제 쿼리 수행
	       			memberService.memberDelete(id);
	       			session.removeAttribute("session_id");
	       			session.removeAttribute("session_name");
	       			model.addAttribute("deleteCheck",deleteCheck);
	       			
	       		    return "deleteResult";
	       		}
	       		else {
	       			deleteCheck = -1; //패스워드가 안맞을때
	       		
	       		
	       		model.addAttribute("deleteCheck", deleteCheck);
	       		
	       		return "deleteResult";
	       		}
	       	}
	}
	       
