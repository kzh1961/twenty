package com.people.pension;

import java.util.List;

import javax.annotation.Resource;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Service;

@Service
public class PensionService implements PensionDAO {

	@Resource(name = "sqlSessionTemplate")
	private SqlSessionTemplate sqlSessionTemplate;

	/* 지역별 펜션 리스트 */
	@Override
	public List<PensionModel> pensionList(String search) {
		return sqlSessionTemplate.selectList("pension.pensionList", "%" + search + "%");
	}
	/*펜션 이름 검색 리스트*/
	@Override
	public List<PensionModel> pensionList2(String search) {
		return sqlSessionTemplate.selectList("pension.pensionList2", "%" + search + "%");
	}

	/* 펜션 상세보기 */
	@Override
	public PensionModel pensionView(String pen_name) {
		return sqlSessionTemplate.selectOne("pension.pensionView", pen_name);
	}
	
	/*코멘트리스트*/
	@Override
	public List<PensionCommentModel> pensionCommentList(String pen_name) {
		return sqlSessionTemplate.selectList("pension.pensionCommentList", pen_name);
	}
	
	/*코멘트달기*/
	@Override
	public Object pensionCommentWrite(PensionCommentModel pensionCommentModel) {
		return sqlSessionTemplate.insert("pension.pensionCommentWrite", pensionCommentModel);
	}
	
	/*코멘트삭제*/
	@Override
	public Object pensionCommentDelete(int comment_num) {
		return sqlSessionTemplate.delete("pension.pensionCommentDelete", comment_num);
	}
	
	/*관리자펜션등록*/
	@Override
	public Object adminPensionWrite(PensionModel pensionModel) {
		return sqlSessionTemplate.insert("pension.adminPensionWrite", pensionModel);
		
	}
	
	/*관리자펜션리스트*/
	@Override
	public List<PensionModel> adminPensionList(String search) {
		return sqlSessionTemplate.selectList("pension.adminPensionList", "%" + search + "%");
	}
	
	/*관리자펜션삭제*/
	@Override
	public Object adminPensionDelete(String pen_name) {
		return sqlSessionTemplate.delete("pension.adminPensionDelete" , pen_name);
	}
	@Override
	public Object pensionModify(PensionModel pensionModel) {
		// TODO Auto-generated method stub
		return sqlSessionTemplate.update("pension.pensionModify", pensionModel);
	}
	@Override
	public int penAvgPoint(String pen_name) {
		return sqlSessionTemplate.update("pension.penAvgPoint", pen_name);
	}
	@Override
	public int countPoint(PensionCommentModel pensionCommentModel) {
		return sqlSessionTemplate.selectOne("pension.countPoint", pensionCommentModel);
	}
}
