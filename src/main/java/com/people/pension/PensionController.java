package com.people.pension;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.people.room.RoomModel;
import com.people.room.RoomService;

@Controller
public class PensionController {

	@Resource(name = "pensionService")
	private PensionService pensionService;

	@Resource(name = "roomService")
	private RoomService roomService;

	/* 지역별 펜션 리스트 */
	@RequestMapping(value = "/pensionList/{search}")
	public String pensionList(@PathVariable String search, HttpServletRequest request, Model model) {

		List<PensionModel> pensionList = pensionService.pensionList(search);

		model.addAttribute("pensionList", pensionList);
		model.addAttribute("search", search);
		
		return "pensionList";
	}

	/* 펜션 상세보기 */
	@RequestMapping(value = "/pensionView")
	public String pensionView(HttpServletRequest request, Model model) throws UnsupportedEncodingException {
		
		String minprice;
		String pen_name = request.getParameter("pen_name");
		
		PensionModel pensionModel = pensionService.pensionView(pen_name);
		List<RoomModel> roomList = roomService.roomList(pen_name);
		minprice = roomService.minprice(pen_name);

		// 각 평점 별 개수
		PensionCommentModel pensionCommentModel = new PensionCommentModel();
		pensionCommentModel.setPen_name(pen_name);
		pensionCommentModel.setStar_num(1);
		int countNum1 = pensionService.countPoint(pensionCommentModel);
		pensionCommentModel.setStar_num(2);
		int countNum2 = pensionService.countPoint(pensionCommentModel);
		pensionCommentModel.setStar_num(3);
		int countNum3 = pensionService.countPoint(pensionCommentModel);
		pensionCommentModel.setStar_num(4);
		int countNum4 = pensionService.countPoint(pensionCommentModel);
		pensionCommentModel.setStar_num(5);
		int countNum5 = pensionService.countPoint(pensionCommentModel);

		model.addAttribute("countNum1", countNum1);
		model.addAttribute("countNum2", countNum2);
		model.addAttribute("countNum3", countNum3);
		model.addAttribute("countNum4", countNum4);
		model.addAttribute("countNum5", countNum5);
		model.addAttribute("minprice", minprice);
		model.addAttribute("pensionModel", pensionModel);
		model.addAttribute("roomList", roomList);
		model.addAttribute("pensionCommentList", pensionService.pensionCommentList(pen_name));

		return "pensionView";

	}

	// 상품평 쓰기
	@RequestMapping("pensionCommentWrite")
	public String pensionCommentWrite(Model model, HttpServletRequest request, HttpSession session,
			PensionCommentModel pensionCommentModel) {

		
		String pen_name = request.getParameter("pen_name");

		String id = session.getAttribute("session_id").toString();
		
		pensionCommentModel.setPen_name(pen_name);
		pensionCommentModel.setId(id);
		
		pensionService.pensionCommentWrite(pensionCommentModel);

		// 구매만족도
		pensionService.penAvgPoint(pen_name);

		return "forward:pensionView";

	}

	/* 코멘트 삭제 */
	@RequestMapping(value = "/pensionCommentDelete")
	public String pensionCommentDelete(Model model, HttpServletRequest request,
			PensionCommentModel pensionCommentModel) {

		int comment_num = Integer.parseInt(request.getParameter("comment_num"));
		String pen_name = request.getParameter("pen_name");

		pensionService.pensionCommentDelete(comment_num);
		pensionService.penAvgPoint(pen_name);

		return "forward:pensionView";
	}

}
