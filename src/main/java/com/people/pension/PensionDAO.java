package com.people.pension;

import java.util.List;

public interface PensionDAO {

	/* 지역별 펜션 리스트 */
	public List<PensionModel> pensionList(String search);

	/* 펜션 상세보기 */
	public PensionModel pensionView(String pen_name);

	/* 상품평 리스트 */
	public List<PensionCommentModel> pensionCommentList(String pen_name);

	/* 상품평 달기 */
	public Object pensionCommentWrite(PensionCommentModel pensionCommentModel);

	/* 상품평 삭제 */
	public Object pensionCommentDelete(int comment_num);

	// 상품평 평균 점수
	public int penAvgPoint(String pen_name);

	// 상품평점 별 개수
	public int countPoint(PensionCommentModel pensionCommentModel);

	/* 관리자 펜션등록 */
	public Object adminPensionWrite(PensionModel pensionModel);

	/* 관리자 펜션 리스트 */
	public List<PensionModel> adminPensionList(String search);

	/* 관리자 펜션 삭제 */
	public Object adminPensionDelete(String pen_name);

	public List<PensionModel> pensionList2(String search);

	public Object pensionModify(PensionModel pensionModel);
}
