package com.people.pension;

public class PensionModel {

	private String pen_name;
	private String pen_code;
	private String pen_phone;
	private String pen_image1;
	private String pen_image2;
	private String content;
	private double avgstar;

	public String getPen_name() {
		return pen_name;
	}

	public void setPen_name(String pen_name) {
		this.pen_name = pen_name;
	}

	public String getPen_code() {
		return pen_code;
	}

	public void setPen_code(String pen_code) {
		this.pen_code = pen_code;
	}

	public String getPen_phone() {
		return pen_phone;
	}

	public void setPen_phone(String pen_phone) {
		this.pen_phone = pen_phone;
	}

	public String getPen_image1() {
		return pen_image1;
	}

	public void setPen_image1(String pen_image1) {
		this.pen_image1 = pen_image1;
	}

	public String getPen_image2() {
		return pen_image2;
	}

	public void setPen_image2(String pen_image2) {
		this.pen_image2 = pen_image2;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public double getAvgstar() {
		return avgstar;
	}

	public void setAvgstar(double avgstar) {
		this.avgstar = avgstar;
	}

}
