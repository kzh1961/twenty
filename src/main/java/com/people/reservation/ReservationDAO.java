package com.people.reservation;

import java.util.List;

public interface ReservationDAO {
	public List<ReservationModel> selectResRoom(ReservationModel reservationModel);
	
	public Object reservation(ReservationModel reservationModel);

	public List<ReservationModel> adminResList();

	public Object adminResDelete(int res_num);
	
	public List<ReservationModel> resList(ReservationModel reservationModel);
	
	public Object reservationView(int res_num);
	
	public Object adminResModify(ReservationModel resModel);
	
	public Object resDelete(int res_num);
}
