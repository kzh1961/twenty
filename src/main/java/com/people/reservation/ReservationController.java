package com.people.reservation;

import java.text.DecimalFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.people.room.RoomModel;
import com.people.room.RoomService;
import com.people.util.Paging;

@Controller
@RequestMapping(value="res")
public class ReservationController {
	
	@Resource(name="roomService")
	private RoomService roomService;
	@Resource(name="reservationService")
	private ReservationService reservationService;
	
	ReservationModel resModel = new ReservationModel();
	RoomModel roomModel = new RoomModel();

	private List<ReservationModel> resRoomList;
	private List<ReservationModel> resList = new ArrayList<ReservationModel>();
	private List<String> expiryDate = new ArrayList<String>();

	
	@RequestMapping(value="/penResDate/{pen_name}/{room_num}")
	public String penResDate(@PathVariable String pen_name, @PathVariable int room_num, Model model){
/*		String pen_name = request.getParameter("pen_name");
		int room_num = Integer.parseInt(request.getParameter("room_num"));*/
		SimpleDateFormat format = new SimpleDateFormat("yyyy-M-d");
		Date expirySt_day;
		Date expiryEnd_day;
		String day;

		resModel.setPen_name(pen_name);
		resModel.setRoom_num(room_num);
		roomModel.setRoom_num(room_num);
		roomModel.setPen_name(pen_name);
		
		resRoomList = reservationService.selectResRoom(resModel);
		roomModel = roomService.roomView(roomModel);
		
		if (resRoomList.size() > 0) {
			for (int i = 0; i < resRoomList.size(); i++) {
				expirySt_day = format.parse(resRoomList.get(i).getSt_day(), new ParsePosition(0));
				expiryEnd_day = format.parse(resRoomList.get(i).getEnd_day(), new ParsePosition(0));
				do{
					day = format.format(expirySt_day);
					expiryDate.add(day);
					expirySt_day = new Date(expirySt_day.getTime() + (long) (1000 * 60 * 60 * 24));
				}while(expirySt_day.before(expiryEnd_day));
		
			}
		}
		model.addAttribute("resRoomList",resRoomList);
		model.addAttribute("room", roomModel);
		model.addAttribute("expiryDate",expiryDate);
		
		return "penResDate";
	}
	@RequestMapping(value="/penResInfo")
	public String penResInfo(HttpServletRequest request, Model model){
		long day;
		String st_day = request.getParameter("st_day");
		String end_day = request.getParameter("end_day");
		String price1 = request.getParameter("price1");
		String price2 = request.getParameter("price2");
		int people = Integer.parseInt(request.getParameter("people"));
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date nowDate = format.parse(st_day, new ParsePosition(0));
		Date expiryDate = format.parse(end_day, new ParsePosition(0));
		
		long time = nowDate.getTime();
		day = (expiryDate.getTime()-nowDate.getTime())/1000/60/60/24;
		if(day==0){
			day=1;
		}
		int k=0;
		for(int i=0;i<=day;i++){
			Calendar cal = Calendar.getInstance();
			nowDate = new java.util.Date((time+(1000*60*60*24*i)));
			cal.setTime(nowDate);
			switch(cal.get(Calendar.DAY_OF_WEEK)){
	        case 7:
	            k=k+1;
	            day=day-1;
	            break ;
			}
		}
		
		String priceArr1[] = price1.split(",");
		price1="";
		for(int i=0;i<priceArr1.length;i++){
			price1 = price1+priceArr1[i];
		}
		String priceArr2[] = price2.split(",");
		price2="";
		for(int i=0;i<priceArr2.length;i++){
			price2 = price2+priceArr2[i];
		}
		int total = Integer.parseInt(price1);
					
		total = (total*(int)day)+Integer.parseInt(price2)*k;;
		DecimalFormat df = new DecimalFormat("#,###");
		price1 = (String)df.format(total);
		
		
		model.addAttribute("room",roomModel);
		model.addAttribute("st_day",st_day);
		model.addAttribute("end_day", end_day);
		model.addAttribute("people", people);
		model.addAttribute("price", price1);
		
		return "penResInfo";
	}
	
	@RequestMapping(value="/reservation")
	public String reservation(Model model,ReservationModel resModel,HttpSession session){
		resModel.setPen_name(roomModel.getPen_name());
		resModel.setId(session.getAttribute("session_id").toString());
		reservationService.reservation(resModel);
		return "redirect:/main";
	}
	
	@RequestMapping(value="resList")
	public String resList(Model model,HttpSession session,HttpServletRequest request){
		String id = (String)session.getAttribute("session_id");	
		resModel.setId(id);
		resList = reservationService.resList(resModel);
		model.addAttribute("resList", resList);

		return "resList";
	}
	
	@RequestMapping(value="resDelete")
	public String resDelete(Model model,HttpServletRequest request){
		int res_num = Integer.parseInt(request.getParameter("res_num"));
		
		reservationService.resDelete(res_num);
		return "forward:resList";
	}
}
