package com.people.reservation;

import java.util.List;

import javax.annotation.Resource;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Service;
@Service
public class ReservationService implements ReservationDAO{
	@Resource(name = "sqlSessionTemplate")
	private SqlSessionTemplate sqlSessionTemplate;
	
	@Override
	public List<ReservationModel> selectResRoom(ReservationModel reservationModel) {
		return sqlSessionTemplate.selectList("reservation.selectResRoom",reservationModel);
	}

	@Override
	public Object reservation(ReservationModel reservationModel) {
		return sqlSessionTemplate.selectList("reservation.reservation",reservationModel);
	}
	
	@Override
	public List<ReservationModel> adminResList() {
		return sqlSessionTemplate.selectList("reservation.adminResList");
	}
	
	@Override
	public Object adminResDelete(int res_num) {
		return sqlSessionTemplate.delete("reservation.adminResDelete", res_num);
	}

	@Override
	public List<ReservationModel> resList(ReservationModel reservationModel) {
		return sqlSessionTemplate.selectList("reservation.resList", reservationModel);
	}

	@Override
	public ReservationModel reservationView(int res_num) {
		return sqlSessionTemplate.selectOne("reservation.resView", res_num);
	}

	@Override
	public Object adminResModify(ReservationModel resModel) {
		return sqlSessionTemplate.update("reservation.adminResModify", resModel);
	}

	@Override
	public Object resDelete(int res_num) {
		return sqlSessionTemplate.delete("reservation.resDelete",res_num);
	}

	
}
