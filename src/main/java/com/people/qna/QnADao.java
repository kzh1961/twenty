package com.people.qna;

import java.util.List;

public interface QnADao {
	// 관리자용 글목록
	List<QnAModel> qnaListAdmin();

	// 글목록
	List<QnAModel> qnaList(String id);

	// 검색 (0=제목, 1=내용)
	List<QnAModel> qnaSearch0(String search);

	List<QnAModel> qnaSearch1(String search);

	// 글쓰기
	int qnaWrite(QnAModel qnaModel);

	// 글 상세보기
	QnAModel qnaView(int num);

	// 글삭제
	int qnaDelete(int num);

	// 글수정
	int qnaModify(QnAModel qnaModel);

	// 관리자 답변보기
	QnACommModel qnaCommView(int qna_num);

	// 관리자 답변쓰기
	int qnaCommWrite(QnACommModel qnaCommModel);

	// 관리자 답변삭제
	int qnaCommDelete(int comment_num);

	// 관리자 답변여부 등록
	int QnAAdminWrite(int qna_num);

	// 관리자 답변여부 삭제
	int QnAAdminDelete(int qna_num);

}
