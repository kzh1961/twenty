package com.people.qna;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.people.member.MemberModel;
import com.people.member.MemberService;
import com.people.order.OrderModel;
import com.people.order.OrderService;
import com.people.util.Paging;

@Controller
public class QnAController {

	@Resource
	private OrderService orderService;
	@Resource
	private MemberService memberService;
	@Resource
	private QnAService qnaService;
	private String isSearch;
	private int searchNum;

	// 페이징을 위한 변수 설정
	private int currentPage = 1;
	private int totalCount;
	private int blockCount = 10;
	private int blockPage = 5;
	private String pagingHtml;
	private Paging page;

	private String id;

	@ModelAttribute("qnaModel")
	public QnAModel formBack() {
		return new QnAModel(); // 객체 생성후 반환
	}

	@ModelAttribute("qnaCommModel")
	public QnACommModel formBack2() {
		return new QnACommModel(); // 객체 생성후 반환
	}

	// 1:1문의 리스트(검색)
	@RequestMapping(value = "/qnaList")
	public String qnaList(HttpServletRequest request, QnAModel qnaModel, Model model, HttpSession session)
			throws UnsupportedEncodingException {

		if (request.getParameter("currentPage") == null || request.getParameter("currentPage").trim().isEmpty()
				|| request.getParameter("currentPage").equals("0")) {
			currentPage = 1;
		} else {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}

		List<QnAModel> qnaList;
		id = (String) session.getAttribute("session_id");

		if (id.equals("admin"))
			qnaList = qnaService.qnaListAdmin();
		else
			qnaList = qnaService.qnaList(id);

		// 검색
		isSearch = request.getParameter("isSearch");

		if (isSearch != null) {
			searchNum = Integer.parseInt(request.getParameter("searchNum"));

			if (searchNum == 0)
				qnaList = qnaService.qnaSearch0(isSearch);
			else
				qnaList = qnaService.qnaSearch1(isSearch);

			totalCount = qnaList.size();
			page = new Paging(currentPage, totalCount, blockCount, blockPage, "qnaList", searchNum, isSearch);
			pagingHtml = page.getPagingHtml().toString();

			int lastCount = totalCount;

			if (page.getEndCount() < totalCount)
				lastCount = page.getEndCount() + 1;

			qnaList = qnaList.subList(page.getStartCount(), lastCount);

			model.addAttribute("isSearch", isSearch);
			model.addAttribute("totalCount", totalCount);
			model.addAttribute("pagingHtml", pagingHtml);
			model.addAttribute("currentPage", currentPage);
			model.addAttribute("qnaList", qnaList);

			return "qnaList";
		}

		totalCount = qnaList.size();
		page = new Paging(currentPage, totalCount, blockCount, blockPage, "qnaList");
		pagingHtml = page.getPagingHtml().toString();
		int lastCount = totalCount;

		if (page.getEndCount() < totalCount)
			lastCount = page.getEndCount() + 1;

		qnaList = qnaList.subList(page.getStartCount(), lastCount);

		model.addAttribute("totalCount", totalCount);
		model.addAttribute("pagingHtml", pagingHtml);
		model.addAttribute("currentPage", currentPage);
		model.addAttribute("qnaList", qnaList);

		return "qnaList";
	}

	// 1:1문의 상세보기
	@RequestMapping(value = "/qnaView")
	public String qnaView(@ModelAttribute("QnA") QnACommModel qnaCommModel, HttpServletRequest request,
			QnAModel qnaModel, Model model) {
		int qna_num = qnaModel.getQna_num();
		qnaModel = qnaService.qnaView(qna_num);

		// 1:1문의 답변 보기
		qnaCommModel = qnaService.qnaCommView(qna_num);
		model.addAttribute("qnaCommModel", qnaCommModel);
		model.addAttribute("qnaModel", qnaModel);

		return "qnaView";
	}

	// 1:1문의 글쓰기폼
	@RequestMapping(value = "/qnaWriteForm")
	public String qnaWriteForm(HttpServletRequest request, QnAModel qnaModel, Model model, HttpSession session) {
		id = (String) session.getAttribute("session_id");
		OrderModel orderModel = new OrderModel();
		orderModel.setOrder_member_id(id);
		MemberModel memberModel = memberService.getMember(id);
		model.addAttribute("memberModel", memberModel);
		List<OrderModel> orderList = orderService.OrderList(orderModel);
		model.addAttribute("orderList", orderList);

		return "qnaWriteForm";
	}

	// 1:1문의 글쓰기
	@RequestMapping(value = "/qnaWrite")
	public String qnaWrite(@ModelAttribute("qnaModel") QnAModel qnaModel, HttpServletRequest request,
			HttpSession session, Model model) {

		String content = qnaModel.getContent().replaceAll("\r\n", "<br />");
		qnaModel.setContent(content);
		qnaService.qnaWrite(qnaModel);
		model.addAttribute("qnaModel", qnaModel);

		return "redirect:qnaList";
	}

	// 1:1문의 수정폼
	@RequestMapping(value = "/qnaModifyForm")
	public String qnaModifyForm(HttpServletRequest request, QnAModel qnaModel, Model model) {

		qnaModel = qnaService.qnaView(qnaModel.getQna_num());
		String content = qnaModel.getContent().replaceAll("<br />", "\r\n");
		qnaModel.setContent(content);
		model.addAttribute("qnaModel", qnaModel);

		return "qnaModifyForm";
	}

	// 1:1문의 수정
	@RequestMapping("/qnaModify")
	public String qnaModify(@ModelAttribute("qnaModel") QnAModel qnaModel, HttpServletRequest request, Model model) {

		String contents = qnaModel.getContent().replaceAll("\r\n", "<br />");
		qnaModel.setContent(contents);
		qnaService.qnaModify(qnaModel);
		model.addAttribute("qna_num", qnaModel.getQna_num());

		return "redirect:qnaView";
	}

	// 1:1문의 글 삭제
	@RequestMapping(value = "/qnaDelete")
	public String qnaDelete(HttpServletRequest request) {
		int qna_num = Integer.parseInt(request.getParameter("qna_num"));
		qnaService.qnaDelete(qna_num);

		return "redirect:qnaList";
	}

	// 1:1문의 답변 쓰기
	@RequestMapping(value = "/qnaCommWrite")
	public String qnaCommWrite(@ModelAttribute("QnA") QnACommModel qnaCommModel, HttpServletRequest request,
			Model model) {

		String content = qnaCommModel.getContent().replaceAll("\r\n", "<br />");
		qnaCommModel.setContent(content);
		int qna_num = Integer.parseInt(request.getParameter("qna_num"));
		qnaCommModel.setQna_num(qna_num);
		qnaService.qnaCommWrite(qnaCommModel);
		model.addAttribute("qnaCommModel", qnaCommModel);
		qnaService.QnAAdminWrite(qna_num);

		return "redirect:qnaView?qna_num=" + qnaCommModel.getQna_num();
	}

	// 1:1문의 답변 삭제
	@RequestMapping(value = "/qnaCommDelete")
	public String qnaCommDelete(HttpServletRequest request) {
		int qna_num = Integer.parseInt(request.getParameter("qna_num"));
		int comment_num = Integer.parseInt(request.getParameter("comment_num"));
		qnaService.qnaCommDelete(comment_num);
		qnaService.QnAAdminDelete(qna_num);
		return "redirect:qnaView?qna_num=" + qna_num;
	}
}
