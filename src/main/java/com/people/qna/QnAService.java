package com.people.qna;

import java.util.List;

import javax.annotation.Resource;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Service;

@Service
public class QnAService implements QnADao {

	@Resource
	private SqlSessionTemplate sqlSessionTemplate;

	// 글목록
	@Override
	public List<QnAModel> qnaList(String id) {
		return sqlSessionTemplate.selectList("QnA.QnAList", id);
	}

	// 관리자용 글목록
	@Override
	public List<QnAModel> qnaListAdmin() {
		return sqlSessionTemplate.selectList("QnA.QnAListAdmin");
	}

	// 제목으로 검색
	@Override
	public List<QnAModel> qnaSearch0(String search) {
		return sqlSessionTemplate.selectList("QnA.QnASearch0", "%" + search + "%");
	}

	// 내용으로 검색
	@Override
	public List<QnAModel> qnaSearch1(String search) {
		return sqlSessionTemplate.selectList("QnA.QnASearch1", "%" + search + "%");
	}

	// 글 하나 불러오기
	@Override
	public QnAModel qnaView(int qna_num) {
		return sqlSessionTemplate.selectOne("QnA.QnAView", qna_num);
	}

	// 글쓰기
	@Override
	public int qnaWrite(QnAModel qnaModel) {
		return sqlSessionTemplate.insert("QnA.QnAWrite", qnaModel);
	}

	// 글수정
	@Override
	public int qnaModify(QnAModel qnaModel) {
		return sqlSessionTemplate.update("QnA.QnAModify", qnaModel);
	}

	// 글삭제
	@Override
	public int qnaDelete(int qna_num) {
		return sqlSessionTemplate.update("QnA.QnADelete", qna_num);
	}

	// 관리자 답변보기
	public QnACommModel qnaCommView(int qna_num) {
		return sqlSessionTemplate.selectOne("QnA.QnACommView", qna_num);
	}

	// 관리자 답변보기
	public QnACommModel ad(int qna_num) {
		return sqlSessionTemplate.selectOne("QnA.QnACommView", qna_num);
	}

	// 관리자 답변쓰기
	@Override
	public int qnaCommWrite(QnACommModel qnaCommModel) {
		return sqlSessionTemplate.insert("QnA.QnACommWrite", qnaCommModel);
	}

	// 관리자 답변삭제
	@Override
	public int qnaCommDelete(int comment_num) {
		return sqlSessionTemplate.update("QnA.QnACommDelete", comment_num);
	}

	// 관리자 답변여부 등록
	@Override
	public int QnAAdminWrite(int qna_num) {
		return sqlSessionTemplate.update("QnA.QnAAdminWrite", qna_num);
	}

	// 관리자 답변여부 삭제
	@Override
	public int QnAAdminDelete(int qna_num) {
		return sqlSessionTemplate.update("QnA.QnAAdminDelete", qna_num);
	}

}