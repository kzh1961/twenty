package com.people.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.people.notice.NoticeModel;

public class NoticeValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {

		return NoticeModel.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		// ValidationUtils.rejectIfEmptyOrWhitespace(errors, "subject", "제목을
		// 입력해주세요.");
		// ValidationUtils.rejectIfEmptyOrWhitespace(errors, "content", "내용을
		// 입력해주세요.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "subject", "required", new Object[] { "subject" },
				"제목을 입력해주세요.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "content", "required", new Object[] { "content" },
				"내용을 입력해주세요.");
	}
}
