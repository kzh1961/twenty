package com.people.goods;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

//수호
@Controller
public class GoodsController {

	@Resource(name = "goodsService")
	private GoodsService goodsService;
	String session_id;

	// goods list
	@RequestMapping("goodsList")
	public String goodsList(Model model, HttpServletRequest request) throws Exception {

		List<GoodsModel> goodslist = goodsService.goodsList();
		model.addAttribute("goodsList", goodslist);
		return "goodsList";
	}

	/// goods category
	@RequestMapping("goodsCategoryList")
	public String goodsCategoryList(Model model, HttpServletRequest request) throws Exception {

		String goods_categoryy = request.getParameter("goods_category");
		List<GoodsModel> goodsCategoryList = goodsService.goodsCategoryList(goods_categoryy);
		model.addAttribute("cate", goods_categoryy);
		model.addAttribute("goodsList", goodsCategoryList);
		return "goodsList";
	}

	// 상품상세보기(상품정보 + 코멘트)
	@RequestMapping("goodsView")
	public String goodsView(Model model, HttpServletRequest request) throws Exception {

		// 상품상세보기
		int goods_num = Integer.parseInt(request.getParameter("goods_num"));
		GoodsModel goodsModel = goodsService.goodsView(goods_num);
		System.out.println(goodsModel);
		// 추천상품 리스트
		List<GoodsModel> goodsViewBest = goodsService.goodsViewBest();

		// 각 평점 별 개수
		GoodsCommentModel goodsCommentModel = new GoodsCommentModel();
		goodsCommentModel.setGoods_num(goods_num);
		goodsCommentModel.setGoods_point(1);
		int countNum1 = goodsService.countPoint(goodsCommentModel);
		goodsCommentModel.setGoods_point(2);
		int countNum2 = goodsService.countPoint(goodsCommentModel);
		goodsCommentModel.setGoods_point(3);
		int countNum3 = goodsService.countPoint(goodsCommentModel);
		goodsCommentModel.setGoods_point(4);
		int countNum4 = goodsService.countPoint(goodsCommentModel);
		goodsCommentModel.setGoods_point(5);
		int countNum5 = goodsService.countPoint(goodsCommentModel);

		model.addAttribute("countNum1", countNum1);
		model.addAttribute("countNum2", countNum2);
		model.addAttribute("countNum3", countNum3);
		model.addAttribute("countNum4", countNum4);
		model.addAttribute("countNum5", countNum5);
		model.addAttribute("goodsModel", goodsModel);
		model.addAttribute("goodsViewBest", goodsViewBest);
		System.out.println(goods_num);
		System.out.println(goodsModel);
		model.addAttribute("goodsCommentList", goodsService.goodsCommentList(goods_num));

		return "goodsView";
	}

	// goods
	@RequestMapping("goodsSearchList")
	public String goodsSearchList(Model model, HttpServletRequest request) {

		String search = request.getParameter("search");
		List<GoodsModel> goodslist = goodsService.goodsSearchList(search);
		model.addAttribute("goodsList", goodslist);

		return "goodsList";
	}

	// 상품평 쓰기
	@RequestMapping("goodsCommentWrite")
	public String goodsCommentWrite(Model model, HttpServletRequest request, HttpSession session,
			GoodsCommentModel goodsCommentModel) {

		int goods_num = Integer.parseInt(request.getParameter("goods_num"));

		try {
			session_id = session.getAttribute("session_id").toString();

			if (session_id == null) {
				return "redirect:loginForm";
			}

			if (request.getParameter("comments").equals("") || request.getParameter("comments").trim().isEmpty()) {
				return "redirect:goodsView?goods_num=" + goods_num;
			}
		} catch (NullPointerException np) {
			return "redirect:goodsView?goods_num=" + goods_num;
		}

		goodsCommentModel.setComments(request.getParameter("comments").replaceAll("\r\n", "<br />"));
		goodsCommentModel.setGoods_num(goods_num);
		goodsCommentModel.setId(session_id);

		goodsService.goodsCommentWrite(goodsCommentModel);

		// 구매만족도
		goodsService.goodsAvgPoint(goods_num);

		return "redirect:goodsView?goods_num=" + goods_num;

	}

	// 상품평 삭제
	@RequestMapping("goodsCommentDelete")
	public String goodsCommentDelete(Model model, HttpServletRequest request, GoodsCommentModel goodsCommentModel) {
		int comment_num = Integer.parseInt(request.getParameter("comment_num"));
		int goods_num = Integer.parseInt(request.getParameter("goods_num"));
		goodsService.goodsCommentDelete(comment_num);
		goodsService.goodsAvgPoint(goods_num);

		return "redirect:goodsView?goods_num=" + goodsCommentModel.getGoods_num();

	}
}