package com.people.goods;

import java.util.Date;

//쇼핑몰 상품 모델
public class GoodsModel {

	private int goods_num; // 상품번호
	private String goods_category; // 상품 카테고리
	private String goods_name; // 상품 이름
	private int goods_amount; // 상품 수량
	private int goods_price; // 상품 가격
	private String goods_image1; // 메인 이미지 및 상세보기 상단 사진
	private String goods_image2; // 글 중반 내용사진
	private String goods_image3; // 글 하단 배송관련 이미지
	private Date goods_date; // 상품 등록일
	private String goods_seller; // 상품 판매자
	private String goods_subtitle; // 상품 부제목
	private double goods_avg_point; // 상품 평균 평점

	// 상품상세보기 추천을 위한 order_goods_amount 추가
	private int goods_order_sum;

	public int getGoods_order_sum() {
		return goods_order_sum;
	}

	public void setGoods_order_sum(int goods_order_sum) {
		this.goods_order_sum = goods_order_sum;
	}
	// 상품상세보기 추천을 위한 order_goods_amount 추가 끝

	public int getGoods_num() {
		return goods_num;
	}

	public void setGoods_num(int goods_num) {
		this.goods_num = goods_num;
	}

	public String getGoods_category() {
		return goods_category;
	}

	public void setGoods_category(String goods_category) {
		this.goods_category = goods_category;
	}

	public String getGoods_name() {
		return goods_name;
	}

	public void setGoods_name(String goods_name) {
		this.goods_name = goods_name;
	}

	public int getGoods_amount() {
		return goods_amount;
	}

	public void setGoods_amount(int goods_amount) {
		this.goods_amount = goods_amount;
	}

	public int getGoods_price() {
		return goods_price;
	}

	public void setGoods_price(int goods_price) {
		this.goods_price = goods_price;
	}

	public String getGoods_image1() {
		return goods_image1;
	}

	public void setGoods_image1(String goods_image1) {
		this.goods_image1 = goods_image1;
	}

	public String getGoods_image2() {
		return goods_image2;
	}

	public void setGoods_image2(String goods_image2) {
		this.goods_image2 = goods_image2;
	}

	public String getGoods_image3() {
		return goods_image3;
	}

	public void setGoods_image3(String goods_image3) {
		this.goods_image3 = goods_image3;
	}

	public Date getGoods_date() {
		return goods_date;
	}

	public void setGoods_date(Date goods_date) {
		this.goods_date = goods_date;
	}

	public String getGoods_seller() {
		return goods_seller;
	}

	public void setGoods_seller(String goods_seller) {
		this.goods_seller = goods_seller;
	}

	public String getGoods_subtitle() {
		return goods_subtitle;
	}

	public void setGoods_subtitle(String goods_subtitle) {
		this.goods_subtitle = goods_subtitle;
	}

	public double getGoods_avg_point() {
		return goods_avg_point;
	}

	public void setGoods_avg_point(double goods_avg_point) {
		this.goods_avg_point = goods_avg_point;
	}

}