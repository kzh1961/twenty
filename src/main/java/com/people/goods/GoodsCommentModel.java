package com.people.goods;

import java.util.Date;

//상품 후기 코멘트
public class GoodsCommentModel {

	private int comment_num; // 코멘트 번호
	private int goods_num; // 상품 번호
	private String id; // 회원 아이디
	private String comments; // 후기코멘트 내용
	private int goods_point; // 평점
	private Date comment_date; // 코멘트 등록일

	public int getComment_num() {
		return comment_num;
	}

	public void setComment_num(int comment_num) {
		this.comment_num = comment_num;
	}

	public int getGoods_num() {
		return goods_num;
	}

	public void setGoods_num(int goods_num) {
		this.goods_num = goods_num;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public int getGoods_point() {
		return goods_point;
	}

	public void setGoods_point(int goods_point) {
		this.goods_point = goods_point;
	}

	public Date getComment_date() {
		return comment_date;
	}

	public void setComment_date(Date comment_date) {
		this.comment_date = comment_date;
	}
}
