package com.people.goods;

import java.util.List;
import com.people.goods.GoodsModel;

public interface GoodsDao {

	// 상품 총 리스트(관리자용)
	public List<GoodsModel> goodsList();

	// 각 카테고리별 들어갈 경우 카테고리별 전체 출력 1=여성패션, 2=남성패션, 3=신발, 4=구두, 5=ㅈㅂㄷ, 6=ㅈㅂㄱ,
	// 7=ㅂㅈㄱㅈㅂㄱ
	public List<GoodsModel> goodsCategoryList(String goods_category);

	// 메인화면에서 카테고리별로 6개씩 뽑아오기 1=여성패션, 2=남성패션, 3=신발, 4=구두, 5=ㅈㅂㄷ, 6=ㅈㅂㄱ, 7=ㅂㅈㄱㅈㅂㄱ
	public List<GoodsModel> goodsSelectList(String goods_category);

	// 리스트 1개 가져오기 = 상품 상세보기
	public GoodsModel goodsView(int goods_num);

	// 상품 상세보기에서 추천상품 판매수10위 안에 든거
	public List<GoodsModel> goodsViewBest();

	// 상품 수량 감소 증가(주문용)
	public Object amountDown(GoodsModel goodsModel);

	// 상품 검색시 리스트
	public List<GoodsModel> goodsSearchList(String search);

	// 상품 상세보기 코멘트 작성
	public Object goodsCommentWrite(GoodsCommentModel goodsCommentModel);

	// 상품 상세보기 코멘트 전체 리스트
	public List<GoodsCommentModel> goodsCommentList(int goods_num);

	// 상품 상세보기 코멘트 삭제하기
	public Object goodsCommentDelete(int comment_num);

	// 상품 평균 평점
	public int goodsAvgPoint(int goods_num);

	// 상품 평점 별 개수
	public int countPoint(GoodsCommentModel goodsCommentModel);

}