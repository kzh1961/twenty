package com.people.goods;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

@Service
public class GoodsService implements GoodsDao {

	// mybatis 사용하기위한 세션템플릿 설정
	@Resource(name = "sqlSessionTemplate")
	private SqlSessionTemplate sqlSessionTemplate;

	// 상품 총 리스트 출력(관리자용)
	@Override
	public List<GoodsModel> goodsList() {
		return sqlSessionTemplate.selectList("goods.goodsList");
	}

	// 상품 카테고리별 클릭시 출력
	@Override
	public List<GoodsModel> goodsCategoryList(String goods_category) {
		return sqlSessionTemplate.selectList("goods.goodsCategoryList", goods_category);
	}

	// 상품 메인화면에서 카테고리별로 6개씩 출력용
	@Override
	public List<GoodsModel> goodsSelectList(String goods_category) {
		return sqlSessionTemplate.selectList("goods.goodsSelectList", goods_category);
	}

	// 상품 상세보기
	@Override
	public GoodsModel goodsView(int goods_num) {
		return sqlSessionTemplate.selectOne("goods.goodsSelectOne", goods_num);
	}

	// 상품 상세보기에서 추천상품 판매수10위 안에 든거
	@Override
	public List<GoodsModel> goodsViewBest() {
		return sqlSessionTemplate.selectList("goods.goodsViewBest");
	}

	// 상품 수량 감소 증가(주문용)
	@Override
	public Object amountDown(GoodsModel goodsModel) {
		return sqlSessionTemplate.update("goods.amountDown", goodsModel);
	}

	// 상품 검색시 리스트
	@Override
	public List<GoodsModel> goodsSearchList(String search) {
		return sqlSessionTemplate.selectList("goods.goodsSearchList", "%" + search + "%");
	}

	// 상품 코멘트쓰기
	@Override
	public Object goodsCommentWrite(GoodsCommentModel goodsCommentModel) {
		return sqlSessionTemplate.insert("goods.goodsCommentWrite", goodsCommentModel);
	}

	// 상품번호별 코멘트 리스트(상세보기시 사용)
	@Override
	public List<GoodsCommentModel> goodsCommentList(int goods_num) {
		return sqlSessionTemplate.selectList("goods.goodsCommentList", goods_num);
	}

	//
	@Override
	public Object goodsCommentDelete(int comment_num) {
		return sqlSessionTemplate.delete("goods.goodsCommentDelete", comment_num);
	}

	// 상품 평균 평점
	@Override
	public int goodsAvgPoint(int goods_num) {
		return sqlSessionTemplate.update("goods.goodsAvgPoint", goods_num);
	}

	// 상품 평점 별 개수
	@Override
	public int countPoint(GoodsCommentModel goodsCommentModel) {
		return sqlSessionTemplate.selectOne("goods.countPoint", goodsCommentModel);
	}

}