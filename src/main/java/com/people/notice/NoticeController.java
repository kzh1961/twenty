package com.people.notice;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.people.util.Paging;

@Controller
public class NoticeController {

	@Resource
	private NoticeService noticeService;
	private int searchNum;
	private String isSearch;

	// 페이징을 위한 변수 설정
	private int currentPage = 1;
	private int totalCount;
	private int blockCount = 10;
	private int blockPage = 5;
	private String pagingHtml;
	private Paging page;

	// 공지사항 리스트(검색)
	@RequestMapping(value = "noticeList", method = RequestMethod.GET)
	public String noticeList(Model model, HttpServletRequest request) throws UnsupportedEncodingException {

		if (request.getParameter("currentPage") == null || request.getParameter("currentPage").trim().isEmpty()
				|| request.getParameter("currentPage").equals("0")) {
			currentPage = 1;
		} else {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		List<NoticeModel> noticeList;
		
		/* 게시판 검색 */
		isSearch = request.getParameter("isSearch");
	

		if (isSearch != null) {
			searchNum = Integer.parseInt(request.getParameter("searchNum"));

			if (searchNum == 0)
				noticeList = noticeService.noticeSearch0(isSearch);
			else /* (searchNum == 1) */
				noticeList = noticeService.noticeSearch1(isSearch);

			totalCount = noticeList.size();
			page = new Paging(currentPage, totalCount, blockCount, blockPage, "noticeList", searchNum, isSearch);
			pagingHtml = page.getPagingHtml().toString();

			int lastCount = totalCount;

			if (page.getEndCount() < totalCount)
				lastCount = page.getEndCount() + 1;

			noticeList = noticeList.subList(page.getStartCount(), lastCount);

			model.addAttribute("isSearch", isSearch);
			model.addAttribute("totalCount", totalCount);
			model.addAttribute("pagingHtml", pagingHtml);
			model.addAttribute("currentPage", currentPage);
			model.addAttribute("noticeList", noticeList);

			return "noticeList";

		}

		noticeList = noticeService.noticeList();
		totalCount = noticeList.size();
		page = new Paging(currentPage, totalCount, blockCount, blockPage, "noticeList");
		pagingHtml = page.getPagingHtml().toString();
		int lastCount = totalCount;

		if (page.getEndCount() < totalCount)
			lastCount = page.getEndCount() + 1;

		noticeList = noticeList.subList(page.getStartCount(), lastCount);

		model.addAttribute("totalCount", totalCount);
		model.addAttribute("pagingHtml", pagingHtml);
		model.addAttribute("currentPage", currentPage);
		model.addAttribute("noticeList", noticeList);

		return "noticeList";

	}

	// 공지사항 상세보기
	@RequestMapping("noticeView")
	public String noticeView(Model model, HttpServletRequest request) {
		int notice_num = Integer.parseInt(request.getParameter("notice_num"));
		NoticeModel noticeModel = noticeService.noticeView(notice_num);
		noticeService.noticeUpdateReadcount(notice_num);

		model.addAttribute("currentPage", currentPage);
		model.addAttribute("noticeModel", noticeModel);

		return "noticeView";
	}

	// 공지사항 글쓰기폼
	@RequestMapping("noticeWriteForm")
	public String noticeWriteForm(Model model, HttpServletRequest request) {
		model.addAttribute("noticeModel", new NoticeModel());
		return "noticeWriteForm";
	}

	// 공지사항 글쓰기
	@RequestMapping("noticeWrite")
	public String noticeWrite(@ModelAttribute("noticeModel") NoticeModel noticeModel, Model model, BindingResult result,
			HttpServletRequest request, HttpSession session) {

		if (result.hasErrors()) {
			return "redirect:noticeWriteForm";
		}
		String content = noticeModel.getContent().replaceAll("\r\n", "<br />");
		noticeModel.setContent(content);
		noticeService.noticeWrite(noticeModel);
		model.addAttribute("noticeModel", noticeModel);

		return "redirect:noticeList";
	}

	// 공지사항 수정폼
	@RequestMapping("noticeModifyForm")
	public String noticeModifyForm(@ModelAttribute("noticeModel") NoticeModel noticeModel, Model model,
			BindingResult result, HttpServletRequest request) {

		noticeModel = noticeService.noticeView(noticeModel.getNotice_num());
		String content = noticeModel.getContent().replaceAll("<br />", "\r\n");
		noticeModel.setContent(content);
		model.addAttribute("noticeModel", noticeModel);

		return "noticeModifyForm";
	}

	// 공지사항 수정
	@RequestMapping("noticeModify")
	public String noticeModify(@ModelAttribute("noticeModel") NoticeModel noticeModel, Model model,
			HttpServletRequest request) {

		String content = noticeModel.getContent().replaceAll("\r\n", "<br />");
		noticeModel.setContent(content);
		noticeService.noticeModify(noticeModel);
		model.addAttribute("notice_num", noticeModel.getNotice_num());

		return "redirect:noticeView";
	}

	// 공지사항 삭제
	@RequestMapping("noticeDelete")
	public String noticeDelete(HttpServletRequest request) {
		int notice_num = Integer.parseInt(request.getParameter("notice_num"));
		noticeService.noticeDelete(notice_num);

		return "redirect:noticeList";
	}
	
}